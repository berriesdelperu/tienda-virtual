import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { urlService } from '../urlservice';

var headers = new HttpHeaders().set('Content-Type', 'application/json').set('user', 'berries').set('key', 'I32q4CNRJOP17dwyT689cFvKaaxWOAh5j8VExUBUFBUVqtIdjIuAigN4nFbg');

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  public domain: String;
  public domain_images: String;
  public isLoggedin: boolean;

  constructor(private http: HttpClient, private urlService: urlService) {

    this.domain = this.urlService.domain_berries;
    this.domain_images = this.urlService.domain_images_berries;
  }

  stateLogin() {
    var auxSession = localStorage.getItem("datospersona");
    if (auxSession) {
      this.isLoggedin = true;
    } else {
      this.isLoggedin = false;
    }
  }

  logout() {
    this.isLoggedin = false;
    //localStorage.clear();
    localStorage.removeItem('datospersona');
  }



  getLoginByInfo(info) {
    return this.http.post<Object[]>(`${this.domain}clientes/clientev2`, info, { headers }).pipe(map(res => res));

  }

  getPerfil(datosperfil) {
    return this.http.post<Object[]>(`${this.domain}clientes/cliente`, datosperfil, { headers }).pipe(map(res => res));
  }


  registerUserBDBerries(data) {
    return this.http.post<Object[]>(`${this.domain}clientes/crear`, data, { headers }).pipe(map(res => res));
  }

  login(tipo_logueo: string, id_cli_nat: string, nombre_completo: string, clave: string, dni: string, telefono: string, correo: string) {
    let bodyString = JSON.stringify({ tipo_logueo, id_cli_nat, nombre_completo, clave, dni, telefono, correo });

    return this.http.post<Object[]>(`${this.domain_images}loginCliente.php`, bodyString, { headers: new HttpHeaders().set('Accept', 'application/json') }).pipe(map(res => res));
  }

  registarUserBDAdmin(data) {

    return this.http.post<Object[]>(`${this.domain_images}registrarCliente.php`, data, { headers: new HttpHeaders().set('Accept', 'application/json') }).pipe(map(res => res));

  }

  recuperarClave(data) {

    return this.http.post<Object[]>(`${this.domain_images}recuperarClave.php`, data, { headers: new HttpHeaders().set('Accept', 'application/json') }).pipe(map(res => res));

  }

  getPasswordByUsuario(data) {
    return this.http.post<Object[]>(`${this.domain_images}postPas.php`, data, { headers: new HttpHeaders().set('Accept', 'application/json') }).pipe(map(res => res));

  }

  cambiarClave(data) {

    return this.http.post<Object[]>(`${this.domain_images}cambiarClave.php`, data, { headers: new HttpHeaders().set('Accept', 'application/json') }).pipe(map(res => res));

  }

  listaDireccion(data) {
    return this.http.post<Object[]>(`${this.domain_images}postDireccion.php`, data, { headers: new HttpHeaders().set('Accept', 'application/json') }).pipe(map(res => res));
  }

  /*
  registrarOtraDireccion(data){

   return this.http.post<Object[]>(`${this.domain_images}registrarDireccion.php`, data, {headers: new HttpHeaders().set('Accept', 'application/json')}).pipe(map(res => res));

  }
  */

  /*
  eliminarDireccion(data){
   return this.http.post<Object[]>(`${this.domain_images}eliminarDireccion.php`, data, {headers: new HttpHeaders().set('Accept', 'application/json')}).pipe(map(res => res));

  }
  */

  editarPerfil(data) {
    return this.http.post<Object[]>(`${this.domain_images}actualizarDatos.php`, data, { headers: new HttpHeaders().set('Accept', 'application/json') }).pipe(map(res => res));
  }

  editarPerfilBDBerries(data) {
    return this.http.post<Object[]>(`${this.domain}clientes/actualizar`, data, { headers }).pipe(map(res => res));
  }



}
