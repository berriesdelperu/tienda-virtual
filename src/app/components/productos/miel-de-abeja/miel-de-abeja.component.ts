import {
  Component,
  OnInit,
  ViewChild,
  AfterViewInit,
  HostListener,
} from "@angular/core";
import { ProductoService } from "../../../services/producto.service";
import { Router } from "@angular/router";
import { UtilService } from "../../../utils/util.service";
import { ComplementosComponent } from "../../complementos/complementos.component";

@Component({
  selector: "app-miel-de-abeja",
  templateUrl: "./miel-de-abeja.component.html",
  styleUrls: ["./miel-de-abeja.component.css"],
})
export class MielDeAbejaComponent implements OnInit {
  public arrayProductsMieles: Array<any> = [];
  public listMieles: Array<any> = [];
  public slidesMieles: any = [[]];
  public arrayImagesProductsMieles: Array<any> = [];
  public auxArray: Array<any> = [];

  public slidesCategorias: any = [[]];
  public listCategorias: Array<any> = [];

  public countItems = 4;
  public innerWidth: any;

  @ViewChild(ComplementosComponent, { static: true })
  complemento: ComplementosComponent;

  constructor(
    private productoService: ProductoService,
    public router: Router,
    private utilService: UtilService
  ) {
    if (innerWidth < 992) {
      this.countItems = 2;
    }

    if (innerWidth < 769) {
      this.countItems = 1;
    }
  }

  @HostListener("window:resize", ["$event"])
  onResize(event) {
    this.innerWidth = window.innerWidth;
  }


  ngOnInit() {

    this.innerWidth = window.innerWidth;
    window.scrollTo(0, 0);
    $(".imgIconMovilmenuFinal").attr(
      "src",
      "assets/img/icons/icon-menu.svg"
    );
    $(".imgIconMoviltiendaFinal").attr(
      "src",
      "assets/img/icons/icon-tienda.svg"
    );
    $(".imgIconMovilCartFinal").attr(
      "src",
      "assets/img/icons/icon-cart.svg"
    );
    $(".imgIconMovilUserFinal").attr(
      "src",
      "assets/img/icons/icon-user.svg"
    );
    $(".navigationFixed").removeClass("fondito-berries");
    $(".content-tienda").removeClass('header-tienda');
    $('.navbar .nav-item .nav-link').css('color', "#ffffff");
    $('.navbar .nav-item .dropdown-toggle').removeClass('border-dropdown-gray');
    $('.navbar .nav-item .rounded-market').removeClass('border-button-nav');
    $("#img-market").attr("src", "assets/img/icons/icon-market.svg");
    $("#img-cart").attr("src", "assets/img/icons/icon-cart.svg");
    $("#img-user").attr("src", "assets/img/icons/icon-user.svg");
    document.getElementById("scrolling-navbar").style.background = "transparent"
    this.getMieles();

    $(".carousel").on("touchstart", function (event) {
      var xClick = event.originalEvent.touches[0].pageX;
      $(this).one("touchmove", function (event) {
        var xMove = event.originalEvent.touches[0].pageX;
        if (Math.floor(xClick - xMove) > 5) {
          ($(this) as any).carousel('next');
        }
        else if (Math.floor(xClick - xMove) < -5) {
          ($(this) as any).carousel('prev');
        }
      });
      $(".carousel").on("touchend", function () {
        $(this).off("touchmove");
      });
    });
  }


  getMieles() {
    let id_categoria = { linea: "5" };
    let image = { tipo_consulta: "2" };

    this.productoService
      .getProductsByCategoria(id_categoria)
      .subscribe((response) => {
        this.arrayProductsMieles = response;

        this.productoService.getImageProduct(image).subscribe((response1) => {
          var arrayIdItemsWImages = [];
          var arrayIdItemsSystem = [];

          this.arrayImagesProductsMieles = response1["productos"];

          for (var j = 0; j < this.arrayImagesProductsMieles.length; j++) {
            arrayIdItemsWImages.push(
              Number(this.arrayImagesProductsMieles[j]["id_producto"])
            );
          }

          for (var k = 0; k < this.arrayProductsMieles.length; k++) {
            arrayIdItemsSystem.push(
              Number(this.arrayProductsMieles[k]["id_pro"])
            );
          }

          for (var i = 0; i < this.arrayProductsMieles.length; i++) {
            if (
              arrayIdItemsWImages.includes(
                this.arrayProductsMieles[i]["id_pro"]
              )
            ) {
              this.listMieles.push({
                id_pro: this.arrayProductsMieles[i]["id_pro"],
                linea_de_negocio: this.arrayProductsMieles[i][
                  "linea_de_negocio"
                ],
                presentacion: this.arrayProductsMieles[i]["presentacion"],
                nombre_producto: this.arrayProductsMieles[i]["nombre_producto"],
                precio_con_igv: this.arrayProductsMieles[i]["precio_con_igv"],
                stock_actual: this.arrayProductsMieles[i]["stock_actual"],
                stock_reservado: this.arrayProductsMieles[i]["stock_reservado"],
                stock_disponible: this.arrayProductsMieles[i][
                  "stock_disponible"
                ],
                cantidad: 1,
                porcentaje: "0",
                nuevo_precio: this.arrayProductsMieles[i]["precio_con_igv"],
                imagen: this.arrayImagesProductsMieles[
                  arrayIdItemsWImages.indexOf(
                    this.arrayProductsMieles[i]["id_pro"]
                  )
                ]["imagen_principal"],
              });
            } else {
              this.listMieles.push({
                id_pro: this.arrayProductsMieles[i]["id_pro"],
                linea_de_negocio: this.arrayProductsMieles[i][
                  "linea_de_negocio"
                ],
                presentacion: this.arrayProductsMieles[i]["presentacion"],
                nombre_producto: this.arrayProductsMieles[i]["nombre_producto"],
                precio_con_igv: this.arrayProductsMieles[i]["precio_con_igv"],
                stock_actual: this.arrayProductsMieles[i]["stock_actual"],
                stock_reservado: this.arrayProductsMieles[i]["stock_reservado"],
                stock_disponible: this.arrayProductsMieles[i][
                  "stock_disponible"
                ],
                cantidad: 1,
                porcentaje: "0",
                nuevo_precio: this.arrayProductsMieles[i]["precio_con_igv"],
                imagen: "assets/img/icons/image-not-found.png",
              });
            }
          }

          this.listCategorias.push({
            img: "assets/img/icons/icon-honey-1.png",
            titulo: "Tiempo de Vida",
            descripcion:
              "Si bien es cierto que la miel de abeja pura no tiene caducidad cuando se conserva de forma adecuada, por reglamentación aconsejamos su uso en un tiempo no mayor a  2 años.",
          });

          this.listCategorias.push({
            img: "assets/img/icons/icon-honey-2.png",
            titulo: "Conservación",
            descripcion:
              "Para conservarla en buen estado, mantenla siempre a  temperatura de ambiente. No necesitas refrigerarla.",
          });

          this.listCategorias.push({
            img: "assets/img/icons/icon-honey-3.png",
            titulo: "TIPS",
            descripcion:
              "La verdadera miel tiende a cristalizarse con las bajas temperaturas, es por ello que, si llegara a ocurrir, recomendamos ponerla en baño maría por unos minutos, hasta que regrese a su estado semilíquido nuevamente.",
          });

          this.listCategorias.push({
            img: "assets/img/icons/icon-honey-4.png",
            titulo: "Descripción linea",
            descripcion:
              "Esta miel de abeja proviene de una floración especialmente de arándanos. Su ámbar intenso, su consistencia fluida y su sabor malteado, que te recordará a la madera, la convertirán en tu favorita.",
          });

          this.slidesCategorias = this.utilService.chunk(
            this.listCategorias,
            1
          );
          ($('#ContenedorSlider') as any).carousel({
            interval: 2000,
            cycle: true
          });
          // if(this.listMieles.length < 5){
          //   $(".rowRightProductosRelacionados").hide();
          //   $("#olProductosRelacionadosIndicator").hide();
          // }

          this.slidesMieles = this.utilService.chunk(
            this.listMieles,
            this.countItems
          );
        });
      });
  }

  goBlog() {
    this.router.navigate(["/blog"]);
  }
}
