import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TiendaComponent } from './tienda.component';
import { DetalleProductoComponent} from './detalle-producto/detalle-producto.component';
import { BuscarProductoComponent } from './buscar-producto/buscar-producto.component';

const routes: Routes = [

  
  {
    path: '',
    component: TiendaComponent,
  },
  {
    path: 'buscar',
    component: BuscarProductoComponent
  },
  {
    path: ':id',
    component: DetalleProductoComponent
  }
  

];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TiendaRoutingModule { }
