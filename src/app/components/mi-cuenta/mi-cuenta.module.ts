import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MiCuentaRoutingModule } from './mi-cuenta-routing.module';
import { MiCuentaComponent } from './mi-cuenta.component';
import { EditarCuentaComponent } from './editar-cuenta/editar-cuenta.component';
import { DataTablesModule } from 'angular-datatables';
import { FormsModule } from '@angular/forms';
import { Select2Module } from 'ng2-select2';
import { AgmCoreModule } from '@agm/core';


@NgModule({
  declarations: [MiCuentaComponent, EditarCuentaComponent ],
  imports: [
    CommonModule,
    MiCuentaRoutingModule,
    DataTablesModule,
    FormsModule,
    Select2Module,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCdMfbm6AJUE-9lHGdcF0mSMz0EehAK9qQ',
      libraries: ['places']
    }),

    
  ],
})
export class MiCuentaModule { }
