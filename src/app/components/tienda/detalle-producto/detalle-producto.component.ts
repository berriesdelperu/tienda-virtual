import {
  Component,
  OnInit,
  ViewChild,
  AfterViewInit,
  HostListener,
} from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { ProductoService } from "../../../services/producto.service";
import { Router } from "@angular/router";
import { UtilService } from "src/app/utils/util.service";
import { ComplementosComponent } from "../../complementos/complementos.component";
import { isNgTemplate } from "@angular/compiler";

@Component({
  selector: "app-detalle-producto",
  templateUrl: "./detalle-producto.component.html",
  styleUrls: ["./detalle-producto.component.css"],
})
export class DetalleProductoComponent implements OnInit {
  public id_producto: string;
  public nombreCategoria: string;
  public nombreProducto: string;
  public peso: string;
  public precio: string;
  public id_categoria: string;
  public imagen: string;
  public caracteristicas: string;
  public ingredientes: string;
  public sellos: string[][];
  public detailProduct: Array<any>;

  public auxArray: Array<any> = [];

  public arrayProductsRelation: Array<any> = [];
  public listProductsRelation: Array<any> = [];
  public slidesProductsRelation: any = [[]];
  public arrayImagesProductRelation: Array<any> = [];

  public arrayProductDetail: Array<any> = [];
  public arrayImagesDetail: Array<any> = [];
  public listDetail: Array<any> = [];

  public porcentajeDescuento: string;

  public isProductoPromocion: Boolean;

  public id = {};

  public listaCalorias: Object[];
  public countItems = 4;
  public innerWidth: any;

  public nombreCategoriaLink = "";

  @ViewChild(ComplementosComponent, { static: true })
  complemento: ComplementosComponent;

  constructor(
    private activeRoute: ActivatedRoute,
    private productoService: ProductoService,
    private router: Router,
    private utilService: UtilService
  ) {
    const routeParams = this.activeRoute.snapshot.params;

    this.id_producto = routeParams.id;

    if (innerWidth < 992) {
      this.countItems = 2;
    }

    if (innerWidth < 769) {
      this.countItems = 1;
    }

    this.isProductoPromocion = true;

    this.id = { producto: this.id_producto };
  }
  @HostListener("window:resize", ["$event"])
  onResize(event) {
    this.innerWidth = window.innerWidth;
  }

  ngOnInit() {

    this.innerWidth = window.innerWidth;

    window.scrollTo(0, 0);

    $(".imgIconMovilmenuFinal").attr(
      "src",
      "assets/img/icons/icon-menu-2.svg"
    );
    $(".imgIconMoviltiendaFinal").attr(
      "src",
      "assets/img/icons/icon-tienda-2.svg"
    );
    $(".imgIconMovilCartFinal").attr(
      "src",
      "assets/img/icons/icon-cart-2.svg"
    );
    $(".imgIconMovilUserFinal").attr(
      "src",
      "assets/img/icons/icon-user-2.svg"
    );


    $(".content-tienda").removeClass('header-tienda');
    $('.navbar .nav-item .nav-link').css('color', "#778489");
    $('.navbar .nav-item .dropdown-toggle').addClass('border-dropdown-gray');
    $('.navbar .nav-item .rounded-market').addClass('border-button-nav');;
    $("#img-market").attr("src", "assets/img/icons/icon-market-gray.svg");
    $("#img-cart").attr("src", "assets/img/icons/icon-cart-gray.svg");
    $("#img-user").attr("src", "assets/img/icons/icon-user-gray.svg");
    document.getElementById("scrolling-navbar").style.background = "white"
    this.getDetailByProduct();
    this.getProductsRelation();
    this.getCaracteristicas();
  }

  mostrarTabla() {
    $("#contentTablaNutricional").show();
  }

  ocultarTabla() {
    $("#contentTablaNutricional").hide();
  }

  getDetailByProduct() {
    let id_image = { id_parametro: this.id_producto };

    this.productoService.getDetailProduct(this.id).subscribe(
      (response) => {
        this.arrayProductDetail = response;

        this.productoService
          .getProductDetailImage(id_image)
          .subscribe((response1) => {
            this.arrayImagesDetail = response1["productos"];

            for (var i = 0; i < this.arrayProductDetail.length; i++) {
              for (var j = 0; j < this.arrayImagesDetail.length; j++) {
                if (
                  this.arrayProductDetail[i]["id_pro"] ===
                  Number(this.arrayImagesDetail[j]["id_producto"])
                ) {
                  this.listDetail.push({
                    id_pro: this.arrayProductDetail[i]["id_pro"],
                    linea_de_negocio: this.arrayProductDetail[i][
                      "linea_de_negocio"
                    ],
                    presentacion: this.arrayProductDetail[i]["presentacion"],
                    nombre_producto: this.arrayProductDetail[i][
                      "nombre_producto"
                    ],
                    precio_con_igv: this.arrayProductDetail[i][
                      "precio_con_igv"
                    ],
                    stock_actual: this.arrayProductDetail[i]["stock_actual"],
                    stock_reservado: this.arrayProductDetail[i][
                      "stock_reservado"
                    ],
                    stock_disponible: this.arrayProductDetail[i][
                      "stock_disponible"
                    ],
                    cantidad: 1,
                    porcentaje: this.arrayImagesDetail[i]["monto_promo"],
                    nuevo_precio: (
                      this.arrayProductDetail[i]["precio_con_igv"] -
                      this.arrayProductDetail[i]["precio_con_igv"] *
                      (Number(this.arrayImagesDetail[j]["monto_promo"]) / 100)
                    ).toFixed(2),
                    imagen: this.arrayImagesDetail[j]["imagen_principal"],
                    caracteristicas: this.arrayImagesDetail[j][
                      "caracteristicas"
                    ],
                    ingredientes: this.arrayImagesDetail[j]["ingredientes"],
                  });
                }
              }
            }

            if (this.listDetail[0]["linea_de_negocio"] == "MERMELADAS") {
              this.nombreCategoriaLink = "mermeladas";
            }

            if (this.listDetail[0]["linea_de_negocio"] == "CONGELADOS") {
              this.nombreCategoriaLink = "congelados";
            }

            if (this.listDetail[0]["linea_de_negocio"] == "MIELES") {
              this.nombreCategoriaLink = "miel-de-abeja";
            }

            if (this.listDetail[0]["linea_de_negocio"] == "PULPAS") {
              this.nombreCategoriaLink = "pulpas";
            }

            this.nombreCategoria = this.listDetail[0]["linea_de_negocio"];
            this.nombreProducto = this.listDetail[0]["nombre_producto"];
            this.id_categoria = this.listDetail[0]["id_lin_neg"];
            this.imagen = this.listDetail[0]["imagen"];
            this.caracteristicas = this.listDetail[0]["caracteristicas"].split(
              "~"
            );
            this.ingredientes = this.listDetail[0]["ingredientes"];

            this.porcentajeDescuento = this.listDetail[0]["porcentaje"];

            this.sellos = this.getStampImages();

            if (!this.porcentajeDescuento) {
              this.isProductoPromocion = false;
            } else {
              this.isProductoPromocion = true;
            }
          });
      },
      (error) => {
        console.log(error);
      }
    );
  }

  createStamp(stampName: string) {
    const baseUrl = "assets/img/icons/sellos/";

    return [
      baseUrl + "90px_" + stampName + ".png",
      baseUrl + "60px_" + stampName + ".png",
    ];
  }

  getStampImages() {
    const stamps: string[][] = [];
    switch (this.nombreCategoria) {
      case "PULPAS":
        stamps.push(this.createStamp("100_natural_listo"));
        stamps.push(this.createStamp("biodegradable"));
        stamps.push(this.createStamp("recolectado"));
        stamps.push(this.createStamp("sin_quimicos"));

        break;
      case "MERMELADAS":
        stamps.push(this.createStamp("100_natural"));
        stamps.push(this.createStamp("panela"));
        stamps.push(this.createStamp("sin_quimicos"));
        stamps.push(this.createStamp("sincolorante"));
        break;
      case "CONGELADOS":
        stamps.push(this.createStamp("100_natural_listo"));
        stamps.push(this.createStamp("recolectado"));
        stamps.push(this.createStamp("sin_quimicos"));
        stamps.push(this.createStamp("biodegradable"));
        break;
      case "MIELES":
        stamps.push(this.createStamp("100_natural_listo"));
        break;
    }
    return stamps;
  }

  getProductsRelation() {
    let image = { tipo_consulta: "2" };

    this.productoService
      .getProductsRelacionado(this.id)
      .subscribe((response) => {
        this.arrayProductsRelation = response;

        this.productoService.getImageProduct(image).subscribe((response1) => {
          var arrayIdItemsWImages = [];
          var arrayIdItemsSystem = [];

          this.arrayImagesProductRelation = response1["productos"];

          for (var j = 0; j < this.arrayImagesProductRelation.length; j++) {
            arrayIdItemsWImages.push(
              Number(this.arrayImagesProductRelation[j]["id_producto"])
            );
          }

          for (var k = 0; k < this.arrayProductsRelation.length; k++) {
            arrayIdItemsSystem.push(
              Number(this.arrayProductsRelation[k]["id_pro"])
            );
          }

          for (var i = 0; i < this.arrayProductsRelation.length; i++) {
            if (
              arrayIdItemsWImages.includes(
                this.arrayProductsRelation[i]["id_pro"]
              )
            ) {
              this.listProductsRelation.push({
                id_pro: this.arrayProductsRelation[i]["id_pro"],
                linea_de_negocio: this.arrayProductsRelation[i][
                  "linea_de_negocio"
                ],
                presentacion: this.arrayProductsRelation[i]["presentacion"],
                nombre_producto: this.arrayProductsRelation[i][
                  "nombre_producto"
                ],
                precio_con_igv: this.arrayProductsRelation[i]["precio_con_igv"],
                stock_actual: this.arrayProductsRelation[i]["stock_actual"],
                stock_reservado: this.arrayProductsRelation[i][
                  "stock_reservado"
                ],
                stock_disponible: this.arrayProductsRelation[i][
                  "stock_disponible"
                ],
                cantidad: 1,
                porcentaje: "0",
                nuevo_precio: this.arrayProductsRelation[i]["precio_con_igv"],
                imagen: this.arrayImagesProductRelation[
                  arrayIdItemsWImages.indexOf(
                    this.arrayProductsRelation[i]["id_pro"]
                  )
                ]["imagen_principal"],
              });
            } else {
              this.listProductsRelation.push({
                id_pro: this.arrayProductsRelation[i]["id_pro"],
                linea_de_negocio: this.arrayProductsRelation[i][
                  "linea_de_negocio"
                ],
                presentacion: this.arrayProductsRelation[i]["presentacion"],
                nombre_producto: this.arrayProductsRelation[i][
                  "nombre_producto"
                ],
                precio_con_igv: this.arrayProductsRelation[i]["precio_con_igv"],
                stock_actual: this.arrayProductsRelation[i]["stock_actual"],
                stock_reservado: this.arrayProductsRelation[i][
                  "stock_reservado"
                ],
                stock_disponible: this.arrayProductsRelation[i][
                  "stock_disponible"
                ],
                cantidad: 1,
                porcentaje: "0",
                nuevo_precio: this.arrayProductsRelation[i]["precio_con_igv"],
                imagen: "assets/img/icons/image-not-found.png",
              });
            }
          }

          if (this.listProductsRelation.length < 5) {
            $("#rowRightProductosRelacionados").hide();
            $("#olProductosRelacionadosIndicator").hide();
          }
          if (this.countItems == 1) {
            if (this.innerWidth < 575) {
              $("#rowRightProductosRelacionados").show();
              $("#olProductosRelacionadosIndicator").show();
            }
          }

          this.slidesProductsRelation = this.utilService.chunk(
            this.listProductsRelation,
            this.countItems
          );
        });
      });
  }

  redirectOtherProduct(idproduct) {
    this.router
      .navigateByUrl("refresh", { skipLocationChange: true })
      .then(() => {
        this.router.navigate(["/tienda/" + idproduct]);
      });
  }

  getCaracteristicas() {
    this.productoService.getProductCaracteristicas(this.id).subscribe(
      (response) => {
        if (response["success"]) {
          this.listaCalorias = response["calorias"];
        } /* else {

        console.log(response["message"]);

      }*/
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
