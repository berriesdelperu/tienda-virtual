import { Component, OnInit, NgZone, ViewChild, ElementRef } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { UtilService } from 'src/app/utils/util.service';
import { Select2OptionData } from 'ng2-select2';
import { DireccionesService } from 'src/app/services/direcciones.service';
import { MapsAPILoader, MouseEvent } from '@agm/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-editar-direccion',
  templateUrl: './editar-direccion.component.html',
  styleUrls: ['./editar-direccion.component.css']
})
export class EditarDireccionComponent implements OnInit {

  public onClose: any;
  public id_direccion: any;
  public id_provincia: any;
  public id_distrito: any;
  public id_precio_envio: any;
  public direccion: any;
  public id_opcion_direccion: any;
  public numero: any;
  public referencia_1: any;
  public referencia_2: any;
  public direccion_principal: any;

  public listPrecioEnvio: Object[];


  public provincias: Array<Select2OptionData>;
  public optionsSelect2Provincias: Select2Options;
  public provinciaSeleccionada: any;
  public selectedProvincia: string;

  public distritos: Array<Select2OptionData>;
  public optionsSelect2Distritos: Select2Options;
  public distritoSeleccionado: any;
  public selectedDistrito: string;

  public tipo_direccion: Array<Select2OptionData>;
  public optionsSelect2TipoDireccion: Select2Options;
  public tipodireccionSeleccionado: any;
  public selectedTipoDireccion: string;

  public latitud: number;
  public longitud: number;

  @ViewChild('searchEditar', { static: true }) searchElementRef: ElementRef;
  private geoCoder;
  public latitude: number;
  public longitude: number;
  public zoom: number;
  public address: string;


  constructor(public modalRef: BsModalRef, public utilService: UtilService, private direccionesService: DireccionesService,
    private mapsAPILoader: MapsAPILoader, private ngZone: NgZone) {

    this.optionsSelect2Provincias = {
      placeholder: "[ Seleccione provincia ]",
      width: "100%"
    }

    this.optionsSelect2Distritos = {
      placeholder: "[ Seleccione distrito ]",
      width: "100%"
    }

    this.optionsSelect2TipoDireccion = {
      placeholder: "[ Seleccione el tipo de direccion ]",
      width: "100%"
    }
  }

  ngOnInit() {

    this.getProvincias();
    this.autocomplete();
    this.getTipoDireccion();
  }

  autocomplete() {
    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation();
      this.geoCoder = new google.maps.Geocoder;

      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"],
        componentRestrictions: { 'country': 'PE' }
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();


          this.direccion = place.formatted_address;

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 15;
        });
      });
    });

  }

  private setCurrentLocation() {

    this.latitude = Number(this.latitud);
    this.longitude = Number(this.longitud);
    this.zoom = 15;
  }

  markerDragEnd($event: MouseEvent) {

    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    this.getAddress(this.latitude, this.longitude);
  }

  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 15;
          this.address = results[0].formatted_address;
          this.direccion = this.address;
        } else {
          window.alert('No se encontraron resultados GEOCODER');
        }
      } else {
        window.alert('Geocoder fallido : ' + status);
      }

    });
  }

  changeProvincias(e: any) {
    this.distritos = null
    if (this.selectedProvincia) {
      this.direccion = ""
      this.numero = ""
      this.referencia_1 = ""
      $('#selectTipo select').val(null).trigger('change')

    }
    this.selectedProvincia = e.value;
    let provincia = { id_provincia: this.selectedProvincia };

    this.getDistritos(provincia);



  }

  changeDistritos(e: any) {
    if (this.selectedDistrito) {
      this.direccion = ""
      this.numero = ""
      this.referencia_1 = ""
      $('#selectTipo select').val(null).trigger('change')

    }
    let distrito = { id_distrito: this.selectedDistrito };

    this.getPrecioxDireccion(distrito);
    this.selectedDistrito = e.value;
    this.numero = ""
    this.referencia_1 = ""
  }

  changeTipoDireccion(e: any) {
    this.selectedTipoDireccion = e.value;

  }


  getProvincias() {

    this.direccionesService.getProvincias().subscribe(response => {

      var arrayAuxReg = [];
      arrayAuxReg.push({ id: '', text: "" });

      for (let index = 0; index < response["length"]; index++) {

        arrayAuxReg.push({
          id: response[index]['id_provincia'],
          text: response[index]['provincia']
        });

      }

      this.provincias = arrayAuxReg;

      this.provinciaSeleccionada = this.id_provincia;



    })

  }

  getDistritos(provincia) {



    this.direccionesService.getDistritos(provincia).subscribe(response => {

      var arrayAuxReg = [];
      arrayAuxReg.push({ id: '', text: "" });

      for (let index = 0; index < response["length"]; index++) {

        arrayAuxReg.push({
          id: response[index]['id_distrito'],
          text: response[index]['distrito']
        });

      }

      this.distritos = arrayAuxReg;

      this.distritoSeleccionado = this.id_distrito;



    })

  }


  getTipoDireccion() {

    this.direccionesService.getOpcionDireccion().subscribe(response => {

      var arrayAuxReg = [];
      arrayAuxReg.push({ id: '', text: "" });

      for (let index = 0; index < response["length"]; index++) {

        arrayAuxReg.push({
          id: response[index]['id_opcion'],
          text: response[index]['opcion']
        });

      }

      this.tipo_direccion = arrayAuxReg;

      this.tipodireccionSeleccionado = this.id_opcion_direccion;



    })

  }


  getPrecioxDireccion(distrito) {
    this.direccionesService.getPrecioxDireccion(distrito).subscribe(response => {
      this.listPrecioEnvio = response;
    },
      error => {
        console.log(error);
      })
  }


  actualizarDireccion() {

    if (this.selectedProvincia == "") {
      Swal.fire("Oops...", "Seleccione Provincia", "error");
      return false;

    } else if (this.selectedDistrito == "") {
      Swal.fire("Oops...", "Seleccione Distrito", "error");
      return false;

    } else if (this.selectedTipoDireccion == "") {
      Swal.fire("Oops...", "Ingrese el tipo de direccion", "error");

      return false;
    } else if (this.direccion == "") {
      Swal.fire("Oops...", "Ingrese la dirección", "error");

      return false;
    } else if (this.numero == "") {
      Swal.fire("Oops...", "Ingrese el numero", "error");

      return false;
    } else if (this.referencia_1 == "") {
      Swal.fire("Oops...", "Ingrese la referencia", "error");

      return false;

    } else {

      let data = {
        id_direccion: String(this.id_direccion),
        id_provincia: String(this.selectedProvincia),
        id_distrito: String(this.selectedDistrito),
        id_precio_envio: String(this.listPrecioEnvio[0]["id_envio"]),
        direccion: this.direccion,
        id_opcion_direccion: String(this.selectedTipoDireccion),
        numero: this.numero,
        referencia_1: this.referencia_1,
        referencia_2: this.referencia_1,
        principal: this.direccion_principal,
        latitud: String(this.latitude),
        longitud: String(this.longitude)
      };


      console.log(data);





      this.direccionesService.actualizarDireccion(data).subscribe(response => {


        this.onClose({
          estado: true,
          mensaje: "Actualizado",
          data: data
        });
      },
        error => {
          console.log(error);
        })


    }


  }



}
