import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CanActivate } from '@angular/router';
import { ClienteService } from '../services/cliente.service';

@Injectable()
export class CanActivateViaAuthGuard implements CanActivate {

    constructor(private clienteService: ClienteService, private router: Router) { }

    canActivate() {
        if (!this.clienteService.isLoggedin) {
            this.router.navigate(['/']);
            return false;
        }

        return true;
    }
}
