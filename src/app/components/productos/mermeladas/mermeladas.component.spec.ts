import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MermeladasComponent } from './mermeladas.component';

describe('MermeladasComponent', () => {
  let component: MermeladasComponent;
  let fixture: ComponentFixture<MermeladasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MermeladasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MermeladasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
