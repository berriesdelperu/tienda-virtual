import { Component, OnInit } from '@angular/core';
import {  BsModalRef } from 'ngx-bootstrap/modal';
import { UtilService } from 'src/app/utils/util.service';
import Inputmask from "inputmask";
import { TarjetasService } from 'src/app/services/tarjetas.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-agregar-tarjeta',
  templateUrl: './agregar-tarjeta.component.html',
  styleUrls: ['./agregar-tarjeta.component.css']
})
export class AgregarTarjetaComponent implements OnInit {
   
  public num_tarjeta: string;
  public mes_tarjeta: string;
  public year_tarjeta: string;
  public id_cliente : string;
  public onClose :any;

  constructor(public  modalRef: BsModalRef , public utilService: UtilService , public tarjetaService: TarjetasService) { 
    this.num_tarjeta = "";
    this.mes_tarjeta = "";
    this.year_tarjeta = "";
    var auxiliar =  JSON.parse(localStorage.getItem("datospersona"));
    this.id_cliente =auxiliar["id_cli_nat"];

   
    
  }

  ngOnInit() {

    Inputmask({"mask": "9999 9999 9999 9999" , "placeholder": ""}).mask(document.getElementById("num_tarjeta"));
  }

  agregarTarjeta(){

    if(this.num_tarjeta == ""  || this.num_tarjeta.length < 16 ){
      Swal.fire("Oops...","El numero de tarjeta es inválido","error");
      return false;
    }else if(this.mes_tarjeta == "" || this.mes_tarjeta.length < 2 || this.mes_tarjeta == "00"){
      Swal.fire("Oops...","El mes de la tarjeta es inválido","error");
      return false;
    }else if(this.year_tarjeta == "" || this.year_tarjeta.length < 4 ){
      Swal.fire("Oops...","El año de la tarjeta es inválido","error");
      return false;

    }else{
      
      var space_blank_tarjeta = this.num_tarjeta.replace(/\s/g, '');
      var aux_num = this.num_tarjeta.substr(-5);

      let data = {
        nro_tarjeta: space_blank_tarjeta,
        mes: this.mes_tarjeta,
        year : this.year_tarjeta,
        last_four_digits: "XXXX XXXX XXXX"+ aux_num,
        id_cli_nat : this.id_cliente
      }


  
      this.tarjetaService.registrarTarjetas(data).subscribe(response => {
  


        this.onClose({
          estado  : true,
          mensaje : "Registro realizado",
          data    : data
        });
  
      },
      error => {
        console.log(error);
      })

    }

    
    
  }

}
