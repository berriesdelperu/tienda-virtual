import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map, tap } from "rxjs/operators";
import { urlService } from "../urlservice";
import { Subject } from "rxjs";

var headers = new HttpHeaders()
  .set("Content-Type", "application/json")
  .set("user", "berries")
  .set("key", "I32q4CNRJOP17dwyT689cFvKaaxWOAh5j8VExUBUFBUVqtIdjIuAigN4nFbg");

@Injectable({
  providedIn: "root",
})
export class DireccionesService {
  public domain: String;

  public _refreshDirecciones = new Subject<void>();

  constructor(private http: HttpClient, private urlService: urlService) {
    this.domain = this.urlService.domain_berries;
  }

  getProvincias() {
    return this.http
      .get<Object[]>(`${this.domain}direcciones/provincia`, { headers })
      .pipe(map((res) => res));
  }

  getDistritos(data) {
    return this.http
      .post<Object[]>(`${this.domain}direcciones/distrito`, data, { headers })
      .pipe(map((res) => res));
  }

  getOpcionDireccion() {
    return this.http
      .get<Object[]>(`${this.domain}direcciones/opcion_direccion`, { headers })
      .pipe(map((res) => res));
  }

  getPrecioxDireccion(data) {
    return this.http
      .post<Object[]>(`${this.domain}direcciones/envio`, data, { headers })
      .pipe(map((res) => res));
  }

  refreshListaDireccion() {
    return this._refreshDirecciones;
  }

  crearDireccion(data) {
    // return this.http.post<Object[]>(`${this.domain}direcciones/crear`, data, {headers}).pipe(map(res => res));
    return this.http
      .post(`${this.domain}direcciones/crear`, data, { headers })
      .pipe(
        tap(() => {
          this._refreshDirecciones.next();
        })
      );
  }

  getDireccionByCliente(data) {
    return this.http
      .post<Object[]>(`${this.domain}direcciones/lista`, data, { headers })
      .pipe(map((res) => res));
  }

  eliminarDireccion(data) {
    return this.http
      .post<Object[]>(`${this.domain}direcciones/eliminar`, data, { headers })
      .pipe(map((res) => res));
  }

  actualizarDireccion(data) {
    return this.http
      .post<Object[]>(`${this.domain}direcciones/actualizar`, data, { headers })
      .pipe(map((res) => res));
  }
}
