import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MielDeAbejaComponent } from './miel-de-abeja.component';

describe('MielDeAbejaComponent', () => {
  let component: MielDeAbejaComponent;
  let fixture: ComponentFixture<MielDeAbejaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MielDeAbejaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MielDeAbejaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
