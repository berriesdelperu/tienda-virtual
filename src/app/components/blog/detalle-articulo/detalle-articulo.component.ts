import { Component, OnInit, AfterViewInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { BlogService } from "../../../services/blog.service";

@Component({
  selector: "app-detalle-articulo",
  templateUrl: "./detalle-articulo.component.html",
  styleUrls: ["./detalle-articulo.component.css"],
})
export class DetalleArticuloComponent implements OnInit {
  public id_article: string;
  public slug: string;
  public arrayDetail: Object[];
  public arrayCategories: Object[];
  public arrayArticles: Object[];

  constructor(
    private blogService: BlogService,
    private activeRoute: ActivatedRoute,
    private router: Router
  ) {
    const routeParams = this.activeRoute.snapshot.params;
    this.id_article = routeParams.id;
    this.slug = routeParams.slug;
    this.arrayDetail = [];
  }

  ngOnInit() {
    $(".imgIconMovilmenuFinal").attr("src", "assets/img/icons/icon-menu-2.svg");
    $(".imgIconMoviltiendaFinal").attr(
      "src",
      "assets/img/icons/icon-tienda-2.svg"
    );
    $(".imgIconMovilCartFinal").attr("src", "assets/img/icons/icon-cart-2.svg");
    $(".imgIconMovilUserFinal").attr("src", "assets/img/icons/icon-user-2.svg");

    window.scrollTo(0, 0);
    $(".content-tienda").addClass("header-tienda");
    $(".navbar .nav-item .nav-link").css("color", "#778489");
    $(".navbar .nav-item .dropdown-toggle").addClass("border-dropdown-gray");
    $(".navbar .nav-item .rounded-market").addClass("border-button-nav");
    $("#img-market").attr("src", "assets/img/icons/icon-market-gray.svg");
    $("#img-cart").attr("src", "assets/img/icons/icon-cart-gray.svg");
    $("#img-user").attr("src", "assets/img/icons/icon-user-gray.svg");

    this.getDetailArticle();
    this.getArticles();
  }

  getDetailArticle() {
    this.blogService.getDetailArticle(this.id_article, this.slug).subscribe(
      (response) => {
        this.arrayDetail = response;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  getArticles() {
    this.blogService.getAllArticles().subscribe(
      (response) => {
        let shuffled = response.sort(() => 0.5 - Math.random());

        // Trae un sub-array de n elementos despues de ordenarlos.
        this.arrayArticles = shuffled.slice(0, 3);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  redirectOtherArticle(id, slug) {
    this.router
      .navigateByUrl("/refresh", { skipLocationChange: true })
      .then(() => {
        this.router.navigate(["/blog/" + id + "/" + slug]);
      });
  }
}
