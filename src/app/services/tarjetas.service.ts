import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import {urlService} from '../urlservice';

@Injectable({
  providedIn: 'root'
})
export class TarjetasService {

  public domain_images: String;

  constructor(private http: HttpClient , private urlService: urlService) {

    this.domain_images = this.urlService.domain_images_berries;
   }


   getTarjetas(data){
     
    return this.http.post<Object[]>(`${this.domain_images}getTarjetas.php`, data, {headers: new HttpHeaders().set('Accept', 'application/json')}).pipe(map(res => res));
   }

   registrarTarjetas(data){
    return this.http.post<Object[]>(`${this.domain_images}registrarTarjeta.php`, data, {headers: new HttpHeaders().set('Accept', 'application/json')}).pipe(map(res => res));
   }

   eliminarTarjeta(data){
    return this.http.post<Object[]>(`${this.domain_images}eliminarTarjeta.php`, data, {headers: new HttpHeaders().set('Accept', 'application/json')}).pipe(map(res => res));

   }


}
