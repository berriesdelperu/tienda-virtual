import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-terminos",
  templateUrl: "./terminos.component.html",
  styleUrls: ["./terminos.component.css"],
})
export class TerminosComponent implements OnInit {
  constructor() {}

  ngOnInit() {
    $(".imgIconMovilmenuFinal").attr("src", "assets/img/icons/icon-menu-2.svg");
    $(".imgIconMoviltiendaFinal").attr(
      "src",
      "assets/img/icons/icon-tienda-2.svg"
    );
    $(".imgIconMovilCartFinal").attr("src", "assets/img/icons/icon-cart-2.svg");
    $(".imgIconMovilUserFinal").attr("src", "assets/img/icons/icon-user-2.svg");
    $(".content-tienda").addClass("header-tienda");
    $(".navbar .nav-item .nav-link").css("color", "#778489");
    $(".navbar .nav-item .dropdown-toggle").addClass("border-dropdown-gray");
    $(".navbar .nav-item .rounded-market").addClass("border-button-nav");
    $("#img-market").attr("src", "assets/img/icons/icon-market-gray.svg");
    $("#img-cart").attr("src", "assets/img/icons/icon-cart-gray.svg");
    $("#img-user").attr("src", "assets/img/icons/icon-user-gray.svg");
  }
}
