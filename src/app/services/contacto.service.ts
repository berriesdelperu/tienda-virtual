import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import {urlService} from '../urlservice';

var headers = new HttpHeaders().set('Content-Type','application/json');

@Injectable({
  providedIn: 'root'
})
export class ContactoService {

  public domain_images: String;

  constructor(private http: HttpClient , private urlService: urlService) {

    this.domain_images = this.urlService.domain_images_berries;
   }

   registerFormContact(form_contact){
    return this.http.post<Object[]>(`${this.domain_images}registrarContacto.php`, form_contact, {headers }).pipe(map(res => res));
  }
  registarReclamationsBook(form_reclamation){
    return this.http.post<Object[]>(`${this.domain_images}registrarReclamo.php`, form_reclamation, {headers }).pipe(map(res => res));
  }

}
