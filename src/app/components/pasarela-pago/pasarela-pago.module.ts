import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PasarelaPagoRoutingModule } from './pasarela-pago-routing.module';
import { PasarelaPagoComponent } from './pasarela-pago.component';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';

@NgModule({
  declarations: [PasarelaPagoComponent],
  imports: [
    CommonModule,
    PasarelaPagoRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AutocompleteLibModule
  ]
})
export class PasarelaPagoModule { }
