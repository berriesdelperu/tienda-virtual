import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComplementosComponent} from './complementos.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [ComplementosComponent],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [ComplementosComponent]
})
export class ComplementosModule { }
