import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import {urlService} from '../urlservice';

@Injectable({
  providedIn: 'root'
})
export class CuponesService {

  public domain_images: String;

  constructor(private http: HttpClient , private urlService: urlService) {

    this.domain_images = this.urlService.domain_images_berries;
   }


  validarCupon(data){

    return this.http.post<Object[]>(`${this.domain_images}validacionCupon.php`, data, {headers: new HttpHeaders().set('Accept', 'application/json')}).pipe(map(res => res));

   }

   actualizarCupon(data){

    return this.http.post<Object[]>(`${this.domain_images}actualizarCupon.php`, data, {headers: new HttpHeaders().set('Accept', 'application/json')}).pipe(map(res => res));

   }

}
