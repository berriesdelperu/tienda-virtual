import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PasarelaPagoComponent} from '../pasarela-pago/pasarela-pago.component';
import { CanActivateViaAuthGuard } from 'src/app/utils/auth.service';

const routes: Routes = [
  {
    path: '',
    component: PasarelaPagoComponent,
    canActivate: [CanActivateViaAuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PasarelaPagoRoutingModule { }
