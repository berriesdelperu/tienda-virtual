import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'; 
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

/*domain url */
import { urlService} from '../app/urlservice';
/*librerias externas */
import { ModalModule } from 'ngx-bootstrap/modal';
import { Select2Module } from 'ng2-select2';
import { SocialLoginModule, AuthServiceConfig, FacebookLoginProvider } from 'angularx-social-login';
import { AgmCoreModule } from '@agm/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/PageMaster/header/header.component';
import { FooterComponent } from './components/PageMaster/footer/footer.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { AppRoutingModule } from './app-routing.module';
import {ComplementosModule} from './components/complementos/complementos.module';
import {ProductosDestacadosModule} from './components/productos-destacados/productos-destacados.module';
import { EditarDireccionComponent } from './components/Modals/editar-direccion/editar-direccion.component';
import { NopagefoundComponent } from './components/nopagefound/nopagefound.component';
import { AgregarTarjetaComponent } from './components/Modals/agregar-tarjeta/agregar-tarjeta.component';
import { CanActivateViaAuthGuard } from './utils/auth.service';

import { HashLocationStrategy, LocationStrategy } from '@angular/common';

const config = new AuthServiceConfig([
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider('847045749401895')
  }
]);

export function provideConfig() {
  return config;
}


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    InicioComponent,
    EditarDireccionComponent,
    NopagefoundComponent,
    AgregarTarjetaComponent
  ],
  imports: [
  //  BrowserModule,
    BrowserModule.withServerTransition ({ appId : 'berries-del-peru' }),
    AppRoutingModule,
    HttpClientModule,
    SocialLoginModule,
    ComplementosModule,
    ProductosDestacadosModule,
    FormsModule,
    SocialLoginModule,
    RouterModule,
    Select2Module,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCdMfbm6AJUE-9lHGdcF0mSMz0EehAK9qQ',
      libraries: ['places']
    }),
    ModalModule.forRoot(),
    
    
  ],
  providers: [
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    urlService,
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    },
    CanActivateViaAuthGuard
  ],
  entryComponents:[EditarDireccionComponent , AgregarTarjetaComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
