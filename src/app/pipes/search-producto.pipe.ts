import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchProducto'
})
export class SearchProductoPipe implements PipeTransform {

  transform(value: any, search: any): any {
    if  (!search) {return value; }
    let solution = value.filter(v => {
       if ( !v ) {return;}
      return  v.nombre_producto.toLowerCase().indexOf(search.nombre_producto.toLowerCase()) !== -1;
   })
   return solution;
}

}
