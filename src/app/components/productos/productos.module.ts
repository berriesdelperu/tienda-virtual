import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductosRoutingModule } from './productos-routing.module';
import { MermeladasComponent } from './mermeladas/mermeladas.component';
import { CongeladosComponent } from './congelados/congelados.component';
import { FrescosComponent } from './frescos/frescos.component';
import { MielDeAbejaComponent } from './miel-de-abeja/miel-de-abeja.component';
import { DeshidratadosComponent } from './deshidratados/deshidratados.component';
import { PulpasComponent } from './pulpas/pulpas.component';
import{ComplementosModule } from '../complementos/complementos.module';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [MermeladasComponent, CongeladosComponent, FrescosComponent, MielDeAbejaComponent, DeshidratadosComponent, PulpasComponent],
  imports: [
    CommonModule,
    ProductosRoutingModule,
    ComplementosModule,
    RouterModule
  ]
})
export class ProductosModule { }
