import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LibroDeReclamacionesComponent } from './libro-de-reclamaciones.component';

describe('LibroDeReclamacionesComponent', () => {
  let component: LibroDeReclamacionesComponent;
  let fixture: ComponentFixture<LibroDeReclamacionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LibroDeReclamacionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LibroDeReclamacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
