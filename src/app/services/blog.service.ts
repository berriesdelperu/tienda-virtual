import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import {urlService} from '../urlservice';

var headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded').set('Authorization','Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvd3d3LmJlcnJpZXNkZWxwZXJ1LmNvbVwvYmVycmllcy1ibG9nIiwiaWF0IjoxNTgyMzkxMTg2LCJuYmYiOjE1ODIzOTExODYsImV4cCI6MjM1OTk5MTE4NiwiZGF0YSI6eyJ1c2VyIjp7ImlkIjoiMiJ9fX0.RmyUwqrTHhFaOmMQ1M9BiKMkvAAsrjIAYRYsPg_1nBE');

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  public domain: String;

  constructor(private http: HttpClient , private urlService: urlService) {
    this.domain = this.urlService.domain_blog_berries;
    
   }

  
   getCategories(){
    return this.http.get<Object[]>(`${this.domain}categories`,{headers}).pipe(map(res => res));
   }

   getAllArticles(){
    return this.http.get<Object[]>(`${this.domain}posts?_embed`,{headers}).pipe(map(res => res));
    }

   getDetailArticle(id_article,slug){
      return this.http.get<Object[]>(`${this.domain}posts/?include[]=${id_article}&slug=${slug}&_embed`,{headers}).pipe(map(res => res));
      }

    


}
