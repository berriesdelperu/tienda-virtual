import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map } from "rxjs/operators";
import { urlService } from "../urlservice";

var headers = new HttpHeaders()
  .set("Content-Type", "application/json")
  .set("user", "berries")
  .set("key", "I32q4CNRJOP17dwyT689cFvKaaxWOAh5j8VExUBUFBUVqtIdjIuAigN4nFbg");

@Injectable({
  providedIn: "root",
})
export class CompraService {
  public domain: String;

  constructor(private http: HttpClient, private urlService: urlService) {
    this.domain = this.urlService.domain_berries;
  }

  getHistorial(data) {
    return this.http
      .post<Object[]>(`${this.domain}boletas/cliente/`, data, { headers })
      .pipe(map((res) => res));
  }
}
