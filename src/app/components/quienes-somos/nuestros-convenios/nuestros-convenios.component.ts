import { Component, OnInit, AfterViewInit } from "@angular/core";

@Component({
  selector: "app-nuestros-convenios",
  templateUrl: "./nuestros-convenios.component.html",
  styleUrls: ["./nuestros-convenios.component.css"],
})
export class NuestrosConveniosComponent implements OnInit {
  constructor() {}

  ngOnInit() {
    window.scrollTo(0, 0);
    $("#content-tienda").removeClass("header-tienda");
    $(".navbar .nav-item .nav-link").css("color", "#fff");
    $(".navbar .nav-item .dropdown-toggle").removeClass("border-dropdown-gray");
    $(".navbar .nav-item .rounded-market").removeClass("border-button-nav");
    $(".navbar .nav-item .circle-cart").toggleClass("span-circle-cart");
    $("#img-market").attr("src", "assets/img/icons/icon-market.svg");
    $("#img-cart").attr("src", "assets/img/icons/icon-cart.svg");
    $("#img-user").attr("src", "assets/img/icons/icon-user.svg");
  }
}
