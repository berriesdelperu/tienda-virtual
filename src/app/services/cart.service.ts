import { Injectable } from '@angular/core';
import { Producto } from '../models/producto';
import { BehaviorSubject, Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CartService {

  public itemsInCartSubject = new BehaviorSubject<Producto[]>([]);
  public itemsInCart: Producto[] = [];


  public addToCart(item: Producto) {

    this.itemsInCart = JSON.parse(localStorage.getItem("cart"));
    if (!this.itemsInCart) {
      this.itemsInCart = [];
    }
    var indice = -1;
    for (var i = 0; i < this.itemsInCart.length; i++) {
      if (this.itemsInCart[i]["id_pro"] == item["id_pro"]) {
        indice = i;
        break;
      }
    }

    if (indice == -1) {

      /* if(this.itemsInC art.length == 0){
         this.itemsInCart =  JSON.parse(localStorage.getItem("cart"))
       }
       */

      this.itemsInCartSubject.next([...this.itemsInCart, item]);
      this.itemsInCartSubject.subscribe(response => {
        this.itemsInCart = response;
      });

      localStorage.setItem("cart", JSON.stringify(this.itemsInCart));

    } else {
      var cantidadAnterior = this.itemsInCart[indice].cantidad;
      var cantidadExtra = item.cantidad;
      this.itemsInCart[indice].cantidad = cantidadAnterior + cantidadExtra;
      localStorage.setItem("cart", JSON.stringify(this.itemsInCart));
    }



  }


  public removeFromCart(item: Producto) {
    var currentItems = JSON.parse(localStorage.getItem("cart"));
    const itemsWithoutRemoved = currentItems.filter(response =>
      response.id_pro !== item.id_pro
    );
    this.itemsInCartSubject.next(itemsWithoutRemoved);
    localStorage.setItem("cart", JSON.stringify(itemsWithoutRemoved));
  }

  public getItems(): Observable<Producto[]> {
    var aux = JSON.parse(localStorage.getItem("cart"));
    if (aux == null) {
      aux = [];
    }
    this.itemsInCartSubject.next(aux);
    return this.itemsInCartSubject;
  }


}
