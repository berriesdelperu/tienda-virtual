import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TiendaRoutingModule } from './tienda-routing.module';
import { TiendaComponent } from './tienda.component';
import { DetalleProductoComponent } from './detalle-producto/detalle-producto.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComplementosModule } from '../complementos/complementos.module';
import { RouterModule } from '@angular/router';
import { SearchProductoPipe} from '../../pipes/search-producto.pipe';
import { ClickOutsideDirective} from '../../directivas/click-outside.directive';
import { BuscarProductoComponent } from './buscar-producto/buscar-producto.component';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';





@NgModule({
  declarations: [TiendaComponent, DetalleProductoComponent , SearchProductoPipe , ClickOutsideDirective, BuscarProductoComponent],
  imports: [
    CommonModule,
    TiendaRoutingModule,
    FormsModule,
    ComplementosModule,
    RouterModule,
    ReactiveFormsModule,
    AutocompleteLibModule
  
  ]
})
export class TiendaModule { }
