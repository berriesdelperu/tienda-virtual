function culqi() {
  if (Culqi.token) {
    var montoFinal = localStorage.getItem("montoFinal");
    var datosCompra = localStorage.getItem("boleta");
    var montoFixed = parseFloat(montoFinal).toFixed();
    console.log(montoFinal);
    console.log(parseFloat(montoFixed));
    $.ajax({
      url: "https://api.culqi.com/v2/charges",
      type: "POST",
      data: JSON.stringify({
        source_id: Culqi.token.id,
        amount: parseFloat(montoFixed),
        currency_code: "PEN",
        email: Culqi.token.email,
      }),
      contentType: "application/json",
      headers: {
        Accept: "application/json",
        // PROD
        authorization: "Bearer sk_live_52948ada483cef0e",

        // DEV
        // authorization: "Bearer sk_test_3k9bPpPpCUuhHgSw",
      },
      error: function (err) {
        $("#overlay-spinner").fadeOut(300);
        Swal.fire("Opps", err.responseJSON.user_message, "error");
      },

      dataType: "json",
      success: function (data) {
        $("#overlay-spinner").fadeIn(300);

        $.ajax({
          type: "POST",
          url: "https://intranet.berriesdelperu.com/api/v1.0/boletas/generar",
          data: datosCompra,
          dataType: "json",
          headers: {
            user: "berries",
            key: "I32q4CNRJOP17dwyT689cFvKaaxWOAh5j8VExUBUFBUVqtIdjIuAigN4nFbg",
          },
          contentType: "application/json; charset=utf-8",

          success: function (dataCompra) {
            datosTransaccion = {
              nro_operacion: data.id,
              nombres: JSON.parse(localStorage.getItem("datospersona"))[
                "nombres"
              ],
              documento: JSON.parse(localStorage.getItem("datospersona"))[
                "documento"
              ],
              telefono: JSON.parse(localStorage.getItem("datospersona"))[
                "telefono"
              ],
              ruc: "",
              razon_social: "",
              correo_facturacion: data.email,
              departamento: "",
              provincia: "",
              distrito: "",
              productosArray: JSON.parse(localStorage.getItem("boleta"))[
                "productos"
              ],
              direccion_entrega: "",
              referencia: "",
              costo_envio: JSON.parse(localStorage.getItem("boleta"))[
                "precio_envio"
              ],
              sub_total: (data.transfer_amount / 100).toFixed(2),
              igv: (
                (Number(data.total_fee) + Number(data.total_fee_taxes)) /
                100
              ).toFixed(2),
              nro_boleta: dataCompra.success,
            };

            console.log(datosTransaccion);
            $.ajax({
              type: "POST",
              url:
                "https://www.berriesdelperu.com/berries-admin/webservices/api/registrarTransaccion.php",
              data: JSON.stringify(datosTransaccion),
              dataType: "json",
              headers: {
                user: "berries",
                key:
                  "I32q4CNRJOP17dwyT689cFvKaaxWOAh5j8VExUBUFBUVqtIdjIuAigN4nFbg",
                Accept: "application/json",
              },
              contentType: "application/json; charset=utf-8",

              success: function (transData) {
                localStorage.removeItem("montoFinal");
                localStorage.removeItem("cart");
                localStorage.removeItem("boleta");
                Swal.fire({
                  imageUrl: "assets/img/icons/icon-good-pago.svg",
                  imageHeight: 60,
                  html:
                    '<h2 style="font-size:20px;text-align:center;color:#778489;font-weight:bold;">¡Compra exitosa!</h2>',
                  confirmButtonColor: "#8be1b0",
                  buttonsStyling: true,
                  confirmButtonText:
                    '<div style="width:199px; font-size: 14px; font-weight: bold;text-align: center; color: #ffffff; letter-spacing: 1.56px;">DE ACUERDO</div>',
                }).then(() => {
                  window.location.href = "/";
                });
              },

              error: function (err) {
                $("#overlay-spinner").fadeOut(300);
                console.log(err);
                Swal.fire(
                  "Opps",
                  "Error al enviar el correo de confirmación",
                  "error"
                );
              },
            });
          },
          error: function (err) {
            $("#overlay-spinner").fadeOut(300);

            Swal.fire("Opps", "Error en API de intranet", "error");
          },
        });
      },
    });
  } else {
    Swal.fire("Opps", Culqi.token.user_message, "error");
  }
}
