import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuestrosConveniosComponent } from './nuestros-convenios.component';

describe('NuestrosConveniosComponent', () => {
  let component: NuestrosConveniosComponent;
  let fixture: ComponentFixture<NuestrosConveniosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuestrosConveniosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuestrosConveniosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
