import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CarritoComprasComponent} from './carrito-compras.component';
import { CanActivateViaAuthGuard } from 'src/app/utils/auth.service';


const routes: Routes = [
  {
    path: '',
    component: CarritoComprasComponent,
    canActivate: [CanActivateViaAuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CarritoComprasRoutingModule { }
