import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import {urlService} from '../urlservice';

var headers = new HttpHeaders().set('Content-Type','application/json').set('user','berries').set('key','I32q4CNRJOP17dwyT689cFvKaaxWOAh5j8VExUBUFBUVqtIdjIuAigN4nFbg');

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  public domain: String;
  public domain_images: String;

  constructor(private http: HttpClient , private urlService: urlService) {
    this.domain = this.urlService.domain_berries;
    this.domain_images = this.urlService.domain_images_berries;
   }




  getCategories(){
  return this.http.get<Object[]>(`${this.domain}lineas`,{headers}).pipe(map(res => res));
  }

  getAllProducts(){
  return this.http.get<Object[]>(`${this.domain}productos`,{headers}).pipe(map(res => res));
  }

  getProductsByCategoria(id_categoria){

  return this.http.post<Object[]>(`${this.domain}productos/linea`, id_categoria, {headers }).pipe(map(res => res));
    }

  getDetailProduct(id_producto){
    return this.http.post<Object[]>(`${this.domain}productos/producto`, id_producto, {headers }).pipe(map(res => res));
  }
  

 getImageProduct(image){
  return this.http.post<Object[]>(`${this.domain_images}postProducto.php`,image, {headers: new HttpHeaders().set('Accept', 'application/json')}).pipe(map(res => res));
}

  getPortada(){

    return this.http.get<Object[]>(`${this.domain_images}getPortada.php`,{headers: new HttpHeaders().set('Accept', 'application/json')}).pipe(map(res => res));
  }


  /* Productos populares , complementarios y otros */

  getProductosPopulares(){

    return this.http.get<Object[]>(`${this.domain}productos/popular`,{headers}).pipe(map(res => res));

  }

  getProductosComplementarios(){

    return this.http.get<Object[]>(`${this.domain}productos/complemento`,{headers}).pipe(map(res => res));

  }

  getProductosDestacados(){

    return this.http.get<Object[]>(`${this.domain}productos/destacado`,{headers}).pipe(map(res => res));

  }

  getProductsRelacionado(id_producto){

    return this.http.post<Object[]>(`${this.domain}productos/relacionado`, id_producto, {headers }).pipe(map(res => res));
  }

      getProductsPromocion(data){

        return this.http.post<Object[]>(`${this.domain_images}postPromocion.php`,data, {headers: new HttpHeaders().set('Accept', 'application/json')}).pipe(map(res => res));

    }

    getProductDetailImage(data){
      return this.http.post<Object[]>(`${this.domain_images}buscarProducto.php`,data, {headers: new HttpHeaders().set('Accept', 'application/json')}).pipe(map(res => res));

    }

    getProductCaracteristicas(data){

      return this.http.post<Object[]>(`${this.domain_images}postCalorias.php`,data, {headers: new HttpHeaders().set('Accept', 'application/json')}).pipe(map(res => res));

    }


    






  


}
