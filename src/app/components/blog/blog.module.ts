import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlogRoutingModule } from './blog-routing.module';
import { BlogComponent } from './blog.component';
import { DetalleArticuloComponent } from './detalle-articulo/detalle-articulo.component';
import { FormsModule } from '@angular/forms';
import { SearchFiltroPipe} from '../../pipes/search-filtro.pipe';
import {NgxPaginationModule} from 'ngx-pagination';


@NgModule({
  declarations: [BlogComponent, DetalleArticuloComponent,SearchFiltroPipe ],
  imports: [
    CommonModule,
    BlogRoutingModule,
    FormsModule,
    NgxPaginationModule

   
    
  ]
})
export class BlogModule { }
