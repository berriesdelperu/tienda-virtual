import { Component, OnInit } from "@angular/core";
import { ClienteService } from "../../services/cliente.service";
import { Router } from "@angular/router";
import { AuthService } from "angularx-social-login";
import { CompraService } from "src/app/services/compra.service";
import { Subject } from "rxjs";
import { DireccionesService } from "src/app/services/direcciones.service";
import { TarjetasService } from "src/app/services/tarjetas.service";
import { UtilService } from "src/app/utils/util.service";

@Component({
  selector: "app-mi-cuenta",
  templateUrl: "./mi-cuenta.component.html",
  styleUrls: ["./mi-cuenta.component.css"],
})
export class MiCuentaComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  public idpersona: any;
  public nombre_completo: string;
  public correo: string;
  public contraseña: string;
  public telefono: string;
  public clave: string;
  public direccion_principal: string;
  public listaDirecciones: Array<any> = [];
  public usuario: string;
  public historialCompras: any[] = [];
  public dtTrigger: Subject<any> = new Subject();
  public loading: boolean;

  public isExistsDireccion: boolean;
  public isExistsTarjeta: boolean;
  public listTarjetas: Object[];
  public referencia_show: string;
  public distrito_show: string;

  constructor(
    private perfilService: ClienteService,
    private router: Router,
    private authService: AuthService,
    public compraService: CompraService,
    public direccionesService: DireccionesService,
    public tarjetaService: TarjetasService,
    public utilService: UtilService
  ) {
    this.usuario = "";
    this.loading = true;
    this.isExistsDireccion = false;
    this.isExistsTarjeta = false;

    this.referencia_show = "";
    this.distrito_show = "";
  }

  ngOnInit() {
    $(".imgIconMovilmenuFinal").attr("src", "assets/img/icons/icon-menu-2.svg");
    $(".imgIconMoviltiendaFinal").attr(
      "src",
      "assets/img/icons/icon-tienda-2.svg"
    );
    $(".imgIconMovilCartFinal").attr("src", "assets/img/icons/icon-cart-2.svg");
    $(".imgIconMovilUserFinal").attr("src", "assets/img/icons/icon-user-2.svg");

    this.dtOptions = {
      pagingType: "simple_numbers",
      pageLength: 4,
      language: {
        processing: "Procesando...",
        lengthMenu: "Mostrar _MENU_ registros",
        zeroRecords: "No se encontraron resultados",
        emptyTable: "Ningún dato disponible en esta tabla",
        info:
          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        infoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
        infoFiltered: "(filtrado de un total de _MAX_ registros)",
        infoPostFix: "",
        search: "Buscar:",
        url: "",
        loadingRecords: "Cargando...",
        paginate: {
          first: "",
          last: "",
          next:
            "<span class='pagination-fa'><img src='assets/img/icons/icon-next-datatable.svg'/></span>",
          previous: "<span>",
        },
        aria: {
          sortAscending:
            ": Activar para ordenar la columna de manera ascendente",
          sortDescending:
            ": Activar para ordenar la columna de manera descendente",
        },
      },
    };

    window.scrollTo(0, 0);
    $(".content-tienda").addClass("header-tienda");
    $(".navbar .nav-item .nav-link").css("color", "#778489");
    $(".navbar .nav-item .dropdown-toggle").addClass("border-dropdown-gray");
    $(".navbar .nav-item .rounded-market").addClass("border-button-nav");
    $("#img-market").attr("src", "assets/img/icons/icon-market-gray.svg");
    $("#img-cart").attr("src", "assets/img/icons/icon-cart-gray.svg");
    $("#img-user").attr("src", "assets/img/icons/icon-user-gray.svg");

    $(window).resize(function () {
      if ($(window).width() < 500) {
        $(".content-tienda").addClass("header-tienda");
        $(".navbar .nav-item .nav-link").addClass("color-nav");
        $(".navbar .nav-item .dropdown-toggle").addClass(
          "border-dropdown-gray"
        );
        $(".navbar .nav-item .rounded-market").addClass("border-button-nav");
        $("#img-market").attr("src", "assets/img/icons/icon-market-gray.svg");
        $("#icon-cart").attr("src", "assets/img/icons/icon-cart-2.svg");
        $("#img-user").attr("src", "assets/img/icons/icon-user-2.svg");
      }
    });

    var auxiliar = JSON.parse(localStorage.getItem("datospersona"));

    this.idpersona = { id_cli_nat: auxiliar["id_cli_nat"] };

    this.direccion_principal = auxiliar["direccion"];
    this.getPerfil();
    this.listaDireccion();
    this.getTarjetas();
    this.getHistorialCompras();
  }

  getPerfil() {
    this.perfilService.getPerfil(this.idpersona).subscribe(
      (response) => {
        this.nombre_completo = response[0]["nombre_completo"];
        this.correo = response[0]["correo"];
        this.telefono = response[0]["telefono"];
      },
      (error) => {
        console.log(error);
      }
    );

    this.perfilService
      .getPasswordByUsuario(this.idpersona)
      .subscribe((response) => {
        this.clave = response["clave"];
      });
  }

  listaDireccion() {
    let cliente = { cliente: this.idpersona["id_cli_nat"] };

    this.direccionesService.getDireccionByCliente(cliente).subscribe(
      (response) => {
        console.log(response);

        for (var i = 0; i < response.length; i++) {
          if (response[i]["direccion_principal"] == 1) {
            this.direccion_principal =
              response[i]["direccion"] + ", " + response[i]["numero"];
            this.referencia_show = response[i]["referencia_1"];
            this.distrito_show = this.utilService.titleCase(
              response[i]["distrito"]
            );
          } else if (response[i]["direccion_principal"] == 2) {
            if (this.listaDirecciones.length == 0) {
              this.isExistsDireccion = true;
            } else {
              this.isExistsDireccion = false;
            }

            this.listaDirecciones.push({
              id_direccion: response[i]["id_direccion"],
              provincia: response[i]["provincia"],
              distrito: response[i]["distrito"],
              precio_envio: response[i]["precio_envio"],
              direccion: response[i]["direccion"],
              opcion_direccion: response[i]["opcion_direccion"],
              numero: response[i]["numero"],
              referencia_1: response[i]["referencia_1"],
              referencia_2: response[i]["referencia_2"],
              direccion_principal: response[i]["direccion_principal"],
              latitud: "",
              longitud: "",
              mensaje: "envio a domicilio",
            });
          }
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  getTarjetas() {
    let cliente = { id_cli_nat: this.idpersona["id_cli_nat"] };

    this.tarjetaService.getTarjetas(cliente).subscribe(
      (response) => {
        if (response["success"] == true) {
          this.listTarjetas = response["tarjetas"];
        }
        /*else{
        console.log(response["message"]);

      }*/
      },
      (error) => {
        console.log(error);
      }
    );
  }

  getHistorialCompras() {
    var id_cliente = { cliente: this.idpersona["id_cli_nat"] };

    this.compraService.getHistorial(id_cliente).subscribe(
      (response) => {
        console.log(response);

        this.historialCompras = response;

        if (!this.historialCompras) {
          console.log("ocurrio un error al traer los datos del api");
        } else {
          this.loading = false;
        }

        this.dtTrigger.next();
      },
      (error) => {
        // this.historialCompras = [];
        this.loading = false;
        console.log(error);
      }
    );
  }

  goLogout() {
    document.getElementById("mySidenav").style.width = "0";
    $("#opacity").css("width", "0%");
    this.perfilService.logout();
    this.authService.signOut();
    this.router.navigate(["/"]);
  }
}
