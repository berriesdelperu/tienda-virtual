import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FormularioComponent} from './formulario/formulario.component';
import { LibroDeReclamacionesComponent } from './libro-de-reclamaciones/libro-de-reclamaciones.component';

const routes: Routes = [
  { path: '',
   redirectTo: '/no-encontrado', 
   pathMatch: 'full' 
  },
  {
    path: 'formulario',
    component: FormularioComponent
  },
  {
    path: 'libro-de-reclamaciones',
    component: LibroDeReclamacionesComponent
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContactoRoutingModule { }
