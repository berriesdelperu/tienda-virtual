import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MermeladasComponent} from './mermeladas/mermeladas.component';
import {CongeladosComponent} from './congelados/congelados.component';
import {FrescosComponent} from './frescos/frescos.component';
import {MielDeAbejaComponent} from './miel-de-abeja/miel-de-abeja.component';
import {DeshidratadosComponent} from './deshidratados/deshidratados.component';
import {PulpasComponent} from './pulpas/pulpas.component';


const routes: Routes = [
  { path: '',
   redirectTo: '/no-encontrado', 
   pathMatch: 'full' 
  },
  {
    path: 'mermeladas',
    component: MermeladasComponent
  },
  {
    path: 'congelados',
    component: CongeladosComponent
  },
  {
    path: 'frescos',
    component: FrescosComponent
  },
  {
    path: 'miel-de-abeja',
    component: MielDeAbejaComponent
  },
  {
    path: 'deshidratados',
    component: DeshidratadosComponent
  },
  {
    path: 'pulpas',
    component: PulpasComponent
  }
];







@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductosRoutingModule { }
