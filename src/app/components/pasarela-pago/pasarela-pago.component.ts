import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import { PlatformLocation } from "@angular/common";
import Swal from "sweetalert2";
import Inputmask from "inputmask";
import { UtilService } from "src/app/utils/util.service";
import { TarjetasService } from "src/app/services/tarjetas.service";
import { FormBuilder } from "@angular/forms";

declare var Culqi: any;
declare var $: any;

@Component({
  selector: "app-pasarela-pago",
  templateUrl: "./pasarela-pago.component.html",
  styleUrls: ["./pasarela-pago.component.css"],
})
export class PasarelaPagoComponent implements OnInit {
  public montoAPagar: number;
  public boleta: Object[];
  public primer_valor_tarjeta: string;
  public num_tarjeta: string;
  public save_card: boolean;
  public mes_tarjeta: string;
  public year_tarjeta: string;
  public cvv_tarjeta: string;
  public titular_tarjeta: string;
  public listTarjetas: Object[];
  public storageDatosUsuario: any;

  @ViewChild("auto", { static: false }) auto;

  maxLength: number;

  keyword = "last_four_digits";

  autocomplete: any;

  constructor(
    public utilService: UtilService,
    public location: PlatformLocation,
    public tarjetaService: TarjetasService,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    $(".imgIconMovilmenuFinal").attr("src", "assets/img/icons/icon-menu-2.svg");
    $(".imgIconMoviltiendaFinal").attr(
      "src",
      "assets/img/icons/icon-tienda-2.svg"
    );
    $(".imgIconMovilCartFinal").attr("src", "assets/img/icons/icon-cart-2.svg");
    $(".imgIconMovilUserFinal").attr("src", "assets/img/icons/icon-user-2.svg");

    window.scrollTo(0, 0);

    this.save_card = false;
    this.montoAPagar = 0;
    this.num_tarjeta = "";
    this.mes_tarjeta = "";
    this.year_tarjeta = "";
    this.cvv_tarjeta = "";
    this.titular_tarjeta = "";
    this.primer_valor_tarjeta = "";

    this.storageDatosUsuario = JSON.parse(localStorage.getItem("datospersona"));

    this.titular_tarjeta = this.storageDatosUsuario["correo"];

    this.autocompleteTarjeta();

    $(".content-tienda").addClass("header-tienda");
    $(".navbar .nav-item .nav-link").css("color", "#778489");
    $(".navbar .nav-item .dropdown-toggle").addClass("border-dropdown-gray");
    $(".navbar .nav-item .rounded-market").addClass("border-button-nav");
    $("#img-market").attr("src", "assets/img/icons/icon-market-gray.svg");
    $("#img-cart").attr("src", "assets/img/icons/icon-cart-gray.svg");
    $("#img-user").attr("src", "assets/img/icons/icon-user-gray.svg");

    $(window).resize(function () {
      if ($(window).width() < 500) {
        $(".content-tienda").addClass("header-tienda");
        $(".navbar .nav-item .nav-link").addClass("color-nav");
        $(".navbar .nav-item .dropdown-toggle").addClass(
          "border-dropdown-gray"
        );
        $(".navbar .nav-item .rounded-market").addClass("border-button-nav");
        $("#img-market").attr("src", "assets/img/icons/icon-market-gray.svg");
        $("#icon-cart").attr("src", "assets/img/icons/icon-cart-2.svg");
        $("#img-user").attr("src", "assets/img/icons/icon-user-2.svg");
      }
    });

    this.autocomplete = $(".autocomplete-container .input-container input");

    Inputmask({ mask: "9999 9999 9999 9999", placeholder: "" }).mask(
      this.autocomplete
    );

    // PROD
    Culqi.publicKey = "pk_live_a64b0b12703e1583";

    // DEV
    // Culqi.publicKey = "pk_test_o60VGILl7rvbPfvk";

    Culqi.init();

    this.montoAPagar = parseFloat(localStorage.getItem("montoFinal")) / 100;

    this.location.onPopState(() => {
      localStorage.removeItem("montoFinal");
      localStorage.removeItem("boleta");
    });
  }

  onChangeSearch(val: string) {
    this.num_tarjeta = val;
  }

  selectEvent(item) {
    this.num_tarjeta = item.nro_tarjeta;
    this.mes_tarjeta = item.mes;
    this.year_tarjeta = item.year;
    this.autocomplete.attr("disabled", true);
    $("#defaultChecked2").attr("disabled", true);
    Inputmask({ mask: item.last_four_digits, value: "" }).mask(
      this.autocomplete
    );
  }

  onClear() {
    this.autocomplete.removeAttr("disabled", true);
    this.mes_tarjeta = "";
    this.year_tarjeta = "";
    $("#defaultChecked2").removeAttr("disabled", true);

    Inputmask({ mask: "9999 9999 9999 9999", placeholder: "" }).mask(
      this.autocomplete
    );
  }

  changeSaveCard(event) {
    this.save_card = event.target.checked;
  }

  autocompleteTarjeta() {
    var id_cliente = this.storageDatosUsuario;

    let cliente = { id_cli_nat: id_cliente["id_cli_nat"] };

    this.tarjetaService.getTarjetas(cliente).subscribe(
      (response) => {
        if (response["success"] == true) {
          this.listTarjetas = response["tarjetas"];
        }
        /* else {
        console.log(response["message"]);

      }*/
      },
      (error) => {
        console.log(error);
      }
    );
  }

  validarSoloNumerosCVV(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    if (event.target.value.length < 4) {
      return true;
    } else {
      return false;
    }
  }

  validarSoloNumerosFecha(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    if (event.target.value.length < 2) {
      return true;
    } else {
      return false;
    }
  }

  pagar(e) {
    console.log(e);

    if (this.num_tarjeta == "") {
      Swal.fire({
        title:
          "<img src='assets/img/tiendas/alerttarejtacredito.png' style=' height:100px;margin-bottom:20px' />",
        html: "Ingrese el número de su tarjeta",
        confirmButtonText: "DE ACUERDO",
      });

      return false;
    } else if (this.mes_tarjeta == "") {
      Swal.fire({
        title:
          "<img src='assets/img/tiendas/alerttarejtacredito.png' style='height:100px;margin-bottom:20px' />",
        html: "Ingrese el mes que indica en su tarjeta",
        confirmButtonText: "DE ACUERDO",
      });

      return false;
    } else if (this.year_tarjeta == "") {
      Swal.fire({
        title:
          "<img src='assets/img/tiendas/alerttarejtacredito.png' style='height:100px;margin-bottom:20px' />",
        html: "Ingrese el año de que indica en su tarjeta",
        confirmButtonText: "DE ACUERDO",
      });

      return false;
    } else if (this.cvv_tarjeta == "") {
      Swal.fire({
        title:
          "<img src='assets/img/tiendas/alerttarejtacredito.png' style='height:100px;margin-bottom:20px' />",
        html:
          "Ingrese el CVV que se encuentra en la pate posterior de su tarjeta",
        confirmButtonText: "DE ACUERDO",
      });

      return false;
    } else if (!this.utilService.validateEmail(this.titular_tarjeta)) {
      Swal.fire({
        title:
          "<img src='assets/img/tiendas/iconocorreoalert.png' style='height:100px;margin-bottom:20px' />",
        html: "Ingrese un correo electrónico válido",
        confirmButtonText: "DE ACUERDO",
      });

      return false;
    } else {
      if (this.save_card) {
        var space_blank_tarjeta = this.num_tarjeta.replace(/\s/g, "");
        var aux_num = this.num_tarjeta.substr(-5);

        let data = {
          nro_tarjeta: space_blank_tarjeta,
          mes: this.mes_tarjeta,
          year: this.year_tarjeta,
          last_four_digits: "XXXX XXXX XXXX" + aux_num,
          id_cli_nat: this.storageDatosUsuario.id_cli_nat,
        };

        this.tarjetaService.registrarTarjetas(data).subscribe(
          (response) => {
            console.log(response);
          },
          (error) => {
            console.log(error);
          }
        );
      }

      Culqi.createToken();

      $("#overlay-spinner").fadeIn(300);

      e.stopPropagation();
    }
  }
}
