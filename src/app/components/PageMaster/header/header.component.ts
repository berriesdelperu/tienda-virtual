import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  NgZone,
  AfterViewInit,
  ViewChildren,
} from "@angular/core";
import { formatDate } from "@angular/common";
import { Router } from "@angular/router";
import { MapsAPILoader, MouseEvent } from "@agm/core";
import { Observable, of } from "rxjs";
import Swal from "sweetalert2";
import { ClienteService } from "../../../services/cliente.service";
import {
  AuthService,
  FacebookLoginProvider,
  SocialUser,
} from "angularx-social-login";
import { Producto } from "../../../models/producto";
import { CartService } from "../../../services/cart.service";
import { UtilService } from "../../../utils/util.service";
import { Select2OptionData } from "ng2-select2";

declare var $: any;

import Stepper from "bs-stepper";
import { DireccionesService } from "src/app/services/direcciones.service";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"],
})
export class HeaderComponent implements OnInit, AfterViewInit {
  public stepper1: any;
  public usuario: string;
  public clave: string;
  public enlaces = [];

  public nombres_completo: string;
  public dni: string;
  public telefono: string;
  public correo: string;
  public fecha_registro: string;

  public userFB: SocialUser;
  public loggedInFB: boolean;
  public today = new Date();
  public montoSubTotal;
  public jstoday = "";

  public shoppingCartItems$: Observable<Producto[]> = of([]);
  public shoppingCartItems;
  public id_cli_nat: string;

  public nombres_registro: string;
  public apellidos_registro: string;
  public documento_registro: string;
  public clave_registro: string;
  public correo_registro: string;
  public telefono_registro: string;
  public direccion_entrega: string;
  public detalle_direccion: string;
  public referencia: string;
  public recuperar_correo: string;

  public auxresponse: any;

  public listPrecioEnvio: Object[];

  public listProvincias: Array<Select2OptionData>;
  public optionsSelect2Provincias: Select2Options;
  public selectProvincia: string;

  public listDistritos: Array<Select2OptionData>;
  public optionsSelect2Distritos: Select2Options;
  public selectDistrito: string;

  public listOpcionDireccion: Array<Select2OptionData>;
  public optionsSelect2TipoDireccion: Select2Options;
  public selectTipoDireccion: string;

  public numero_direccion: string;
  public direccion_principal: string;

  public latitude: number;
  public longitude: number;
  public zoom: number;
  public address: string;
  private geoCoder;
  public itemDate = 0;

  public btnStepperContinuar: boolean = true;

  @ViewChild("search", { static: true })
  public searchElementRef: ElementRef;
  public direccion_storage: string;
  public numero_direccion_storage: string;
  public arrayLocalStorage: any = {};

  public isButtonLoginDisabled: Boolean = true;

  public aux1: string;
  public aux2: string;

  @ViewChildren(".input-primer-stepper") inputs;

  constructor(
    private router: Router,
    public clienteService: ClienteService,
    private authService: AuthService,
    private cartService: CartService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private direccionesService: DireccionesService,
    public utilService: UtilService,
    private elementRef: ElementRef
  ) {
    this.optionsSelect2Provincias = {
      placeholder: "Provincia",
      width: "100%",
    };

    this.optionsSelect2Distritos = {
      placeholder: "Distrito",
      width: "100%",
    };

    this.optionsSelect2TipoDireccion = {
      placeholder: "Tipo",
      width: "100%",
    };

    this.clienteService.stateLogin();

    this.shoppingCartItems$ = this.cartService.getItems();

    this.shoppingCartItems$.subscribe((response) => {
      this.shoppingCartItems = response;
    });

    this.usuario = "";
    this.clave = "";
    this.nombres_completo = "";
    this.dni = "";
    this.telefono = "";
    this.correo = "";
    this.fecha_registro = "";
    this.nombres_registro = "";
    this.apellidos_registro = "";
    this.documento_registro = "";
    this.clave_registro = "";
    this.correo_registro = "";
    this.telefono_registro = "";
    this.direccion_entrega = "";
    this.detalle_direccion = "";
    this.id_cli_nat = "";
    this.referencia = "";
    this.recuperar_correo = "";
    this.selectProvincia = "";
    this.selectDistrito = "";
    this.selectTipoDireccion = "";
    this.numero_direccion = "";
    this.direccion_principal = "";
    this.aux1 = "";
    this.aux2 = "";

    this.jstoday = formatDate(this.today, "yyyy-MM-dd", "en-US");

    this.enlaces = [
      {
        inicio: [
          {
            path: "/",
            nombre: "Inicio",
          },
        ],
        productos: [
          {
            path: "/productos/mermeladas",
            nombre: "Mermeladas",
          },
          {
            path: "/productos/congelados",
            nombre: "Congelados",
          },
          /*  {
              path: "/productos/frescos",
              nombre: "Frescos"
            },
            */
          {
            path: "/productos/miel-de-abeja",
            nombre: "Miel de abeja",
          },
          /*  {
              path: "/productos/deshidratados",
              nombre: "Deshidratados"
            },
            */
          {
            path: "/productos/pulpas",
            nombre: "Pulpas",
          },
        ],
        qsomos: [
          {
            path: "/quienes-somos/la-empresa",
            nombre: "La empresa",
          },
          {
            path: "/quienes-somos/nuestros-convenios",
            nombre: "Nuestros convenios",
          },
        ],
        contacto: [
          {
            path: "/contacto/formulario",
            nombre: "Formulario",
          },
          {
            path: "/contacto/libro-de-reclamaciones",
            nombre: "Libro de reclamaciones",
          },
        ],
        tienda: [
          {
            path: "/tienda",
            nombre: "Tienda",
          },
        ],
      },
    ];
  }

  ngAfterViewChecked(): void {
    //Called after every check of the component's view. Applies to components only.
    //Add 'implements AfterViewChecked' to the class.

  }

  ngOnInit() {
    this.autocomplete();
    this.getProvincias();
    this.getOpcionDireccion();

    this.authService.authState.subscribe((user) => {
      this.userFB = user;
      this.loggedInFB = user != null;
      if (user != null) {
        this.nombres_completo = this.userFB.name;
        this.correo = this.userFB.email;
        var tipo_logueo = "2";

        this.datosFB(
          tipo_logueo,
          this.id_cli_nat,
          this.nombres_completo,
          this.clave,
          this.dni,
          this.telefono,
          this.correo
        );
      }
    });

    $(document).ready(function () {


      $(window).on("scroll", function () {
        var statusGeneral = false;

        if (window.location.href.indexOf("tienda") > -1) {
          statusGeneral = true;
        }
        if (window.location.href.indexOf("mi-cuenta") > -1) {
          statusGeneral = true;
        }
        if (window.location.href.indexOf("blog") > -1) {
          statusGeneral = true;
        }
        if (window.location.href.indexOf("carrito-compras") > -1) {
          statusGeneral = true;
        }
        if (window.location.href.indexOf("pasarela-pago") > -1) {
          statusGeneral = true;
        }
        if (!statusGeneral) {
          if ($(this).scrollTop() > 50) {

            $(".navigationFixed").addClass("fondito-berries");
            $(".imgIconMovilmenuFinal").attr(
              "src",
              "assets/img/icons/icon-menu-2.svg"
            );
            $(".imgIconMoviltiendaFinal").attr(
              "src",
              "assets/img/icons/icon-tienda-2.svg"
            );
            $(".imgIconMovilCartFinal").attr(
              "src",
              "assets/img/icons/icon-cart-2.svg"
            );
            $(".imgIconMovilUserFinal").attr(
              "src",
              "assets/img/icons/icon-user-2.svg"
            );


            $(".content-tienda").removeClass('header-tienda');
            $('.navbar .nav-item .nav-link').css('color', "#778489");
            $('.navbar .nav-item .dropdown-toggle').addClass('border-dropdown-gray');
            $('.navbar .nav-item .rounded-market').addClass('border-button-nav');;
            $("#img-market").attr("src", "assets/img/icons/icon-market-gray.svg");
            $("#img-cart").attr("src", "assets/img/icons/icon-cart-gray.svg");
            $("#img-user").attr("src", "assets/img/icons/icon-user-gray.svg");
            document.getElementById("scrolling-navbar").style.background = "white"

          } else {

            $(".imgIconMovilmenuFinal").attr(
              "src",
              "assets/img/icons/icon-menu.svg"
            );
            $(".imgIconMoviltiendaFinal").attr(
              "src",
              "assets/img/icons/icon-tienda.svg"
            );
            $(".imgIconMovilCartFinal").attr(
              "src",
              "assets/img/icons/icon-cart.svg"
            );
            $(".imgIconMovilUserFinal").attr(
              "src",
              "assets/img/icons/icon-user.svg"
            );
            $(".navigationFixed").removeClass("fondito-berries");
            $(".content-tienda").removeClass('header-tienda');
            $('.navbar .nav-item .nav-link').css('color', "#ffffff");
            $('.navbar .nav-item .dropdown-toggle').removeClass('border-dropdown-gray');
            $('.navbar .nav-item .rounded-market').removeClass('border-button-nav');
            $("#img-market").attr("src", "assets/img/icons/icon-market.svg");
            $("#img-cart").attr("src", "assets/img/icons/icon-cart.svg");
            $("#img-user").attr("src", "assets/img/icons/icon-user.svg");
            document.getElementById("scrolling-navbar").style.background = "transparent"

          }
        } else {

          $(".content-tienda").addClass('header-tienda');
          $('.navbar .nav-item .nav-link').css('color', "#778489");
          $('.navbar .nav-item .dropdown-toggle').addClass('border-dropdown-gray');
          $('.navbar .nav-item .rounded-market').addClass('border-button-nav');
          $("#img-market").attr("src", "assets/img/icons/icon-market-gray.svg");
          $("#img-cart").attr("src", "assets/img/icons/icon-cart-gray.svg");
          $("#img-user").attr("src", "assets/img/icons/icon-user-gray.svg");
          $(".navigationFixed").addClass("fondito-berries");

          $(".imgIconMovilmenuFinal").attr(
            "src",
            "assets/img/icons/icon-menu-2.svg"
          );

          $(".imgIconMoviltiendaFinal").attr(
            "src",
            "assets/img/icons/icon-tienda-2.svg"
          );
          $(".imgIconMovilCartFinal").attr(
            "src",
            "assets/img/icons/icon-cart-2.svg"
          );
          $(".imgIconMovilUserFinal").attr(
            "src",
            "assets/img/icons/icon-user-2.svg"
          );
          if ($(this).scrollTop() > 50) {

            document.getElementById("scrolling-navbar").style.background = "white"

          } else {
            $(".navigationFixed").removeClass("fondito-berries");
          }


        }
      });

      $("#menu-button").click(function (event) {
        event.stopPropagation();
        //  $(".navegacion nav").slideToggle("fast");
        $(".navegacion nav").animate({ left: "0" });
      });

      $(".navegacion nav").on("click", function (event) {
        event.stopPropagation();
      });

      $(document).on("click", function () {
        $(".navegacion nav").animate({ left: "-100%" });
      });

      // Agregando y Eliminando Clase nav-responsive
      var wd = $(window).width();

      if (wd <= 700) {
        $(".navegacion nav").addClass("nav-responsive");
      } else {
        $(".navegacion nav").removeClass("nav-responsive");
      }

      $(window).resize(function () {
        var wdi = $(window).width();

        if (wdi <= 700) {
          $(".navegacion nav").addClass("nav-responsive");
        } else {
          $(".navegacion nav").removeClass("nav-responsive");
          $(".navegacion nav").css({ position: "" });
        }
      });

      var navHeight = $(".navegacion").height();

      $(".navegacion").css({ position: "absolute", top: "0" });
      $("body").css({ "padding-top": navHeight });

      $(".submenu").click(function () {
        $(this).children(".children").slideToggle();
      });
    });

    document.addEventListener("DOMContentLoaded", function () {
      this.stepper1 = new Stepper(document.querySelector(".bs-stepper"));
    });

    $(".toggle-password").click(function () {
      var $pwd = $(".input-password");
      if ($pwd.attr("type") === "password") {
        $pwd.attr("type", "text");
      } else {
        $pwd.attr("type", "password");
      }
    });
  }

  ngAfterViewInit() {
    this.validateStepper1();
    // $('#modalRegistrar').modal('handleUpdate')
    // $('.isInModal').select2({
    //   dropdownParent: $('#modalRegistrar')
    // });
  }

  listDataProductInfo() {
    var item = $("#listProducto1Nav").hasClass("fa-chevron-down");
    if (item) {
      $("#listProducto1Nav").removeClass("fa-chevron-down");
      $("#listProducto1Nav").addClass("fa-chevron-up");
    } else {
      $("#listProducto1Nav").removeClass("fa-chevron-up");
      $("#listProducto1Nav").addClass("fa-chevron-down");
    }
  }

  listDataProductInfo2() {
    var item = $("#listProducto1Nav2").hasClass("fa-chevron-down");
    if (item) {
      $("#listProducto1Nav2").removeClass("fa-chevron-down");
      $("#listProducto1Nav2").addClass("fa-chevron-up");
    } else {
      $("#listProducto1Nav2").removeClass("fa-chevron-up");
      $("#listProducto1Nav2").addClass("fa-chevron-down");
    }
  }

  listDataProductInfo3() {
    var item = $("#listProducto1Nav3").hasClass("fa-chevron-down");
    if (item) {
      $("#listProducto1Nav3").removeClass("fa-chevron-down");
      $("#listProducto1Nav3").addClass("fa-chevron-up");
    } else {
      $("#listProducto1Nav3").removeClass("fa-chevron-up");
      $("#listProducto1Nav3").addClass("fa-chevron-down");
    }
  }

  validateStepper1() {
    var inputs = this.elementRef.nativeElement.querySelectorAll(
      ".input-primer-stepper"
    );

    var valid = true;

    for (var i = 0; i < inputs.length; i++) {
      if (inputs[i].value == "") {
        valid = false;
      }

      inputs[i].addEventListener("change", function (evt) {
        if (evt.target.value != "") {
          $("#campos-faltantes").hide();
          $("#btn-continuar-stepper1").css("background", "#8be1b0");
        } else {
          valid = false;
        }
      });
    }

    return valid;
  }

  stepperContinuar() {
    if (!this.validateStepper1()) {
      $("#btn-continuar-stepper1").css("background", "#cad2d5");
      $("#campos-faltantes").show();
      return false;
    } else {
      var stepperk = new Stepper(document.querySelector("#stepperForm"));
      stepperk.to(2);
      $(".modal-body").scrollTop = 0
    }
  }

  showClave() {
    var $pwd = $(".input-password-other");
    if ($pwd.attr("type") === "password") {
      $pwd.attr("type", "text");
    } else {
      $pwd.attr("type", "password");
    }
  }

  autocomplete() {
    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation();
      this.geoCoder = new google.maps.Geocoder();

      let autocomplete = new google.maps.places.Autocomplete(
        this.searchElementRef.nativeElement,
        {
          types: ["address"],
          componentRestrictions: { country: "PE" },
        }
      );
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          this.direccion_principal = place.formatted_address;

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 15;
        });
      });
    });
  }

  private setCurrentLocation() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 15;
        this.getAddress(this.latitude, this.longitude);
      });
    }
  }

  markerDragEnd($event: MouseEvent) {
    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    this.getAddress(this.latitude, this.longitude);
  }

  getAddress(latitude, longitude) {
    this.geoCoder.geocode(
      { location: { lat: latitude, lng: longitude } },
      (results, status) => {
        if (status === "OK") {
          if (results[0]) {
            this.zoom = 15;
            this.address = results[0].formatted_address;
            this.direccion_principal = this.address;
          } else {
            window.alert("No se encontraron resultados GEOCODER");
          }
        } else {
          window.alert("Geocoder fallido : " + status);
        }
      }
    );
  }

  redireccionNavMobile(valuepath) {
    $(".boton-menu strong")
      .removeClass("icon icon-cancel")
      .addClass("icon icon-menu");
    $(".navegacion nav").animate({ left: "-100%" });

    this.router.navigate([valuepath]);
  }

  openMyAccount() {
    if (!this.clienteService.isLoggedin) {
      document.getElementById("mySidenav").style.width = "320px";
      $("#opacity").css("width", "100%");
    } else {
      this.router.navigate(["/mi-cuenta"]);
    }
  }
  openMyAccountMovil() {
    if (!this.clienteService.isLoggedin) {
      document.getElementById("mySidenavMovil").style.width = "320px";
      $("#opacityMovil").css("width", "100%");
    } else {
      this.router.navigate(["/mi-cuenta"]);
    }
  }
  closeMyAccount() {
    document.getElementById("mySidenav").style.width = "0";
    $("#opacity").css("width", "0%");
  }

  closeMyAccountMovil() {
    document.getElementById("mySidenavMovil").style.width = "0";
    $("#opacityMovil").css("width", "0%");
  }

  goCuentaDesktop() {
    document.getElementById("mySidenav").style.width = "0";
    $("#opacity").css("width", "0%");
    this.router.navigate(["/mi-cuenta"]);
  }

  goCuentaMovil() {
    document.getElementById("mySidenavMovil").style.width = "0";
    $("#opacityMovil").css("width", "0%");
    this.router.navigate(["/mi-cuenta"]);
  }

  openCart() {
    this.refrescarDatos();
    document.getElementById("navCart").style.width = "420px";
    $("#opacity").css("width", "100%");

    if (this.clienteService.isLoggedin == false) {
      this.isButtonLoginDisabled = true;
    } else {
      this.isButtonLoginDisabled = false;
    }
  }

  openCartMovil() {
    document.getElementById("navCartMovil").style.width = "320px";
    $("#opacityMovil").css("width", "100%");

    if (this.clienteService.isLoggedin == false) {
      this.isButtonLoginDisabled = true;
    } else {
      this.isButtonLoginDisabled = false;
    }
  }

  closeCart() {
    document.getElementById("navCart").style.width = "0";
    $("#opacity").css("width", "0%");
  }

  closeCartMovil() {
    document.getElementById("navCartMovil").style.width = "0";
    $("#opacityMovil").css("width", "0%");
  }

  goLogin() {
    if (this.usuario == "") {
      Swal.fire({
        title:
          "<img src='assets/img/tiendas/iconocorreoalert.png' style='height:100px;margin-bottom:20px' />",
        html: "Por favor, ingrese su correo",
        confirmButtonText: "DE ACUERDO",
      });

      return false;
    } else {
      let data = { correo: this.usuario };

      this.clienteService.getLoginByInfo(data).subscribe(
        (response) => {
          this.nombres_completo = response[0]["nombre_completo"];
          this.dni = response[0]["dni"];
          this.telefono = response[0]["telefono"];
          this.correo = response[0]["correo"];
          this.fecha_registro = response[0]["fecha_registro"];
          this.id_cli_nat = response[0]["id_cli_nat"];
          //$("#overlay-spinner1").show();

          this.clienteService
            .login(
              "1",
              this.id_cli_nat,
              this.nombres_completo,
              this.clave,
              this.dni,
              this.telefono,
              this.correo
            )
            .subscribe(
              (response1) => {
                $("#overlay-spinner1").show();

                if (response1["success"] == true) {
                  $("#overlay-spinner1").hide();
                  document.getElementById("mySidenav").style.width = "0";
                  $("#opacity").css("width", "0%");
                  document.getElementById("mySidenavMovil").style.width = "0";
                  $("#opacityMovil").css("width", "0%");

                  this.arrayLocalStorage = {
                    id_cli_nat: response1["datos"]["id_cli_nat"],
                    nombres:
                      response1["datos"]["nombres"] +
                      " " +
                      response1["datos"]["apellidos"],
                    correo: response1["datos"]["correo"],
                    documento: response1["datos"]["documento"],
                    telefono: response1["datos"]["telefono"],
                  };

                  localStorage.setItem(
                    "datospersona",
                    JSON.stringify(this.arrayLocalStorage)
                  );
                  // this.isButtonLoginDisabled = false;
                  this.clienteService.stateLogin();
                  this.usuario = "";
                  this.clave = "";

                  // if (JSON.parse(localStorage.getItem("cart")).length == 0 ||  JSON.parse(localStorage.getItem("cart")) == null) {
                  if (localStorage.getItem("cart") == null) {
                    this.router.navigate(["/mi-cuenta"]);
                  } else {
                    this.router.navigate(["/carrito-compras"]);
                  }
                } else {
                  $("#overlay-spinner1").hide();

                  Swal.fire("Oops...", response1["message"], "error");
                }
              },
              (error) => {
                Swal.fire(
                  "Oops...",
                  "Surgio un error , intente de nuevo por favor",
                  "error"
                );
              }
            );
        },
        (error) => {
          Swal.fire("Oops...", "El correo indicado no se encuentra", "error");
        }
      );

      //  $("#overlay-spinner1").hide();
    }
  }

  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  datosFB(
    tipo_logueo,
    id_cli_nat,
    nombre_completo,
    clave,
    dni,
    telefono,
    correo
  ) {
    let data = { correo: correo };

    this.clienteService.getLoginByInfo(data).subscribe(
      (response) => {
        this.nombres_completo = response[0]["nombre_completo"];
        this.dni = response[0]["dni"];
        this.telefono = response[0]["telefono"];
        this.correo = response[0]["correo"];
        this.fecha_registro = response[0]["fecha_registro"];
        this.id_cli_nat = response[0]["id_cli_nat"];

        document.getElementById("mySidenav").style.width = "0";
        $("#opacity").css("width", "0%");
        document.getElementById("mySidenavMovil").style.width = "0";
        $("#opacityMovil").css("width", "0%");

        localStorage.setItem("datospersona", JSON.stringify(response[0]));
        this.clienteService.stateLogin();

        this.clienteService
          .login(
            tipo_logueo,
            id_cli_nat,
            nombre_completo,
            clave,
            dni,
            telefono,
            correo
          )
          .subscribe(
            (response1) => {
              if (response1["success"] == true) {
                if (localStorage.getItem("cart") == null) {
                  this.router.navigate(["/mi-cuenta"]);
                } else {
                  this.router.navigate(["/carrito-compras"]);
                }
              } else {
                Swal.fire("Oops...", response1["message"], "error");
              }
            },
            (error) => {
              console.log(error);
            }
          );
      },
      (error) => {
        if ((error["error"].error = "EL CLIENTE NO EXISTE")) {
          var datos_registro_berries = {
            fecha_registro: this.jstoday,
            dni: "",
            nombre_completo: nombre_completo,
            sexo: "",
            celular: "",
            telefono: "",
            correo: correo,
            recibir_oferta: "2",
          };

          this.clienteService
            .registerUserBDBerries(datos_registro_berries)
            .subscribe(
              (response) => {
                var data_registro_admin = {
                  nombres: nombre_completo,
                  apellidos: "",
                  documento: "",
                  telefono: "",
                  correo: correo,
                  clave: "berries2020",
                  direccion: "",
                  referencia: "",
                  latitud: "",
                  longitud: "",
                  id_cli_nat: response[0]["id_cli_nat"],
                };

                this.clienteService
                  .registarUserBDAdmin(data_registro_admin)
                  .subscribe((response1) => {
                    if (response1["success"] == true) {
                      document.getElementById("mySidenav").style.width = "0";
                      $("#opacity").css("width", "0%");
                      document.getElementById("mySidenavMovil").style.width =
                        "0";
                      $("#opacityMovil").css("width", "0%");

                      localStorage.setItem(
                        "datospersona",
                        JSON.stringify(response[0])
                      );
                      this.clienteService.stateLogin();
                      this.usuario = "";
                      this.clave = "";

                      if (localStorage.getItem("cart") == null) {
                        this.router.navigate(["/mi-cuenta"]);
                      } else {
                        this.router.navigate(["/carrito-compras"]);
                      }
                    } else {
                      Swal.fire("Oops...", response1["message"], "error");
                    }
                  });
              },
              (error) => {
                console.log(error);
              }
            );
        }
      }
    );
  }

  goLogout() {
    document.getElementById("mySidenav").style.width = "0";
    $("#opacity").css("width", "0%");
    document.getElementById("mySidenavMovil").style.width = "0";
    $("#opacityMovil").css("width", "0%");
    this.clienteService.logout();
    this.authService.signOut();
    this.router.navigate(["/"]);
    this.usuario = "";
    this.clave = "";
  }

  getProvincias() {
    this.direccionesService.getProvincias().subscribe(
      (response) => {
        var arrayAuxReg = [];
        arrayAuxReg.push({ id: "", text: "" });

        for (let index = 0; index < response["length"]; index++) {
          arrayAuxReg.push({
            id: response[index]["id_provincia"],
            text: response[index]["provincia"],
          });
        }
        this.listProvincias = arrayAuxReg;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  changeProvincias($event) {
    this.listDistritos = null
    this.selectProvincia = $event.value;
    let provincia = { id_provincia: this.selectProvincia };
    this.direccion_principal = ""
    this.numero_direccion = ""
    this.referencia = ""
    $('#selectTipo select').val(null).trigger('change')
    this.getDistritos(provincia);
  }

  changeDistritos($event) {
    this.selectDistrito = $event.value;

    let distrito = { id_distrito: this.selectDistrito };
    this.direccion_principal = ""
    this.numero_direccion = ""
    this.referencia = ""
    $('#selectTipo select').val(null).trigger('change')

    this.getPrecioxDireccion(distrito);
  }


  getDistritos(provincia) {
    this.direccionesService.getDistritos(provincia).subscribe(
      (response) => {
        var arrayAuxReg = [];
        arrayAuxReg.push({ id: "", text: "" });

        for (let index = 0; index < response["length"]; index++) {
          arrayAuxReg.push({
            id: response[index]["id_distrito"],
            text: response[index]["distrito"],
          });
        }
        this.listDistritos = arrayAuxReg;
        console.log(this.listDistritos)
      },
      (error) => {
        console.log(error);
      }
    );
  }


  getPrecioxDireccion(distrito) {
    this.direccionesService.getPrecioxDireccion(distrito).subscribe(
      (response) => {
        //  this.listDistritos = response;
        this.listPrecioEnvio = response;
        // estructura  {{item.id_envio , item.precio , item.mensaje}} item-precio esta en string convertir a number
      },
      (error) => {
        console.log(error);
      }
    );
  }

  getOpcionDireccion() {
    this.direccionesService.getOpcionDireccion().subscribe((response) => {
      var arrayAuxReg = [];
      arrayAuxReg.push({ id: "", text: "" });

      for (let index = 0; index < response["length"]; index++) {
        arrayAuxReg.push({
          id: response[index]["id_opcion"],
          text: response[index]["opcion"],
        });
      }
      this.listOpcionDireccion = arrayAuxReg;
    });
  }

  changeTipoDireccion($event) {
    this.selectTipoDireccion = $event.value;
  }

  finalizar() {
    if (this.nombres_registro == "") {
      Swal.fire("Oops...", "Ingrese su(s) nombre(s)", "error");
      return false;
    } else if (this.apellidos_registro == "") {
      Swal.fire("Oops...", "Ingrese sus apellidos", "error");
      return false;
    } else if (
      this.documento_registro == "" ||
      this.documento_registro.length < 8
    ) {
      Swal.fire("Oops...", "Ingrese su DNI válido", "error");
      return false;
    } else if (
      this.telefono_registro == "" ||
      this.telefono_registro.length < 9
    ) {
      Swal.fire("Oops...", "Ingrese su teléfono válido de 9 dígitos", "error");
      return false;
    } else if (!this.utilService.validateEmail(this.correo_registro)) {
      Swal.fire("Oops...", "Ingrese un correo válido", "error");
      return false;
    } else if (this.clave_registro == "") {
      Swal.fire("Oops...", "Ingrese su contraseña", "error");
      return false;
    } else if (this.selectProvincia == "") {
      Swal.fire("Oops...", "Selecciona la provincia", "error");
      return false;
    } else if (this.selectDistrito == "") {
      Swal.fire("Oops...", "Selecciona el distrito", "error");
      return false;
    } else if (this.selectTipoDireccion == "") {
      Swal.fire(
        "Oops...",
        "Selecciona si es casa , oficina o departamento",
        "error"
      );
      return false;
    } else if (this.direccion_principal == "") {
      Swal.fire("Oops...", "Ingresa la dirección", "error");
      return false;
    } else if (this.numero_direccion == "") {
      Swal.fire("Oops...", "Ingresa el número de la dirección", "error");
      return false;
    } else {
      var datos_registro_berries = {
        fecha_registro: this.jstoday,
        dni: this.documento_registro,
        nombre_completo: this.nombres_registro,
        sexo: "",
        celular: "",
        telefono: this.telefono_registro,
        correo: this.correo_registro,
        recibir_oferta: "2",
      };

      $("#overlay-spinner1").show();
      //  $("#modalRegistrar").hide();

      this.clienteService
        .registerUserBDBerries(datos_registro_berries)
        .subscribe(
          (response) => {
            var registro_direccion = {
              id_cliente: response[0]["id_cli_nat"],
              id_provincia: this.selectProvincia,
              id_distrito: this.selectDistrito,
              id_precio_envio: String(this.listPrecioEnvio[0]["id_envio"]),
              direccion: this.direccion_principal,
              id_opcion_direccion: this.selectTipoDireccion,
              numero: this.numero_direccion,
              referencia_1: this.referencia,
              referencia_2: this.referencia,
              principal: "1",
              latitud: this.latitude,
              longitud: this.longitude,
            };

            this.direccionesService
              .crearDireccion(registro_direccion)
              .subscribe(
                (response1) => {
                  var data_registro_admin = {
                    nombres: this.nombres_registro,
                    apellidos: this.apellidos_registro,
                    documento: this.documento_registro,
                    telefono: this.telefono_registro,
                    correo: this.correo_registro,
                    clave: this.clave_registro,
                    direccion: this.direccion_principal,
                    referencia: this.referencia,
                    latitud: String(this.latitude),
                    longitud: String(this.longitude),
                    id_cli_nat: response[0]["id_cli_nat"],
                  };

                  this.clienteService
                    .registarUserBDAdmin(data_registro_admin)
                    .subscribe(
                      (response2) => {
                        if (response2["success"] == true) {
                          $("#overlay-spinner1").hide();

                          $("#modalRegistrar").hide();

                          Swal.fire({
                            imageUrl: "assets/img/icons/icon-jugo-registro.svg",
                            imageHeight: 184,
                            imageAlt: "A tall image",
                            html:
                              '<h2 style="font-size: 25px; font-weight: bold; text-align: center; color: #2c385f;">Hora de Disfrutar</h2><br><p style="line-height: 1.63;text-align: center; color: #778489;">Ya eres parte de la familia de<br> ' +
                              "Berries del Perú. Ya puedes empezar<br> " +
                              "a comprar y disfrutar de nuestros <br> productos. Por favor <b>inicia sesión</b>",
                            confirmButtonColor: "#8be1b0",
                            buttonsStyling: true,
                            confirmButtonText:
                              '<div style="width:199px; font-size: 14px; font-weight: bold;text-align: center; color: #ffffff; letter-spacing: 1.56px;">ACEPTAR</div>',
                          }).then(() => {
                            location.reload();
                          });

                          document.getElementById("mySidenav").style.width =
                            "0";
                          $("#opacity").css("width", "0%");
                          document.getElementById(
                            "mySidenavMovil"
                          ).style.width = "0";
                          $("#opacityMovil").css("width", "0%");
                          $("#modalRegistrar").modal("hide");
                        } else {
                          $("#overlay-spinner1").hide();

                          Swal.fire("Oops...", response2["message"], "error");
                        }
                      },
                      (error) => {
                        console.log(error);
                      }
                    );
                },
                (error) => {
                  console.log(error);
                }
              );
          },
          (error) => {
            $("#overlay-spinner1").hide();
            Swal.fire(
              "Oops...",
              "El Correo ya se encuentra registrado",
              "error"
            );
          }
        );
    }
  }

  expandirMobileMenu() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
      x.className += " responsive";
    } else {
      x.className = "topnav";
    }
  }

  goInicio() {
    document.getElementById("myTopnav").className = "topnav";
    this.router.navigate(["/"]);
  }

  goTienda() {
    document.getElementById("myTopnav").className = "topnav";
    this.router.navigate(["/tienda"]);
  }

  goMiCuenta() {
    document.getElementById("myTopnav").className = "topnav";
    this.router.navigate(["/mi-cuenta"]);
  }

  goContacto() {
    document.getElementById("myTopnav").className = "topnav";

    this.router.navigate(["/contacto/formulario"]);
  }

  goCarrito() {
    if (this.clienteService.isLoggedin) {
      document.getElementById("navCart").style.width = "0";
      $("#opacity").css("width", "0%");

      this.router.navigate(["/carrito-compras"]);
    }
  }

  goCarritoMovil() {
    if (this.clienteService.isLoggedin) {
      document.getElementById("navCartMovil").style.width = "0";
      $("#opacityMovil").css("width", "0%");

      this.router.navigate(["/carrito-compras"]);
    }
  }

  redirectToLogin() {
    document.getElementById("navCart").style.width = "0";
    $("#opacity").css("width", "0%");
    /* document.getElementById("navCartMovil").style.width = "0";
     $("#opacityMovil").css("width" , "0%"); */

    this.openMyAccount();
    // this.openMyAccountMovil();
  }

  redirectToLoginMovil() {
    document.getElementById("navCartMovil").style.width = "0";
    $("#opacityMovil").css("width", "0%");

    this.openMyAccountMovil();
  }

  changePassword() {
    if (this.recuperar_correo == "") {
      Swal.fire({
        title:
          "<img src='assets/img/tiendas/iconocorreoalert.png' style='height:100px;margin-bottom:20px' />",
        html: "Por favor, ingrese su correo",
        confirmButtonText: "DE ACUERDO",
      });

      return false;
    } else {
      var data = { correo: this.recuperar_correo };

      $("#overlay-spinner1").show();
      this.clienteService.recuperarClave(data).subscribe(
        (response) => {
          if (response["success"] == true) {
            $("#overlay-spinner1").hide();

            Swal.fire({
              imageUrl: "assets/img/icons/icon-good-pago.svg",
              imageHeight: 60,
              html:
                '<h2 style="font-size:20px;text-align:center;color:#778489;font-weight:bold;">Te hemos mandado un mensaje a tu bandeja de correo , con tu nueva contraseña</h2>',
              confirmButtonColor: "#8be1b0",
              buttonsStyling: true,
              confirmButtonText:
                '<div style="width:199px; font-size: 14px; font-weight: bold;text-align: center; color: #ffffff; letter-spacing: 1.56px;">DE ACUERDO</div>',
            }).then(() => {
              document.getElementById("mySidenav").style.width = "0";
              $("#opacity").css("width", "0%");
              document.getElementById("mySidenavMovil").style.width = "0";
              $("#opacityMovil").css("width", "0%");
              $("#modalClave").modal("hide");
              location.reload();
            });
          } else {
            $("#overlay-spinner1").hide();
            Swal.fire("Oops...", response["message"], "error");
          }
        },
        (error) => {
          console.log(error);
        }
      );
    }
  }

  public removeProduct(item: Producto) {
    this.cartService.removeFromCart(item);
  }

  public refrescarDatos() {
    this.shoppingCartItems$ = this.cartService.getItems();
  }

  public getPrecioTotal() {
    var itemDate = 0;
    for (let index = 0; index < this.shoppingCartItems.length; index++) {
      if (this.shoppingCartItems[index]) {
        itemDate =
          itemDate +
          this.shoppingCartItems[index]["cantidad"] *
          this.shoppingCartItems[index]["precio_con_igv"];
      }
    }
    return itemDate;
  }

  public cantidadCarritoData() {
    this.itemDate = 0;
    for (let index = 0; index < this.shoppingCartItems.length; index++) {
      if (this.shoppingCartItems[index]) {
        this.itemDate =
          this.itemDate + this.shoppingCartItems[index]["cantidad"];
      }
    }
    return this.itemDate;
  }
}
