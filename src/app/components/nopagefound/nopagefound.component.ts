import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-nopagefound",
  templateUrl: "./nopagefound.component.html",
  styleUrls: ["./nopagefound.component.css"],
})
export class NopagefoundComponent implements OnInit {
  constructor() {}

  ngOnInit() {
    window.scrollTo(0, 0);

    $(".content-tienda").addClass("header-tienda");
    $(".navbar .nav-item .nav-link").css("color", "#778489");
    $(".navbar .nav-item .dropdown-toggle").addClass("border-dropdown-gray");
    $(".navbar .nav-item .rounded-market").addClass("border-button-nav");
    $("#img-market").attr("src", "assets/img/icons/icon-market-gray.svg");
    $("#img-cart").attr("src", "assets/img/icons/icon-cart-gray.svg");
    $("#img-user").attr("src", "assets/img/icons/icon-user-gray.svg");
  }
}
