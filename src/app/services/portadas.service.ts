import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import {urlService} from '../urlservice';

@Injectable({
  providedIn: 'root'
})
export class PortadasService {

  public domain_images: String;

  constructor(private http: HttpClient , private urlService: urlService) {

    this.domain_images = this.urlService.domain_images_berries;
   }

   getPortadas(){
    return this.http.get<Object[]>(`${this.domain_images}getPortada.php`,{headers: new HttpHeaders().set('Accept', 'application/json')}).pipe(map(res => res));
   }

   getPopUp(){
     return this.http.get<Object[]>("https://berriesdelperu.com/berries-admin/webservices/api/getPopup.php",{headers: new HttpHeaders().set('Accept', 'application/json')}).pipe(map(res => res));
   }

}
