import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PulpasComponent } from './pulpas.component';

describe('PulpasComponent', () => {
  let component: PulpasComponent;
  let fixture: ComponentFixture<PulpasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PulpasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PulpasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
