import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeshidratadosComponent } from './deshidratados.component';

describe('DeshidratadosComponent', () => {
  let component: DeshidratadosComponent;
  let fixture: ComponentFixture<DeshidratadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeshidratadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeshidratadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
