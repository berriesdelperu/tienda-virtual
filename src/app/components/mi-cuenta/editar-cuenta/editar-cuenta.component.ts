import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { ClienteService } from '../../../services/cliente.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { DireccionesService } from 'src/app/services/direcciones.service';
import { UtilService } from 'src/app/utils/util.service';
import { EditarDireccionComponent } from '../../Modals/editar-direccion/editar-direccion.component';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Select2OptionData } from 'ng2-select2';
import { TarjetasService } from 'src/app/services/tarjetas.service';
import { AgregarTarjetaComponent } from '../../Modals/agregar-tarjeta/agregar-tarjeta.component';
import { MapsAPILoader, MouseEvent } from '@agm/core';
declare var $: any;

@Component({
  selector: 'app-editar-cuenta',
  templateUrl: './editar-cuenta.component.html',
  styleUrls: ['./editar-cuenta.component.css']
})
export class EditarCuentaComponent implements OnInit {

  public idpersona: any;
  public nombre_completo: string;
  public correo: string;
  public contraseña: string;
  public telefono: string;
  public clave: string;
  public direccion_principal_show: string;
  public direccion_otras: string;
  public listaDireccionesTotal: Array<any> = [];
  public listaDirecciones: Array<any> = [];
  public storageDatosUsuario: any;
  public listPrecioEnvio: Object[];
  public listTarjetas: Object[];

  public listProvincias: Array<Select2OptionData>;
  public optionsSelect2Provincias: Select2Options;
  public selectProvincia: string;

  public listDistritos: Array<Select2OptionData>;
  public optionsSelect2Distritos: Select2Options;
  public selectDistrito: string;

  public listOpcionDireccion: Array<Select2OptionData>;
  public optionsSelect2TipoDireccion: Select2Options;
  public selectTipoDireccion: string;

  public numero_direccion: string;
  public referencia: string;

  public modalRef: BsModalRef;
  public modalRefRegistrarTarjeta: BsModalRef

  /****PARA DIRECCION PRINCIPAL */

  public id_direccion: any
  public id_provincia: any;
  public id_distrito: any;
  public id_precio_envio: any;
  public direccion: any;
  public id_opcion_direccion: any;
  public numero: any;
  public referencia_1: any;
  public referencia_2: any;
  public direccion_principal: any;
  public latitud: any;
  public longitud: any;

  @ViewChild('searchAgregar', { static: true }) searchElementRef: ElementRef;
  private geoCoder;
  public latitude: number;
  public longitude: number;
  public zoom: number;
  public address: string;


  public distrito_show: string;

  public antigua: string;
  public nuevaclave: string;






  constructor(private perfilService: ClienteService, private router: Router, private modalService: BsModalService,
    public location: Location, private direccionesService: DireccionesService, public utilService: UtilService,
    private tarjetaService: TarjetasService, private mapsAPILoader: MapsAPILoader, private ngZone: NgZone) {
    this.nuevaclave = "";
    this.antigua = "";

    this.optionsSelect2Provincias = {
      placeholder: "Provincia",
      width: "100%"
    }

    this.optionsSelect2Distritos = {
      placeholder: "Distrito",
      width: "100%"
    }

    this.optionsSelect2TipoDireccion = {
      placeholder: "Tipo",
      width: "100%"
    }

    this.clave = "";
    this.direccion_otras = "";
    this.selectProvincia = "";
    this.selectDistrito = "";
    this.selectTipoDireccion = "";
    this.numero_direccion = "";
    this.numero = "";

    this.distrito_show = "";
    this.referencia_1 = "";
    this.storageDatosUsuario = JSON.parse(localStorage.getItem("datospersona"));
    this.idpersona = { id_cli_nat: this.storageDatosUsuario["id_cli_nat"] }


    if (this.direccion_principal_show === undefined) {
      this.direccion_principal_show = "";
    } else {
      this.direccion_principal_show = this.storageDatosUsuario["direccion"];

    }

  }

  ngOnInit() {


    $(".imgIconMovilmenuFinal").attr("src", "assets/img/icons/icon-menu-2.svg");
    $(".imgIconMoviltiendaFinal").attr("src", "assets/img/icons/icon-tienda-2.svg");
    $(".imgIconMovilCartFinal").attr("src", "assets/img/icons/icon-cart-2.svg");
    $(".imgIconMovilUserFinal").attr("src", "assets/img/icons/icon-user-2.svg");


    window.scrollTo(0, 0);
    $(".content-tienda").addClass('header-tienda');
    $('.navbar .nav-item .nav-link').css('color', "#778489");
    $('.navbar .nav-item .dropdown-toggle').addClass('border-dropdown-gray');
    $('.navbar .nav-item .rounded-market').addClass('border-button-nav');
    $("#img-market").attr("src", "assets/img/icons/icon-market-gray.svg");
    $("#img-cart").attr("src", "assets/img/icons/icon-cart-gray.svg");
    $("#img-user").attr("src", "assets/img/icons/icon-user-gray.svg");


  


    $(window).resize(function () {

      if ($(window).width() < 500) {
        $(".content-tienda").addClass('header-tienda');
        $('.navbar .nav-item .nav-link').addClass('color-nav');
        $('.navbar .nav-item .dropdown-toggle').addClass('border-dropdown-gray');
        $('.navbar .nav-item .rounded-market').addClass('border-button-nav');
        $("#img-market").attr("src", "assets/img/icons/icon-market-gray.svg");
        $("#icon-cart").attr("src", "assets/img/icons/icon-cart-2.svg");
        $("#img-user").attr("src", "assets/img/icons/icon-user-2.svg");
      }
    });

    $(".toggle-password").click(function () {
      var $pwd = $(".input-password");
      if ($pwd.attr('type') === 'password') {
        $pwd.attr('type', 'text');
      } else {
        $pwd.attr('type', 'password');
      }
    });


    $(".toggle-password2").click(function () {
      var $pwd = $(".input-password2");
      if ($pwd.attr('type') === 'password') {
        $pwd.attr('type', 'text');
      } else {
        $pwd.attr('type', 'password');
      }
    });

    this.autocomplete();
    this.getProvincias();
    this.getOpcionDireccion();
    this.getPerfil();
    this.listaDireccion();
    this.getTarjetas();

  }




  autocomplete() {
    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation();
      this.geoCoder = new google.maps.Geocoder;

      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"],
        componentRestrictions: { 'country': 'PE' }
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();


          this.direccion_otras = place.formatted_address;

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 15;
        });
      });
    });

  }

  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 15;
        this.getAddress(this.latitude, this.longitude);
      });
    }
  }

  markerDragEnd($event: MouseEvent) {

    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    this.getAddress(this.latitude, this.longitude);
  }

  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 15;
          this.address = results[0].formatted_address;
          this.direccion_otras = this.address;
        } else {
          window.alert('No se encontraron resultados GEOCODER');
        }
      } else {
        window.alert('Geocoder fallido : ' + status);
      }

    });
  }


  getPerfil() {

    this.perfilService.getPerfil(this.idpersona).subscribe(response => {

      this.nombre_completo = response[0]["nombre_completo"];
      this.correo = response[0]["correo"];
      this.telefono = response[0]["telefono"];
    },
      error => {
        console.log(error);
      });

    this.perfilService.getPasswordByUsuario(this.idpersona).subscribe(response => {
      this.clave = response["clave"];
    });

  }

  cambiarClave() {

    if (this.antigua == "") {
      Swal.fire({
        type: 'info',
        title: 'Ingrese su contraseña actual',
        text: 'Es un requisito para poder actualizar su contraseña',
      });
    } else if (this.nuevaclave == "") {
      Swal.fire({
        type: 'info',
        title: 'Ingrese su nueva contraseña',
        text: 'Es un requisito para poder actualizar su contraseña',
      });

    } else if (this.clave != this.antigua) {
      Swal.fire({
        type: 'warning',
        title: 'Algo anda mal',
        text: 'La contraseña actual que ingreso no es correcta',
      });
    } else {

      var id_cliente = JSON.parse(localStorage.getItem("datospersona"));

      var data = { id_cli_nat: id_cliente["id_cli_nat"], clave: this.nuevaclave };

      this.perfilService.cambiarClave(data).subscribe(response => {

        if (response["success"] == true) {
          this.clave = this.nuevaclave;
          Swal.fire({
            type: 'success',
            title: 'Acción realizada!',
            text: 'Se cambió la contraseña',
          }).then(() => {

            this.router.navigate(['/mi-cuenta']);

          });

        } else {
          console.log("surgio un error");

        }

      },
        error => {
          console.log(error);
        })


    }


  }


  getProvincias() {

    this.direccionesService.getProvincias().subscribe(response => {

      var arrayAuxReg = [];
      arrayAuxReg.push({ id: '', text: "" });

      for (let index = 0; index < response["length"]; index++) {

        arrayAuxReg.push({
          id: response[index]['id_provincia'],
          text: response[index]['provincia']
        });

      }
      this.listProvincias = arrayAuxReg;

    },
      error => {
        console.log(error);
      })


  }

  changeProvincias($event) {
    this.listDistritos = null
    this.selectProvincia = $event.value;
    let provincia = { id_provincia: this.selectProvincia };
    this.getDistritos(provincia);
    this.direccion_otras = ""
    this.numero_direccion = ""
    this.referencia = ""
    $('#selectTipo select').val(null).trigger('change')

  }

  getDistritos(provincia) {

    this.direccionesService.getDistritos(provincia).subscribe(response => {
      var arrayAuxReg = [];
      arrayAuxReg.push({ id: '', text: "" });

      for (let index = 0; index < response["length"]; index++) {

        arrayAuxReg.push({
          id: response[index]['id_distrito'],
          text: response[index]['distrito']
        });
      }
      this.listDistritos = arrayAuxReg;
    },
      error => {
        console.log(error);
      })


  }

  changeDistritos($event) {
    this.selectDistrito = $event.value;
    let distrito = { id_distrito: this.selectDistrito };
    this.getPrecioxDireccion(distrito);
    this.direccion_otras = ""
    this.numero_direccion = ""
    this.referencia = ""
  }

  getPrecioxDireccion(distrito) {
    this.direccionesService.getPrecioxDireccion(distrito).subscribe(response => {
      this.listPrecioEnvio = response;
    },
      error => {
        console.log(error);
      })
  }

  getOpcionDireccion() {

    this.direccionesService.getOpcionDireccion().subscribe(response => {
      var arrayAuxReg = [];
      arrayAuxReg.push({ id: '', text: "" });

      for (let index = 0; index < response["length"]; index++) {

        arrayAuxReg.push({
          id: response[index]['id_opcion'],
          text: response[index]['opcion']
        });
      }
      this.listOpcionDireccion = arrayAuxReg;

    })
  }

  changeTipoDireccion($event) {
    this.selectTipoDireccion = $event.value;
  }


  agregarOtraDireccion() {

    var id_cliente = this.storageDatosUsuario;

    if (this.selectProvincia == "") {

      Swal.fire("Oops...", "Selecciona la provincia", "error");
      return false

    } else if (this.selectDistrito == "") {

      Swal.fire("Oops...", "Selecciona el distrito", "error");
      return false

    } else if (this.selectTipoDireccion == "") {

      Swal.fire("Oops...", "Selecciona si es casa , oficina o departamento", "error");
      return false

    } else if (this.direccion_otras == "") {

      Swal.fire("Oops...", "Ingresa la dirección", "error");
      return false

    } else if (this.numero_direccion == "") {

      Swal.fire("Oops...", "Ingresa el número de la dirección", "error");
      return false

    } else if (this.numero_direccion == "") {

      Swal.fire("Oops...", "Ingresa el alguna diferencia", "error");
      return false

    } else {


      var registro_direccion;

      if (this.direccion_principal_show === "") {

        registro_direccion = {
          id_cliente: id_cliente["id_cli_nat"], id_provincia: this.selectProvincia, id_distrito: this.selectDistrito,
          id_precio_envio: String(this.listPrecioEnvio[0]["id_envio"]), direccion: this.direccion_otras,
          id_opcion_direccion: this.selectTipoDireccion, numero: this.numero_direccion,
          referencia_1: this.referencia, referencia_2: this.referencia,
          principal: "1", latitud: this.latitude, longitud: this.longitude
        };

      } else {

        registro_direccion = {
          id_cliente: id_cliente["id_cli_nat"], id_provincia: this.selectProvincia, id_distrito: this.selectDistrito,
          id_precio_envio: String(this.listPrecioEnvio[0]["id_envio"]), direccion: this.direccion_otras,
          id_opcion_direccion: this.selectTipoDireccion, numero: this.numero_direccion,
          referencia_1: this.referencia, referencia_2: this.referencia,
          principal: "2", latitud: this.latitude, longitud: this.longitude
        };

      }

      this.direccionesService.crearDireccion(registro_direccion).subscribe(response => {



        Swal.fire({
          type: 'success',
          title: 'Acción realizada!',
          text: 'Se agregó la dirección',
        }).then(() => {
          $('#modalRegistrarDireccion').modal('hide');

          this.router.navigate(['/mi-cuenta']);

        });

      },
        error => {
          Swal.fire("Oops...", "Ocurrio un error , intente de nuevo por favor", "error");
        })



    }


  }


  actualizarDireccion(id_direccion, id_provincia, id_distrito, id_precio_envio, direccion, id_opcion_direccion, numero, referencia_1, referencia_2, direccion_principal, latitud, longitud) {

    const initialState = {
      id_direccion: id_direccion,
      id_provincia: id_provincia,
      id_distrito: id_distrito,
      id_precio_envio: id_precio_envio,
      direccion: direccion,
      id_opcion_direccion: id_opcion_direccion,
      numero: numero,
      referencia_1: referencia_1,
      referencia_2: referencia_2,
      direccion_principal: direccion_principal,
      latitud: latitud,
      longitud: longitud
    };


    if (this.direccion_principal_show === "") {

      Swal.fire("Oops...", "Ud. debe agregar una dirección principal", "error");
      return false
    } else {

      this.modalRef = this.modalService.show(EditarDireccionComponent,
        {
          class: 'modal-lg  modal-dialog-centered', initialState
        }
      );

    }






    this.modalRef.content.onClose = (data) => {
      if (data.estado) {
        this.modalRef.hide();
        Swal.fire(
          'Actualizado',
          "Se actualizo la direccion",
          'success'
        ).then(() => {

          this.router.navigate(['/mi-cuenta']);

        })
      }
    }


  }

  openAddTarjeta() {

    this.modalRefRegistrarTarjeta = this.modalService.show(AgregarTarjetaComponent,
      {
        class: 'modal-dialog-centered'
      }
    );


    this.modalRefRegistrarTarjeta.content.onClose = (data) => {
      if (data.estado) {
        this.modalRefRegistrarTarjeta.hide();
        Swal.fire(
          'Actualizado',
          "Se agrego la tarjeta",
          'success'
        ).then(() => {

          this.router.navigate(['/mi-cuenta']);

        })
      }
    }

  }

  eliminarTarjeta(id_tarjeta) {

    let data = { id_tarjeta: id_tarjeta };

    this.tarjetaService.eliminarTarjeta(data).subscribe(response => {



      if (response["success"] == true) {

        Swal.fire({
          type: 'success',
          title: 'Acción realizada!',
          text: 'Se eliminó la tarjeta seleccionada',
        }).then(() => {

          this.router.navigate(['/mi-cuenta']);
        });

      } else {

        Swal.fire("Oops...", response["message"], "error");

      }

    },
      error => {
        console.log(error);
      })

  }

  listaDireccion() {


    var id_cliente = this.storageDatosUsuario;

    let cliente = { cliente: id_cliente["id_cli_nat"] };


    this.direccionesService.getDireccionByCliente(cliente).subscribe(response => {

      this.listaDireccionesTotal = response;



      for (var i = 0; i < this.listaDireccionesTotal.length; i++) {

        if (this.listaDireccionesTotal[i]["direccion_principal"] == 1) {

          this.direccion_principal_show = this.listaDireccionesTotal[i]["direccion"] + " , " + this.listaDireccionesTotal[i]["numero"];
          this.distrito_show = this.utilService.titleCase(this.listaDireccionesTotal[i]["distrito"]);
          this.id_direccion = this.listaDireccionesTotal[i]["id_direccion"];
          this.id_provincia = this.listaDireccionesTotal[i]["id_provincia"];
          this.id_distrito = this.listaDireccionesTotal[i]["id_distrito"];
          this.id_precio_envio = "";
          this.direccion = this.listaDireccionesTotal[i]["direccion"];
          this.id_opcion_direccion = this.listaDireccionesTotal[i]["opcion_direccion"];
          this.numero = this.listaDireccionesTotal[i]["numero"];
          this.referencia_1 = this.listaDireccionesTotal[i]["referencia_1"];
          this.referencia_2 = this.listaDireccionesTotal[i]["referencia_2"];
          this.direccion_principal = this.listaDireccionesTotal[i]["direccion_principal"];
          this.latitud = this.listaDireccionesTotal[i]["latitud"];
          this.longitud = this.listaDireccionesTotal[i]["longitud"];

        } else if (response[i]["direccion_principal"] == 2) {
          this.listaDirecciones.push({
            id_direccion: response[i]["id_direccion"],
            provincia: response[i]["provincia"],
            distrito: response[i]["distrito"],
            precio_envio: response[i]["precio_envio"],
            direccion: response[i]["direccion"],
            opcion_direccion: response[i]["opcion_direccion"],
            numero: response[i]["numero"],
            referencia_1: response[i]["referencia_1"],
            referencia_2: response[i]["referencia_2"],
            direccion_principal: response[i]["direccion_principal"],
            id_provincia: response[i]["id_provincia"],
            id_distrito: response[i]["id_distrito"],
            id_opcion_direccion: response[i]["id_opcion_direccion"],
            id_precio_envio: "",
            latitud: Number(response[i]["latitud"]),
            longitud: Number(response[i]["longitud"]),
            mensaje: "envio a domicilio",
          })
        }

      }

    },
      error => {
        console.log(error);
      })

  }


  eliminarDireccion(id_direccion) {

    var data = { id_direccion: id_direccion };

    this.direccionesService.eliminarDireccion(data).subscribe(response => {

      Swal.fire({
        type: 'success',
        title: 'Acción realizada!',
        text: 'Se eliminó la dirección',
      }).then(() => {

        this.router.navigate(['/mi-cuenta']);
      });



    },
      error => {
        console.log(error);
      })

  }


  getTarjetas() {

    var id_cliente = this.storageDatosUsuario;

    let cliente = { id_cli_nat: id_cliente["id_cli_nat"] };

    this.tarjetaService.getTarjetas(cliente).subscribe(response => {

      if (response["success"] == true) {

        this.listTarjetas = response["tarjetas"];

      }

      /*else{
        console.log(response["message"]);

      }*/

    }, error => {
      console.log(error);
    })
  }


  actualizarPerfil() {

    var id_cliente = this.storageDatosUsuario;

    if (this.nombre_completo == "") {

      Swal.fire("Oops...", "Ingrese sus nombres", "error");
      return false;

    } else if (this.direccion_principal_show == "") {

      Swal.fire("Oops...", "Ingrese dirección principal", "error");
      return false;

    } else if (this.telefono == "") {

      Swal.fire("Oops...", "Ingrese telefono", "error");
      return false;

    } else {

      var dni;

      if (dni == undefined || dni == "") {

        dni = "";

      } else {
        dni = id_cliente["documento"];
      }

      var data = { id_cli_nat: id_cliente["id_cli_nat"], nombres: this.nombre_completo, apellidos: "", direccion: this.direccion_principal_show, referencia: "", latitud: "", longitud: "" };


      var data_berries = { id_cli_nat: id_cliente["id_cli_nat"], estado: "1", dni: dni, nombre_completo: this.nombre_completo, sexo: "1", celular: this.telefono, telefono: this.telefono, correo: this.correo, recibir_oferta: "1" };


      this.perfilService.editarPerfilBDBerries(data_berries).subscribe(response => {


        this.perfilService.editarPerfil(data).subscribe(response => {

          //actualizamos la direccion en el local storage
          this.storageDatosUsuario.direccion = this.direccion_principal_show;
          localStorage.setItem("datospersona", JSON.stringify(this.storageDatosUsuario));

          if (response["success"] == true) {

            Swal.fire({
              type: 'success',
              title: 'Acción realizada!',
              text: 'Sus datos fueron actualizados',
            }).then(() => {

              this.router.navigate(['/mi-cuenta']);
            });

          } else {
            console.log("error");
          }

        })

      })


    }

  }




}
