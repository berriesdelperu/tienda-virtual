import {
  Component,
  OnInit,
  ViewChild,
  AfterViewInit,
  HostListener,
} from "@angular/core";
import { ProductoService } from "../../../services/producto.service";
import { Router } from "@angular/router";
import { UtilService } from "../../../utils/util.service";
import { ComplementosComponent } from "../../complementos/complementos.component";

@Component({
  selector: "app-congelados",
  templateUrl: "./congelados.component.html",
  styleUrls: ["./congelados.component.css"],
})
export class CongeladosComponent implements OnInit {
  public arrayProductsCongelados: Array<any> = [];
  public listCongelados: Array<any> = [];
  public slidesCongelados: any = [[]];
  public arrayImagesProductsCongelados: Array<any> = [];

  public slidesCategorias: any = [[]];
  public listCategorias: Array<any> = [];

  public countItems = 4;
  public innerWidth: any;

  @ViewChild(ComplementosComponent, { static: true })
  complemento: ComplementosComponent;

  constructor(
    private productoService: ProductoService,
    public router: Router,
    private utilService: UtilService
  ) {
    if (innerWidth < 992) {
      this.countItems = 2;
    }

    if (innerWidth < 769) {
      this.countItems = 1;
    }
  }

  @HostListener("window:resize", ["$event"])
  onResize(event) {
    this.innerWidth = window.innerWidth;
  }

  ngOnInit() {

    this.innerWidth = window.innerWidth;
    window.scrollTo(0, 0);
    $(".imgIconMovilmenuFinal").attr(
      "src",
      "assets/img/icons/icon-menu.svg"
    );
    $(".imgIconMoviltiendaFinal").attr(
      "src",
      "assets/img/icons/icon-tienda.svg"
    );
    $(".imgIconMovilCartFinal").attr(
      "src",
      "assets/img/icons/icon-cart.svg"
    );
    $(".imgIconMovilUserFinal").attr(
      "src",
      "assets/img/icons/icon-user.svg"
    );
    $(".navigationFixed").removeClass("fondito-berries");
    $(".content-tienda").removeClass('header-tienda');
    $('.navbar .nav-item .nav-link').css('color', "#ffffff");
    $('.navbar .nav-item .dropdown-toggle').removeClass('border-dropdown-gray');
    $('.navbar .nav-item .rounded-market').removeClass('border-button-nav');
    $("#img-market").attr("src", "assets/img/icons/icon-market.svg");
    $("#img-cart").attr("src", "assets/img/icons/icon-cart.svg");
    $("#img-user").attr("src", "assets/img/icons/icon-user.svg");
    document.getElementById("scrolling-navbar").style.background = "transparent"
    this.getCongelados();
    $(".carousel").on("touchstart", function (event) {
      var xClick = event.originalEvent.touches[0].pageX;
      $(this).one("touchmove", function (event) {
        var xMove = event.originalEvent.touches[0].pageX;
        if (Math.floor(xClick - xMove) > 5) {
          ($(this) as any).carousel('next');
        }
        else if (Math.floor(xClick - xMove) < -5) {
          ($(this) as any).carousel('prev');
        }
      });
      $(".carousel").on("touchend", function () {
        $(this).off("touchmove");
      });
    });

  }


  getCongelados() {
    let id_categoria = { linea: "1" };
    let image = { tipo_consulta: "2" };

    this.productoService
      .getProductsByCategoria(id_categoria)
      .subscribe((response) => {
        this.arrayProductsCongelados = response;

        this.productoService.getImageProduct(image).subscribe((response1) => {
          var arrayIdItemsWImages = [];
          var arrayIdItemsSystem = [];

          this.arrayImagesProductsCongelados = response1["productos"];

          for (var j = 0; j < this.arrayImagesProductsCongelados.length; j++) {
            arrayIdItemsWImages.push(
              Number(this.arrayImagesProductsCongelados[j]["id_producto"])
            );
          }

          for (var k = 0; k < this.arrayProductsCongelados.length; k++) {
            arrayIdItemsSystem.push(
              Number(this.arrayProductsCongelados[k]["id_pro"])
            );
          }

          for (var i = 0; i < this.arrayProductsCongelados.length; i++) {
            if (
              arrayIdItemsWImages.includes(
                this.arrayProductsCongelados[i]["id_pro"]
              )
            ) {
              this.listCongelados.push({
                id_pro: this.arrayProductsCongelados[i]["id_pro"],
                linea_de_negocio: this.arrayProductsCongelados[i][
                  "linea_de_negocio"
                ],
                presentacion: this.arrayProductsCongelados[i]["presentacion"],
                nombre_producto: this.arrayProductsCongelados[i][
                  "nombre_producto"
                ],
                precio_con_igv: this.arrayProductsCongelados[i][
                  "precio_con_igv"
                ],
                stock_actual: this.arrayProductsCongelados[i]["stock_actual"],
                stock_reservado: this.arrayProductsCongelados[i][
                  "stock_reservado"
                ],
                stock_disponible: this.arrayProductsCongelados[i][
                  "stock_disponible"
                ],
                cantidad: 1,
                porcentaje: "0",
                nuevo_precio: this.arrayProductsCongelados[i]["precio_con_igv"],
                imagen: this.arrayImagesProductsCongelados[
                  arrayIdItemsWImages.indexOf(
                    this.arrayProductsCongelados[i]["id_pro"]
                  )
                ]["imagen_principal"],
              });
            } else {
              this.listCongelados.push({
                id_pro: this.arrayProductsCongelados[i]["id_pro"],
                linea_de_negocio: this.arrayProductsCongelados[i][
                  "linea_de_negocio"
                ],
                presentacion: this.arrayProductsCongelados[i]["presentacion"],
                nombre_producto: this.arrayProductsCongelados[i][
                  "nombre_producto"
                ],
                precio_con_igv: this.arrayProductsCongelados[i][
                  "precio_con_igv"
                ],
                stock_actual: this.arrayProductsCongelados[i]["stock_actual"],
                stock_reservado: this.arrayProductsCongelados[i][
                  "stock_reservado"
                ],
                stock_disponible: this.arrayProductsCongelados[i][
                  "stock_disponible"
                ],
                cantidad: 1,
                porcentaje: "0",
                nuevo_precio: this.arrayProductsCongelados[i]["precio_con_igv"],
                imagen: "assets/img/icons/image-not-found.png",
              });
            }
          }

          this.listCategorias.push({
            img: "assets/img/icons/icon-honey-1.png",
            titulo: "Tiempo de Vida",
            descripcion:
              "Puedes disfrutar de estos congelados hasta por 12 meses.",
          });

          this.listCategorias.push({
            img: "assets/img/icons/icon-honey-2.png",
            titulo: "Conservación",
            descripcion:
              "Para mantener estos berries en buen estado, mantenlos siempre congelados.",
          });

          this.listCategorias.push({
            img: "assets/img/icons/icon-honey-3.png",
            titulo: "TIPS",
            descripcion:
              "Listos para consumir, no hace falta lavarlos. Ideal para jugos, smoothies, postres, helados.",
          });

          this.listCategorias.push({
            img: "assets/img/icons/icon-honey-4.png",
            titulo: "Descripción linea",
            descripcion:
              "¡Berries que te brindarán salud y energía! Congelados rápidamente a menos de 18° C para evitar microorganismos y sin usar aditivos químicos. Disfrútalos mientras cuidas del planeta, gracias a su envase biodegradable.",
          });

          this.slidesCategorias = this.utilService.chunk(
            this.listCategorias,
            1
          );
          ($('#ContenedorSlider') as any).carousel({
            interval: 2000,
            cycle: true
          });
          this.slidesCongelados = this.utilService.chunk(
            this.listCongelados,
            this.countItems
          );

          // if(this.listCongelados.length < 5){
          //   $(".rowRightProductosRelacionados").hide();
          // }
        });
      });
  }

  goBlog() {
    this.router.navigate(["/blog"]);
  }
}
