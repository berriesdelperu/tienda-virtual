import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LaEmpresaComponent} from './la-empresa/la-empresa.component';
import { NuestrosConveniosComponent } from './nuestros-convenios/nuestros-convenios.component';

const routes: Routes = [
  { path: '',
   redirectTo: '/no-encontrado', 
   pathMatch: 'full' 
  },
  {
    path: 'la-empresa',
    component: LaEmpresaComponent
  },
  {
    path: 'nuestros-convenios',
    component: NuestrosConveniosComponent
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuienesSomosRoutingModule { }
