import {
  Component,
  OnInit,
  ViewChild,
  AfterViewInit,
  HostListener,
} from "@angular/core";
import { ProductoService } from "../../../services/producto.service";
import { Router } from "@angular/router";
import { UtilService } from "../../../utils/util.service";
import { ComplementosComponent } from "../../complementos/complementos.component";
@Component({
  selector: "app-mermeladas",
  templateUrl: "./mermeladas.component.html",
  styleUrls: ["./mermeladas.component.css"],
})
export class MermeladasComponent implements OnInit {
  public arrayProductsMermeladas: Array<any> = [];
  public listMermeladas: Array<any> = [];
  public slidesMermeladas: any = [[]];

  public slidesCategorias: any = [[]];
  public listCategorias: Array<any> = [];

  public arrayImagesProductsMermeladas: Array<any> = [];
  public auxArray: Array<any> = [];
  public slidesarrayAviso: Array<any> = [[]];

  public countItems = 4;
  public innerWidth: any;

  @ViewChild(ComplementosComponent, { static: true })
  complemento: ComplementosComponent;

  constructor(
    private productoService: ProductoService,
    public router: Router,
    private utilService: UtilService
  ) {
    if (innerWidth < 992) {
      this.countItems = 2;
    }

    if (innerWidth < 769) {
      this.countItems = 1;
    }
  }

  @HostListener("window:resize", ["$event"])
  onResize(event) {
    this.innerWidth = window.innerWidth;
  }

  ngOnInit() {

    this.innerWidth = window.innerWidth;
    window.scrollTo(0, 0);
    $(".imgIconMovilmenuFinal").attr(
      "src",
      "assets/img/icons/icon-menu.svg"
    );
    $(".imgIconMoviltiendaFinal").attr(
      "src",
      "assets/img/icons/icon-tienda.svg"
    );
    $(".imgIconMovilCartFinal").attr(
      "src",
      "assets/img/icons/icon-cart.svg"
    );
    $(".imgIconMovilUserFinal").attr(
      "src",
      "assets/img/icons/icon-user.svg"
    );
    $(".navigationFixed").removeClass("fondito-berries");
    $(".content-tienda").removeClass('header-tienda');
    $('.navbar .nav-item .nav-link').css('color', "#ffffff");
    $('.navbar .nav-item .dropdown-toggle').removeClass('border-dropdown-gray');
    $('.navbar .nav-item .rounded-market').removeClass('border-button-nav');
    $("#img-market").attr("src", "assets/img/icons/icon-market.svg");
    $("#img-cart").attr("src", "assets/img/icons/icon-cart.svg");
    $("#img-user").attr("src", "assets/img/icons/icon-user.svg");
    document.getElementById("scrolling-navbar").style.background = "transparent"
    this.getMermeladas();
    $(".carousel").on("touchstart", function (event) {
      var xClick = event.originalEvent.touches[0].pageX;
      $(this).one("touchmove", function (event) {
        var xMove = event.originalEvent.touches[0].pageX;
        if (Math.floor(xClick - xMove) > 5) {
          ($(this) as any).carousel('next');
        }
        else if (Math.floor(xClick - xMove) < -5) {
          ($(this) as any).carousel('prev');
        }
      });
      $(".carousel").on("touchend", function () {
        $(this).off("touchmove");
      });
    });
  }


  getMermeladas() {
    let id_categoria = { linea: "4" };
    let image = { tipo_consulta: "2" };

    this.productoService
      .getProductsByCategoria(id_categoria)
      .subscribe((response) => {
        this.arrayProductsMermeladas = response;

        this.productoService.getImageProduct(image).subscribe((response1) => {
          var arrayIdItemsWImages = [];
          var arrayIdItemsSystem = [];

          this.arrayImagesProductsMermeladas = response1["productos"];

          for (var j = 0; j < this.arrayImagesProductsMermeladas.length; j++) {
            arrayIdItemsWImages.push(
              Number(this.arrayImagesProductsMermeladas[j]["id_producto"])
            );
          }

          for (var k = 0; k < this.arrayProductsMermeladas.length; k++) {
            arrayIdItemsSystem.push(
              Number(this.arrayProductsMermeladas[k]["id_pro"])
            );
          }

          for (var i = 0; i < this.arrayProductsMermeladas.length; i++) {
            if (
              arrayIdItemsWImages.includes(
                this.arrayProductsMermeladas[i]["id_pro"]
              )
            ) {
              this.listMermeladas.push({
                id_pro: this.arrayProductsMermeladas[i]["id_pro"],
                linea_de_negocio: this.arrayProductsMermeladas[i][
                  "linea_de_negocio"
                ],
                presentacion: this.arrayProductsMermeladas[i]["presentacion"],
                nombre_producto: this.arrayProductsMermeladas[i][
                  "nombre_producto"
                ],
                precio_con_igv: this.arrayProductsMermeladas[i][
                  "precio_con_igv"
                ],
                stock_actual: this.arrayProductsMermeladas[i]["stock_actual"],
                stock_reservado: this.arrayProductsMermeladas[i][
                  "stock_reservado"
                ],
                stock_disponible: this.arrayProductsMermeladas[i][
                  "stock_disponible"
                ],
                cantidad: 1,
                porcentaje: "0",
                nuevo_precio: this.arrayProductsMermeladas[i]["precio_con_igv"],
                imagen: this.arrayImagesProductsMermeladas[
                  arrayIdItemsWImages.indexOf(
                    this.arrayProductsMermeladas[i]["id_pro"]
                  )
                ]["imagen_principal"],
              });
            } else {
              this.listMermeladas.push({
                id_pro: this.arrayProductsMermeladas[i]["id_pro"],
                linea_de_negocio: this.arrayProductsMermeladas[i][
                  "linea_de_negocio"
                ],
                presentacion: this.arrayProductsMermeladas[i]["presentacion"],
                nombre_producto: this.arrayProductsMermeladas[i][
                  "nombre_producto"
                ],
                precio_con_igv: this.arrayProductsMermeladas[i][
                  "precio_con_igv"
                ],
                stock_actual: this.arrayProductsMermeladas[i]["stock_actual"],
                stock_reservado: this.arrayProductsMermeladas[i][
                  "stock_reservado"
                ],
                stock_disponible: this.arrayProductsMermeladas[i][
                  "stock_disponible"
                ],
                cantidad: 1,
                porcentaje: "0",
                nuevo_precio: this.arrayProductsMermeladas[i]["precio_con_igv"],
                imagen: "assets/img/icons/image-not-found.png",
              });
            }
          }

          this.listCategorias.push({
            img: "assets/img/icons/icon-honey-1.png",
            titulo: "Tiempo de Vida",
            descripcion:
              "Podrás disfrutar de esta mermelada hasta por 6 meses.",
          });

          this.listCategorias.push({
            img: "assets/img/icons/icon-honey-2.png",
            titulo: "Conservación",
            descripcion:
              "Para que conserves mejor esta deliciosa mermelada, mantenla en un lugar fresco y seco. ",
          });

          this.listCategorias.push({
            img: "assets/img/icons/icon-honey-3.png",
            titulo: "TIPS",
            descripcion: "Te recomendamos refrigerarla una vez abierta.",
          });

          this.listCategorias.push({
            img: "assets/img/icons/icon-honey-4.png",
            titulo: "Descripción linea",
            descripcion:
              "Elaborada de manera artesanal, no tiene preservantes, ni colorantes, ni gelificantes. ¡Es 100% natural! Tiene un auténtico sabor a fruta y, además, está endulzada con panela orgánica.",
          });

          // if(this.listMermeladas.length < 5){
          //   $(".rowRightProductosRelacionados").hide();
          // }

          this.slidesCategorias = this.utilService.chunk(
            this.listCategorias,
            1
          );

          ($('#ContenedorSlider') as any).carousel({
            interval: 2000,
            cycle: true
          });
          this.slidesMermeladas = this.utilService.chunk(
            this.listMermeladas,
            this.countItems
          );

        });
      });
  }

  goBlog() {
    this.router.navigate(["/blog"]);
  }
}
