import { Component, OnInit } from '@angular/core';
import { ProductoService } from '../../services/producto.service';
import { UtilService } from '../../utils/util.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-complementos',
  templateUrl: './complementos.component.html',
  styleUrls: ['./complementos.component.css']
})
export class ComplementosComponent implements OnInit {

  public arrayProducts: Array<any> = [];
  public arrayImagesProducts: Array<any> = [];
  public tercerArray: Array<any> = [];

  constructor(private productoService: ProductoService, private utilService: UtilService, private router: Router) { }

  ngOnInit() {
    this.getProducts();
  }

  redirectOtherProduct(idproduct) {

    this.router.navigateByUrl("refresh", { skipLocationChange: true }).then(() => {

      this.router.navigate(['/tienda/' + idproduct]);

    });
  }


  getProducts() {

    let image = { tipo_consulta: "2" };

    this.productoService.getProductosComplementarios().subscribe(response => {


      this.arrayProducts = response;



      this.productoService.getImageProduct(image).subscribe(response1 => {

        this.arrayImagesProducts = response1["productos"];

        for (var i = 0; i < this.arrayProducts.length; i++) {

          for (var j = 0; j < this.arrayImagesProducts.length; j++) {

            if (this.arrayProducts[i]["id_pro"] === Number(this.arrayImagesProducts[j]["id_producto"])) {



              this.tercerArray.push({
                id_pro: this.arrayProducts[i]["id_pro"],
                linea_de_negocio: this.arrayProducts[i]["linea_de_negocio"],
                presentacion: this.arrayProducts[i]["presentacion"],
                nombre_producto: this.arrayProducts[i]["nombre_producto"],
                precio_con_igv: this.arrayProducts[i]["precio_con_igv"],
                stock_actual: this.arrayProducts[i]["stock_actual"],
                stock_reservado: this.arrayProducts[i]["stock_reservado"],
                stock_disponible: this.arrayProducts[i]["stock_disponible"],
                cantidad: 1,
                porcentaje: "0",
                nuevo_precio: this.arrayProducts[i]["precio_con_igv"],
                imagen: this.arrayImagesProducts[j]["imagen_principal"]
              })
            }


          }
        }


      })

    },
      error => {
        console.log(error);
      });




  }


}
