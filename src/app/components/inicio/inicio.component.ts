import { Component, OnInit, AfterViewInit } from "@angular/core";
import { ProductoService } from "../../services/producto.service";
import { BlogService } from "../../services/blog.service";
import { UtilService } from "../../utils/util.service";
import { PortadasService } from "src/app/services/portadas.service";
import { Subscription } from "rxjs";
import Swal from "sweetalert2";
import { Router } from "@angular/router";

@Component({
  selector: "app-inicio",
  templateUrl: "./inicio.component.html",
  styleUrls: ["./inicio.component.css"],
  preserveWhitespaces: false,
})
export class InicioComponent implements OnInit {
  public arrayProducts: Array<any> = [];
  public arrayImagesProducts: Array<any> = [];
  public arrayNuevo: Array<any>;
  public post: Object[];
  public arrayPortadas: Object[];
  public objPopUpo: object;
  public tercerArray: Array<any> = [];
  public popUp: Subscription;
  public sub_blog: Subscription;
  public sub_portada: Subscription;

  constructor(
    public router: Router,
    private productoService: ProductoService,
    private blogService: BlogService,
    private utilService: UtilService,
    private portadaService: PortadasService
  ) {
    this.post = [];
  }

  ngOnInit() {
    window.scrollTo(0, 0);

    $(".imgIconMovilmenuFinal").attr("src", "assets/img/icons/icon-menu.svg");
    $(".imgIconMoviltiendaFinal").attr(
      "src",
      "assets/img/icons/icon-tienda.svg"
    );
    $(".imgIconMovilCartFinal").attr("src", "assets/img/icons/icon-cart.svg");
    $(".imgIconMovilUserFinal").attr("src", "assets/img/icons/icon-user.svg");

    $(".navigationFixed").removeClass("fondito-berries");
    $(".content-tienda").removeClass("header-tienda");
    $(".navbar .nav-item .nav-link").css("color", "#ffffff");
    $(".navbar .nav-item .dropdown-toggle").removeClass("border-dropdown-gray");
    $(".navbar .nav-item .rounded-market").removeClass("border-button-nav");
    $("#img-market").attr("src", "assets/img/icons/icon-market.svg");
    $("#img-cart").attr("src", "assets/img/icons/icon-cart.svg");
    $("#img-user").attr("src", "assets/img/icons/icon-user.svg");
    document.getElementById("scrolling-navbar").style.background =
      "transparent";

    this.getPortada();
    this.getProducts();
    this.getPostBlog();
    this.getPopUp();
    console.log(this.popUp);
  }

  getUrlCategoria(categoria) {
    if (categoria == "MERMELADAS") {
      return "/productos/mermeladas";
    }

    if (categoria == "CONGELADOS") {
      return "/productos/congelados";
    }

    if (categoria == "MIELES") {
      return "/productos/miel-de-abeja";
    }

    if (categoria == "PULPAS") {
      return "/productos/pulpas";
    }
  }

  getProducts() {
    //tipo consulta 1 -> productos por Categoria , idparametro categoria que deseo
    //tipo consulta 2 -> solo un producto , id parametro id producto que deseo ( solo para uno)

    let image = { tipo_consulta: "2" };

    this.productoService.getProductosPopulares().subscribe(
      (response) => {
        this.arrayProducts = response;

        this.productoService.getImageProduct(image).subscribe((response1) => {
          var arrayIdItemsWImages = [];
          var arrayIdItemsSystem = [];

          this.arrayImagesProducts = response1["productos"];

          for (var j = 0; j < this.arrayImagesProducts.length; j++) {
            arrayIdItemsWImages.push(
              Number(this.arrayImagesProducts[j]["id_producto"])
            );
          }

          for (var k = 0; k < this.arrayProducts.length; k++) {
            arrayIdItemsSystem.push(Number(this.arrayProducts[k]["id_pro"]));
          }

          for (var i = 0; i < this.arrayProducts.length; i++) {
            if (arrayIdItemsWImages.includes(this.arrayProducts[i]["id_pro"])) {
              this.tercerArray.push({
                id_pro: this.arrayProducts[i]["id_pro"],
                linea_de_negocio: this.arrayProducts[i]["linea_de_negocio"],
                link_negocio: this.getUrlCategoria(
                  this.arrayProducts[i]["linea_de_negocio"]
                ),
                presentacion: this.arrayProducts[i]["presentacion"],
                nombre_producto: this.arrayProducts[i]["nombre_producto"],
                precio_con_igv: this.arrayProducts[i]["precio_con_igv"],
                stock_actual: this.arrayProducts[i]["stock_actual"],
                stock_reservado: this.arrayProducts[i]["stock_reservado"],
                stock_disponible: this.arrayProducts[i]["stock_disponible"],
                cantidad: 1,
                porcentaje: "0",
                nuevo_precio: this.arrayProducts[i]["precio_con_igv"],
                imagen: this.arrayImagesProducts[
                  arrayIdItemsWImages.indexOf(this.arrayProducts[i]["id_pro"])
                ]["imagen_principal"],
              });
            } else {
              this.tercerArray.push({
                id_pro: this.arrayProducts[i]["id_pro"],
                linea_de_negocio: this.arrayProducts[i]["linea_de_negocio"],
                link_negocio: this.getUrlCategoria(
                  this.arrayProducts[i]["linea_de_negocio"]
                ),
                presentacion: this.arrayProducts[i]["presentacion"],
                nombre_producto: this.arrayProducts[i]["nombre_producto"],
                precio_con_igv: this.arrayProducts[i]["precio_con_igv"],
                stock_actual: this.arrayProducts[i]["stock_actual"],
                stock_reservado: this.arrayProducts[i]["stock_reservado"],
                stock_disponible: this.arrayProducts[i]["stock_disponible"],
                cantidad: 1,
                porcentaje: "0",
                nuevo_precio: this.arrayProducts[i]["precio_con_igv"],
                imagen: "assets/img/icons/image-not-found.png",
              });
            }
          }
        });
      },
      (error) => {
        console.log(error);
      }
    );
  }

  getPostBlog() {
    this.sub_blog = this.blogService.getAllArticles().subscribe(
      (response) => {
        let shuffled = response.sort(() => 0.5 - Math.random());

        // Trae un sub-array de n elementos despues de ordenarlos.
        this.post = shuffled.slice(0, 3);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  getPortada() {
    this.sub_portada = this.portadaService.getPortadas().subscribe(
      (response) => {
        if (response["success"] == true) {
          this.arrayPortadas = response["portadas"];
        }
        /* else {

        console.log(response["message"]);

      }*/
      },
      (error) => {
        console.log(error);
      }
    );
  }

  getPopUp() {
    this.popUp = this.portadaService.getPopUp().subscribe(
      (response) => {
        if (response["success"] == true) {
          this.objPopUpo = response["portadas"];
          console.log(response["portadas"]);

          if ($(window).width() > 800) {
            Swal.fire({
              width: 900,
              showCancelButton: true,
              heightAuto: true,
              imageUrl: response["portadas"][0].popup,
              padding: "3em",
              cancelButtonText: "CERRAR  X",
              confirmButtonText: "IR A TIENDA",
              customClass: {
                cancelButton: "cancel-button",
                container: "swal-content",
              },
              backdrop: `
              rgba(0,0,0,0.4)
              left top
              no-repeat
            `,
            }).then((result) => {
              console.log(result);
              if (result.value) {
                this.router.navigate(["tienda"]);
              }
            });
          } else {
            Swal.fire({
              heightAuto: true,
              imageUrl: response["portadas"][0].responsive,
              width: 400,
              padding: "3em",
              showCancelButton: true,
              cancelButtonText: "Cerrar X",
              customClass: {
                container: "swal-content",
                confirmButton: "custom-swal-button",
                cancelButton: "cancel-button",
              },
              confirmButtonText: "IR A TIENDA",

              //  background: `#fff url(${response["portadas"][0].responsive}) no-repeat center center`,
              backdrop: `
              rgba(0,0,0,0.4)
              left top
              no-repeat
            `,
            }).then((result) => {
              console.log(result);
              if (result.value) {
                this.router.navigate(["tienda"]);
              }
            });
          }
        }
        /* else {

        console.log(response["message"]);

      }*/
      },
      (error) => {
        console.log(error);
      }
    );
  }

  ngOnDestroy() {
    this.sub_portada.unsubscribe();
    this.popUp.unsubscribe();
    this.sub_blog.unsubscribe();
  }
}
