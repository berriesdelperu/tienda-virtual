import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { ContactoService } from '../../../services/contacto.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import Swal from 'sweetalert2';
import * as departamentos from '../../../json/departamentos.json';
import * as provincias from '../../../json/provincias.json';
import * as distritos from '../../../json/distritos.json';
import { ProductosDestacadosComponent } from '../../productos-destacados/productos-destacados.component';
import { UtilService } from 'src/app/utils/util.service';

@Component({
  selector: 'app-libro-de-reclamaciones',
  templateUrl: './libro-de-reclamaciones.component.html',
  styleUrls: ['./libro-de-reclamaciones.component.css']
})
export class LibroDeReclamacionesComponent implements OnInit {
  public docMaxLength: number;
  public bien_contratado: string;
  public tipo: string;
  public selectedComprobante: string;
  public confirmarTitular: Boolean;
  public numero_comprobante: string;
  public detalle_producto: string;
  public detalle_reclamo: string;
  public nombres: string;
  public doc_identidad: string;
  public num_doc: string;
  public telefono: string;
  public correo: string;
  public domicilio: string;
  public selectedDepartamento: string;
  public selectedProvincia: string;
  public selectedDistrito: string;
  public listDepartamentos: Object[];
  public listProvincias: Object[];
  public listDistritos: Object[];
  @ViewChild(ProductosDestacadosComponent, { static: true }) complemento: ProductosDestacadosComponent;




  constructor(private serviceContact: ContactoService, private router: Router, public location: Location, public utilService: UtilService) {
    this.bien_contratado = "";
    this.tipo = "";
    this.selectedComprobante = "";
    this.numero_comprobante = "";
    this.detalle_producto = "";
    this.detalle_reclamo = "";
    this.nombres = "";
    this.doc_identidad = "";
    this.num_doc = "";
    this.telefono = "";
    this.correo = "";
    this.domicilio = "";
    this.selectedDepartamento = "";
    this.selectedProvincia = "";
    this.docMaxLength = 8;
    this.selectedDistrito = "";
    this.confirmarTitular = false;
    this.listDepartamentos = departamentos["default"];
  }

  ngOnInit() {

    $(".imgIconMovilmenuFinal").attr("src", "assets/img/icons/icon-menu.svg");
    $(".imgIconMoviltiendaFinal").attr("src", "assets/img/icons/icon-tienda.svg");
    $(".imgIconMovilCartFinal").attr("src", "assets/img/icons/icon-cart.svg");
    $(".imgIconMovilUserFinal").attr("src", "assets/img/icons/icon-user.svg");

    window.scrollTo(0, 0);
    $("#content-tienda").removeClass('header-tienda');
    $('.navbar .nav-item .nav-link').css('color', "#fff");
    $('.navbar .nav-item .dropdown-toggle').removeClass('border-dropdown-gray');
    $('.navbar .nav-item .rounded-market').removeClass('border-button-nav');
    $('.navbar .nav-item .circle-cart').toggleClass('span-circle-cart');
    $("#img-market").attr("src", "assets/img/icons/icon-market.svg");
    $("#img-cart").attr("src", "assets/img/icons/icon-cart.svg");
    $("#img-user").attr("src", "assets/img/icons/icon-user.svg");
  }


  changeComprobante($event) {
    this.selectedComprobante = $event.target.value;
  }

  changeDepartamento($event) {
    this.selectedDepartamento = $event.target.value;
    this.listProvincias = provincias["default"].filter((item) => item.department_id == this.selectedDepartamento);
    this.selectedProvincia = ""
    this.selectedDistrito = ""
  }
  changeProvincia($event) {
    this.selectedProvincia = $event.target.value;
    this.listDistritos = distritos["default"].filter((item) => item.province_id == this.selectedProvincia);
    this.selectedDistrito = ""

  }
  changeDistrito($event) {
    this.selectedDistrito = $event.target.value;
  }

  confirmarTerminosCheck($event) {
    if ($event.target.checked == true) {
      this.confirmarTitular = true;
    } else {
      this.confirmarTitular = false;
    }

  }

  changeDoc(event) {
    if (event.target.value == "DNI") {
      this.docMaxLength = 8
    } else if (event.target.value == "RUC") {
      this.docMaxLength = 11
    } else {
      this.docMaxLength = 9
    }
  }

  enviarLibroReclamaciones() {

    if (this.bien_contratado == "") {
      Swal.fire("Oops...", "Seleccione el bien contratado", "error");
      return false;

    } else if (this.tipo == "") {
      Swal.fire("Oops...", "Seleccione el tipo", "error");

      return false;
    } else if (this.selectedComprobante == "") {
      Swal.fire("Oops...", "Seleccione si es comprobante", "error");

      return false;
    } else if (this.numero_comprobante == "" || this.numero_comprobante.length < 20) {
      Swal.fire("Oops...", "Ingrese el número de comprobante válido", "error");

      return false;
    } else if (this.detalle_producto == "") {
      Swal.fire("Oops...", "Ingrese el detalle del producto o servicio", "error");

      return false;
    } else if (this.detalle_reclamo == "") {
      Swal.fire("Oops...", "Ingrese el detalle del reclamo o queja", "error");

      return false;
    } else if (this.nombres == "") {
      Swal.fire("Oops...", "Ingrese su(s) nombre(s) y apellidos", "error");

      return false;
    } else if (this.doc_identidad == "") {
      Swal.fire("Oops...", "Ingrese su documento de identidad ( Si es DNI , CE o Pasaporte)", "error");

      return false;
    } else if (this.num_doc == "" || this.num_doc.length < 8) {
      Swal.fire("Oops...", "Ingrese el número de su documento de identidad", "error");

      return false;
    } else if (this.telefono == "" || this.telefono.length < 9) {
      Swal.fire("Oops...", "Ingrese su teléfono válido de 9 dígitos", "error");

      return false;
    } else if (!this.utilService.validateEmail(this.correo)) {


      Swal.fire({
        title: "<img src='assets/img/tiendas/iconocorreoalert.png' style='height:100px;margin-bottom:20px' />",
        html: "Por favor, ingrese su correo",
        confirmButtonText: 'DE ACUERDO',
      });

      return false;
    } else if (this.domicilio == "") {
      Swal.fire("Oops...", "Ingrese la dirección de su domicilio", "error");

      return false;
    } else if (this.selectedDepartamento == "") {
      Swal.fire("Oops...", "Ingrese el departamento", "error");

      return false;
    } else if (this.selectedProvincia == "") {
      Swal.fire("Oops...", "Ingrese la provincia", "error");

      return false;
    } else if (this.selectedDistrito == "") {
      Swal.fire("Oops...", "Ingrese el distrito", "error");

      return false;
    } else if (!this.confirmarTitular) {
      Swal.fire("Oops...", "Seleccione la casilla de declaración jurada", "error");

      return false;

    } else {

      var data = {
        bien_contratado: this.bien_contratado, tipo_reclamo: this.tipo, tipo_comprobante: this.selectedComprobante,
        nro_comprobante: this.numero_comprobante, detalle_bien: this.detalle_producto, detalle_reclamo: this.detalle_reclamo,
        nombres: this.nombres, tipo_documento: this.doc_identidad, nro_documento: this.num_doc, telefono: this.telefono,
        correo: this.correo, direccion: this.domicilio, departamento: this.selectedDepartamento, provincia: this.selectedProvincia, distrito: this.selectedDistrito
      }


      this.serviceContact.registarReclamationsBook(data).subscribe(response => {


        if (response["success"] == true) {
          Swal.fire({
            type: 'success',
            title: 'Formulario Registrado!',
            text: 'Sus datos fueron enviados!',
          }).then(() => {
            this.router.navigateByUrl("/refresh", { skipLocationChange: true }).then(() => {

              this.router.navigate([decodeURI(this.location.path())])

            });
          });

        }
        /*else{
          console.log("Surgio un error");
        }*/


      },
        error => {
          console.log(error);
        });



    }


  }





}
