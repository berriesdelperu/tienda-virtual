import { Injectable } from "@angular/core";

@Injectable()
export class urlService {
  // prod
  public domain_berries: String =
    "https://intranet.berriesdelperu.com/api/v1.0/";
  public domain_blog_berries: String =
    "https://berriesdelperu.com/berries-blog/wp-json/wp/v2/";

  public domain_images_berries: String =
    "https://berriesdelperu.com/berries-admin/webservices/api/";

  // dev

  // public domain_berries: String =
  //   "https://entorno.berriesdelperu.com/api/v1.0/";
  // public domain_blog_berries: String =
  //   "https://dev.berriesdelperu.com/berries-blog/wp-json/wp/v2/";

  // public domain_images_berries: String =
  //   "https:/dev.berriesdelperu.com/berries-admin/webservices/api/";
}
