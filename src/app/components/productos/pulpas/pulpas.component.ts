import {
  Component,
  OnInit,
  ViewChild,
  AfterViewInit,
  HostListener,
} from "@angular/core";
import { ProductoService } from "../../../services/producto.service";
import { Router } from "@angular/router";
import { UtilService } from "../../../utils/util.service";
import { ComplementosComponent } from "../../complementos/complementos.component";

@Component({
  selector: "app-pulpas",
  templateUrl: "./pulpas.component.html",
  styleUrls: ["./pulpas.component.css"],
})
export class PulpasComponent implements OnInit {
  public arrayProductsPulpas: Array<any> = [];
  public listPulpas: Array<any> = [];
  public slidesPulpas: any = [[]];
  public arrayImagesProductsPulpas: Array<any> = [];
  public auxArray: Array<any> = [];

  public slidesCategorias: any = [[]];
  public listCategorias: Array<any> = [];

  public countItems = 4;
  public innerWidth: any;

  @ViewChild(ComplementosComponent, { static: true })
  complemento: ComplementosComponent;

  constructor(
    private productoService: ProductoService,
    public router: Router,
    private utilService: UtilService
  ) {
    if (innerWidth < 992) {
      this.countItems = 2;
    }

    if (innerWidth < 769) {
      this.countItems = 1;
    }
  }

  ngOnInit() {

    this.innerWidth = window.innerWidth;
    window.scrollTo(0, 0);
    $(".imgIconMovilmenuFinal").attr(
      "src",
      "assets/img/icons/icon-menu.svg"
    );
    $(".imgIconMoviltiendaFinal").attr(
      "src",
      "assets/img/icons/icon-tienda.svg"
    );
    $(".imgIconMovilCartFinal").attr(
      "src",
      "assets/img/icons/icon-cart.svg"
    );
    $(".imgIconMovilUserFinal").attr(
      "src",
      "assets/img/icons/icon-user.svg"
    );
    $(".navigationFixed").removeClass("fondito-berries");
    $(".content-tienda").removeClass('header-tienda');
    $('.navbar .nav-item .nav-link').css('color', "#ffffff");
    $('.navbar .nav-item .dropdown-toggle').removeClass('border-dropdown-gray');
    $('.navbar .nav-item .rounded-market').removeClass('border-button-nav');
    $("#img-market").attr("src", "assets/img/icons/icon-market.svg");
    $("#img-cart").attr("src", "assets/img/icons/icon-cart.svg");
    $("#img-user").attr("src", "assets/img/icons/icon-user.svg");
    document.getElementById("scrolling-navbar").style.background = "transparent"
    this.getPulpas();
    $(".carousel").on("touchstart", function (event) {
      var xClick = event.originalEvent.touches[0].pageX;
      $(this).one("touchmove", function (event) {
        var xMove = event.originalEvent.touches[0].pageX;
        if (Math.floor(xClick - xMove) > 5) {
          ($(this) as any).carousel('next');
        }
        else if (Math.floor(xClick - xMove) < -5) {
          ($(this) as any).carousel('prev');
        }
      });
      $(".carousel").on("touchend", function () {
        $(this).off("touchmove");
      });
    });
  }

  @HostListener("window:resize", ["$event"])
  onResize(event) {
    this.innerWidth = window.innerWidth;
  }



  getPulpas() {
    let id_categoria = { linea: "7" };

    let image = { tipo_consulta: "2" };

    this.productoService
      .getProductsByCategoria(id_categoria)
      .subscribe((response) => {
        this.arrayProductsPulpas = response;

        this.productoService.getImageProduct(image).subscribe((response1) => {
          var arrayIdItemsWImages = [];
          var arrayIdItemsSystem = [];

          this.arrayImagesProductsPulpas = response1["productos"];

          for (var j = 0; j < this.arrayImagesProductsPulpas.length; j++) {
            arrayIdItemsWImages.push(
              Number(this.arrayImagesProductsPulpas[j]["id_producto"])
            );
          }

          for (var k = 0; k < this.arrayProductsPulpas.length; k++) {
            arrayIdItemsSystem.push(
              Number(this.arrayProductsPulpas[k]["id_pro"])
            );
          }

          for (var i = 0; i < this.arrayProductsPulpas.length; i++) {
            if (
              arrayIdItemsWImages.includes(
                this.arrayProductsPulpas[i]["id_pro"]
              )
            ) {
              this.listPulpas.push({
                id_pro: this.arrayProductsPulpas[i]["id_pro"],
                linea_de_negocio: this.arrayProductsPulpas[i][
                  "linea_de_negocio"
                ],
                presentacion: this.arrayProductsPulpas[i]["presentacion"],
                nombre_producto: this.arrayProductsPulpas[i]["nombre_producto"],
                precio_con_igv: this.arrayProductsPulpas[i]["precio_con_igv"],
                stock_actual: this.arrayProductsPulpas[i]["stock_actual"],
                stock_reservado: this.arrayProductsPulpas[i]["stock_reservado"],
                stock_disponible: this.arrayProductsPulpas[i][
                  "stock_disponible"
                ],
                cantidad: 1,
                porcentaje: "0",
                nuevo_precio: this.arrayProductsPulpas[i]["precio_con_igv"],
                imagen: this.arrayImagesProductsPulpas[
                  arrayIdItemsWImages.indexOf(
                    this.arrayProductsPulpas[i]["id_pro"]
                  )
                ]["imagen_principal"],
              });
            } else {
              this.listPulpas.push({
                id_pro: this.arrayProductsPulpas[i]["id_pro"],
                linea_de_negocio: this.arrayProductsPulpas[i][
                  "linea_de_negocio"
                ],
                presentacion: this.arrayProductsPulpas[i]["presentacion"],
                nombre_producto: this.arrayProductsPulpas[i]["nombre_producto"],
                precio_con_igv: this.arrayProductsPulpas[i]["precio_con_igv"],
                stock_actual: this.arrayProductsPulpas[i]["stock_actual"],
                stock_reservado: this.arrayProductsPulpas[i]["stock_reservado"],
                stock_disponible: this.arrayProductsPulpas[i][
                  "stock_disponible"
                ],
                cantidad: 1,
                porcentaje: "0",
                nuevo_precio: this.arrayProductsPulpas[i]["precio_con_igv"],
                imagen: "assets/img/icons/image-not-found.png",
              });
            }
          }

          this.listCategorias.push({
            img: "assets/img/icons/icon-honey-1.png",
            titulo: "Tiempo de Vida",
            descripcion: "Puedes disfrutar de estas pulpas hasta por 12 meses.",
          });

          this.listCategorias.push({
            img: "assets/img/icons/icon-honey-2.png",
            titulo: "Conservación",
            descripcion:
              "Para mantener nuestras pulpas en buen estado, mantenlas siempre congeladas.",
          });

          this.listCategorias.push({
            img: "assets/img/icons/icon-honey-3.png",
            titulo: "TIPS",
            descripcion:
              "Experimenta los mejores colores, sabores y aromas de nuestra Amazonía con estas pulpas de frutas recolectadas en su entorno natural por comunidades nativas. Además, están envasadas de forma amigable para que disfrutes de todas sus bondades.",
          });

          this.listCategorias.push({
            img: "assets/img/icons/icon-honey-4.png",
            titulo: "Descripción linea",
            descripcion:
              "Ideales para preparar smoothies, jugos, postres y salsas. Encontrarás deliciosas recetas en nuestro blog. Es importante mantenerlas siempre congeladas para evitar su oxidación y fermentación.",
          });

          // if(this.listPulpas.length < 5){
          //   $(".rowRightProductosRelacionados").hide();
          // }

          this.slidesCategorias = this.utilService.chunk(
            this.listCategorias,
            1
          );
          ($('#ContenedorSlider') as any).carousel({
            interval: 2000,
            cycle: true
          });
          this.slidesPulpas = this.utilService.chunk(
            this.listPulpas,
            this.countItems
          );
        });
      });
  }

  goBlog() {
    this.router.navigate(["/blog"]);
  }
}
