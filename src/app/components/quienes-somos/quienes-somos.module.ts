import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QuienesSomosRoutingModule } from './quienes-somos-routing.module';
import { LaEmpresaComponent } from './la-empresa/la-empresa.component';
import { NuestrosConveniosComponent } from './nuestros-convenios/nuestros-convenios.component';
import { ProductosDestacadosModule } from '../productos-destacados/productos-destacados.module';

@NgModule({
  declarations: [LaEmpresaComponent, NuestrosConveniosComponent],
  imports: [
    CommonModule,
    QuienesSomosRoutingModule,
    ProductosDestacadosModule
  ]
})
export class QuienesSomosModule { }
