import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CarritoComprasRoutingModule } from './carrito-compras-routing.module';
import { CarritoComprasComponent } from './carrito-compras.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { Select2Module } from 'ng2-select2';
import { AgmCoreModule } from '@agm/core';




@NgModule({
  declarations: [CarritoComprasComponent  ],
  imports: [
    CommonModule,
    CarritoComprasRoutingModule,
    FormsModule,
    RouterModule,
    Select2Module,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCdMfbm6AJUE-9lHGdcF0mSMz0EehAK9qQ',
      libraries: ['places']
    })

  ],
})
export class CarritoComprasModule { }
