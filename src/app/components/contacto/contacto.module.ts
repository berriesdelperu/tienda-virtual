import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactoRoutingModule } from './contacto-routing.module';
import { FormularioComponent } from './formulario/formulario.component';
import { LibroDeReclamacionesComponent } from './libro-de-reclamaciones/libro-de-reclamaciones.component';
import { FormsModule } from '@angular/forms';
import { ProductosDestacadosModule } from '../productos-destacados/productos-destacados.module';


@NgModule({
  declarations: [FormularioComponent, LibroDeReclamacionesComponent ],
  imports: [
    CommonModule,
    ContactoRoutingModule,
    FormsModule,
    ProductosDestacadosModule

  ]
})
export class ContactoModule { }
