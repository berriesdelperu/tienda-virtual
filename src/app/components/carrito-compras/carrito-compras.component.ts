import { Component, OnInit, ElementRef, ViewChild, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { Producto } from '../../models/producto';
import { CartService } from '../../services/cart.service';
import { Observable, of } from 'rxjs';
import Swal from 'sweetalert2';
import { ProductoService } from 'src/app/services/producto.service';
import { UtilService } from 'src/app/utils/util.service';
import { DireccionesService } from 'src/app/services/direcciones.service';
import { CuponesService } from 'src/app/services/cupones.service';
import { Select2OptionData } from 'ng2-select2';
import { MapsAPILoader, MouseEvent } from '@agm/core';


declare var $: any;
@Component({
  selector: 'app-carrito-compras',
  templateUrl: './carrito-compras.component.html',
  styleUrls: ['./carrito-compras.component.css']
})
export class CarritoComprasComponent implements OnInit {


  public idpersona: any;
  public shoppingCartItems$: Observable<Producto[]> = of([]);
  public shoppingCartItems;
  public totalProductos: number;
  public confirmarTerminos: Boolean;

  public totalBruto: number;
  public delivery: number;
  public totalNeto: number;

  public storageDatosUsuario: any;
  public direccion_principal: string;
  public listaDirecciones: Array<any> = [];


  public arrayProducts: Array<any> = [];
  public arrayImagesProducts: Array<any> = [];
  public tercerArray: Array<any> = [];

  public listaCheckout: Array<any> = [];

  public id_direccion: number;
  public precio_envio_general: any;
  public precio_envio: string;
  public select_direccion: any;
  public selectedDireccionSecond: string;

  public cupon_descuento: string;
  public totalDscto: number;
  public descripcion_descuento: string;


  public bloquearbotonCupon: Boolean;
  public bloquearSelectDireccion: Boolean;

  public showcanceladdCupon: Boolean;
  public showcancelbtnCupon: Boolean;


  public isNoDireccion: Boolean;



  public listProvincias: Array<Select2OptionData>;
  public optionsSelect2Provincias: Select2Options;
  public selectProvincia: string;

  public listDistritos: Array<Select2OptionData>;
  public optionsSelect2Distritos: Select2Options;
  public selectDistrito: string;

  public listOpcionDireccion: Array<Select2OptionData>;
  public optionsSelect2TipoDireccion: Select2Options;
  public selectTipoDireccion: string;

  public direccion_otras: string;
  public recojo_tienda: boolean

  public numero_direccion: string;
  public referencia: string;
  public direccion_principal_show: string;
  public listPrecioEnvio: Object[];


  @ViewChild('searchAgregar', { static: true }) searchElementRef: ElementRef;
  private geoCoder;
  public latitude: number;
  public longitude: number;
  public zoom: number;
  public address: string;





  constructor(public router: Router, private cartService: CartService, private direccionesService: DireccionesService,
    private productoService: ProductoService, public utilService: UtilService, public cuponesService: CuponesService,
    private mapsAPILoader: MapsAPILoader, private ngZone: NgZone) {

    this.shoppingCartItems$ = this.cartService.getItems();
    this.totalProductos = 0;
    this.precio_envio_general = 0;
    this.totalNeto = 0;
    this.totalDscto = 0;
    this.confirmarTerminos = false;
    this.id_direccion = 0;
    this.precio_envio = "";
    this.cupon_descuento = "";
    this.storageDatosUsuario = JSON.parse(localStorage.getItem("datospersona"));
    this.descripcion_descuento = "";

    this.select_direccion = "";
    this.selectedDireccionSecond = "";
    this.recojo_tienda = true

    this.bloquearbotonCupon = false;
    this.bloquearSelectDireccion = true;
    this.showcanceladdCupon = true;
    this.showcancelbtnCupon = false;
    this.isNoDireccion = false;
    this.selectProvincia = "";
    this.selectDistrito = "";
    this.selectTipoDireccion = "";
    this.numero_direccion = "";


    this.optionsSelect2Provincias = {
      placeholder: "[ Seleccione Provincia ]",
      width: "100%"
    }

    this.optionsSelect2Distritos = {
      placeholder: "[ Seleccione distrito ]",
      width: "100%"
    }

    this.optionsSelect2TipoDireccion = {
      placeholder: "[ Seleccione Tipo de dirección ]",
      width: "100%"

    }


    this.shoppingCartItems$.subscribe(response => {

      this.shoppingCartItems = response;

    });


    var auxiliar = JSON.parse(localStorage.getItem("datospersona"));

    this.idpersona = { id_cli_nat: auxiliar["id_cli_nat"] };


    this.direccionesService.refreshListaDireccion().subscribe(() => {

      this.listaDireccion();

    });

    this.listaDireccion();
  }




  ngOnInit() {
    window.scrollTo(0, 0);

    $(".imgIconMovilmenuFinal").attr("src", "assets/img/icons/icon-menu-2.svg");
    $(".imgIconMoviltiendaFinal").attr("src", "assets/img/icons/icon-tienda-2.svg");
    $(".imgIconMovilCartFinal").attr("src", "assets/img/icons/icon-cart-2.svg");
    $(".imgIconMovilUserFinal").attr("src", "assets/img/icons/icon-user-2.svg");

    $(".content-tienda").addClass('header-tienda');
    $('.navbar .nav-item .nav-link').css('color', "#778489");
    $('.navbar .nav-item .dropdown-toggle').addClass('border-dropdown-gray');
    $('.navbar .nav-item .rounded-market').addClass('border-button-nav');
    $("#img-market").attr("src", "assets/img/icons/icon-market-gray.svg");
    $("#img-cart").attr("src", "assets/img/icons/icon-cart-gray.svg");
    $("#img-user").attr("src", "assets/img/icons/icon-user-gray.svg");

    this.autocomplete();
    this.getProvincias();
    this.getOpcionDireccion();
    this.getProductsComplemento();
    if (Number(this.getTotalBruto()) < 65) {
      this.triggerSwal(true, "SÍ, IR AL CHECK-OUT")
    }

  }

  triggerSwal(show, msg) {

    Swal.fire({
      html: 'El pedido minimo para delivery es de ' + '<b>S/.65.00</b>' +
        '<br>' + 'Compras menores a esa cantidad serán' +
        '<br>' + 'recogidas en nuestra oficina' +
        '<br>' + '<br>' + '<b>Jiron las Uvas (Mz X Lote 44)</b>' + '<br>' +
        'Urb. los jazmines, Surco' +
        '<div class="mt-4" >¿Desea continuar?</div>',
      imageUrl: 'assets/img/icons/group-22@2x.png',
      imageWidth: 120,
      imageHeight: 120,
      imageAlt: 'Custom image',
      confirmButtonText: msg,
      confirmButtonClass: "swal__button",
      cancelButtonClass: "swal__button swal__button--cancel",
      cancelButtonText: "SEGUIR COMPRANDO",
      cancelButtonColor: "#ffc359",
      showCancelButton: show,

    }).then(result => {
      if (!result.value) {
        this.router.navigate(['tienda'])
      }
    })
  }


  autocomplete() {
    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation();
      this.geoCoder = new google.maps.Geocoder;

      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"],
        componentRestrictions: { 'country': 'PE' }
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();


          this.direccion_otras = place.formatted_address;

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 15;
        });
      });
    });

  }

  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 15;
        this.getAddress(this.latitude, this.longitude);
      });
    }
  }

  markerDragEnd($event: MouseEvent) {

    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    this.getAddress(this.latitude, this.longitude);
  }

  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 15;
          this.address = results[0].formatted_address;
          this.direccion_otras = this.address;
        } else {
          window.alert('No se encontraron resultados GEOCODER');
        }
      } else {
        window.alert('Geocoder fallido : ' + status);
      }

    });
  }




  add = (indice) => {


    var maxSizeProduct = $(".input-cantidad_" + indice).attr('max');
    var idprodSelected = $(".input-cantidad_" + indice).attr('data-idprod');
    var newVal;
    var oldValue = parseFloat($(".input-cantidad_" + indice).val());
    this.shoppingCartItems.forEach((value) => {

      if (Number(value.id_pro) == Number(idprodSelected)) {

        if (oldValue >= maxSizeProduct) {
          newVal = oldValue;
        } else {
          newVal = oldValue + 1;
          this.cartService.addToCart({ ...value, cantidad: 1 })
        }
        $(".input-cantidad_" + indice).val(newVal);
        value.cantidad = newVal;

      }


    });

  }


  resta = (indice) => {

    var minSizeProduct = $(".input-cantidad_" + indice).attr('min');
    var idprodSelected = $(".input-cantidad_" + indice).attr('data-idprod');
    var newVal;
    var oldValue = parseFloat($(".input-cantidad_" + indice).val());


    this.shoppingCartItems.forEach((value) => {

      if (Number(value.id_pro) == Number(idprodSelected)) {

        if (oldValue <= minSizeProduct) {
          newVal = oldValue;
        } else {
          newVal = oldValue - 1;
          this.cartService.addToCart({ ...value, cantidad: -1 })

        }
        $(".input-cantidad_" + indice).val(newVal);
        value.cantidad = newVal;

        if (this.totalBruto < 65 + Number(value.nuevo_precio) && this.totalBruto >= 65) {
          this.select_direccion = ""
          this.precio_envio_general = 0
          this.triggerSwal(false, "DE ACUERDO")
        }
      }


    });



  }










  getTotalBruto() {

    this.totalBruto = 0;
    for (let i = 0; i < this.shoppingCartItems.length; i++) {
      this.totalBruto += (this.shoppingCartItems[i].cantidad * this.shoppingCartItems[i].nuevo_precio);
    }

    return (this.totalBruto).toFixed(2);
  }

  validarCupon() {

    var cupon = { nro_cupon: this.cupon_descuento };

    this.cuponesService.validarCupon(cupon).subscribe(response => {

      if (response["success"] == true) {

        this.bloquearbotonCupon = true;
        this.showcanceladdCupon = false;
        this.showcancelbtnCupon = true;
        $("#msj-cupon").html('<span class="text-good-cupon">Cupón válido!</span>');

        this.totalDscto = Number(response["cupon"]["descuento"]);

        this.descripcion_descuento = response["cupon"]["descripcion"];

        // $("#input-descuento").removeClass("input-descuento");

        $("#descripcion-descuento").html('<div class="my-2">' +
          '<strong class="title-cart-descuento mt-3 mb-3">' + this.descripcion_descuento + ': </strong>' +
          '<strong class="price-cart-descuento" style="font-family:Ubuntu;">' + this.totalDscto + '%</strong>' +
          '</div>');
        $("#input-descuento").addClass("input-good-descuento");


      } else {
        this.showcancelbtnCupon = false;
        //$("#input-descuento").removeClass("input-good-descuento");

        $("#msj-cupon").html('<span class="text-error-cupon">' + response["message"] + '</span>');
        $("#input-descuento").addClass("input-descuento");

        /*  Swal.fire("Oops...",response["message"],"error"); */

      }
    },
      error => {
        console.log(error);
      })

  }


  cancelarCupon() {

    this.bloquearbotonCupon = false;
    this.showcanceladdCupon = true;
    this.showcancelbtnCupon = false;
    this.totalDscto = 0;
    $("#msj-cupon").html('');
    $("#input-descuento").removeClass("input-good-descuento");
    $("#descripcion-descuento").html("");

  }

  confirmarTerminosCheck($event) {
    if ($event.target.checked == true) {
      this.confirmarTerminos = true;
    } else {
      this.confirmarTerminos = false;
    }

  }





  public removeProduct(item: Producto) {
    this.cartService.removeFromCart(item);

    if (this.totalBruto < 65 + Number(item["precio_con_igv"] * item["cantidad"]) && this.totalBruto >= 65) {
      this.select_direccion = ""
      this.precio_envio_general = 0
      this.triggerSwal(false, "DE ACUERDO")
    }

  }


  public getProductDetail() {

    for (var i = 0; i < this.shoppingCartItems.length; i++) {

      this.listaCheckout.push({
        producto: String(this.shoppingCartItems[i]["id_pro"]),
        cantidad: String(this.shoppingCartItems[i]["cantidad"]),
        precio_unitario: this.shoppingCartItems[i]["precio_con_igv"],
        total: String(this.shoppingCartItems[i]["cantidad"] * this.shoppingCartItems[i]["precio_con_igv"])
      })

    }

    return this.listaCheckout
  }

  changeOtherDireccion($event) {



    //this.selectedDireccionSecond = $event.target.value;
    this.selectedDireccionSecond = $event.target.id;
    $('.select-cart span').text($event.target.innerText);

    var precio_envio = this.selectedDireccionSecond.split("-");
    this.precio_envio_general = precio_envio[1];


    if (this.selectedDireccionSecond == "otro-0") {

      $('#modalRegistrarDireccion').modal("show");
    }



  }

  changeTipoDireccion($event) {

    this.selectTipoDireccion = $event.value;

  }

  getProvincias() {
    this.direccionesService.getProvincias().subscribe(response => {

      var arrayAuxReg = [];
      arrayAuxReg.push({ id: '', text: "" });

      for (let index = 0; index < response["length"]; index++) {

        arrayAuxReg.push({
          id: response[index]['id_provincia'],
          text: response[index]['provincia']
        });

      }
      this.listProvincias = arrayAuxReg;

    },
      error => {
        console.log(error);
      })


  }

  changeProvincias($event) {
    this.listDistritos = null

    this.direccion_otras = ""
    this.numero_direccion = ""
    this.referencia = ""
    $('#selectTipo select').val(null).trigger('change')

    this.selectProvincia = $event.value;

    let provincia = { id_provincia: this.selectProvincia };

    this.getDistritos(provincia);

  }

  getDistritos(provincia) {


    this.direccionesService.getDistritos(provincia).subscribe(response => {
      var arrayAuxReg = [];
      arrayAuxReg.push({ id: '', text: "" });

      for (let index = 0; index < response["length"]; index++) {

        arrayAuxReg.push({
          id: response[index]['id_distrito'],
          text: response[index]['distrito']
        });

      }
      this.listDistritos = arrayAuxReg;
    },
      error => {
        console.log(error);
      })


  }

  changeDistritos($event) {
    this.selectDistrito = $event.value;

    let distrito = { id_distrito: this.selectDistrito };

    this.getPrecioxDireccion(distrito);
  }

  getPrecioxDireccion(distrito) {
    this.direccionesService.getPrecioxDireccion(distrito).subscribe(response => {
      this.listPrecioEnvio = response;
    },
      error => {
        console.log(error);
      })
  }

  getOpcionDireccion() {

    this.direccionesService.getOpcionDireccion().subscribe(response => {

      var arrayAuxReg = [];
      arrayAuxReg.push({ id: '', text: "" });

      for (let index = 0; index < response["length"]; index++) {

        arrayAuxReg.push({
          id: response[index]['id_opcion'],
          text: response[index]['opcion']
        });

      }
      this.listOpcionDireccion = arrayAuxReg;

    })
  }



  agregarDireccion() {

    var id_cliente = this.storageDatosUsuario;

    if (this.selectProvincia == "") {

      Swal.fire("Oops...", "Selecciona la provincia", "error");
      return false

    } else if (this.selectDistrito == "") {

      Swal.fire("Oops...", "Selecciona el distrito", "error");
      return false

    } else if (this.selectTipoDireccion == "") {

      Swal.fire("Oops...", "Selecciona si es casa , oficina o departamento", "error");
      return false

    } else if (this.direccion_otras == "") {

      Swal.fire("Oops...", "Ingresa la dirección", "error");
      return false

    } else if (this.numero_direccion == "") {

      Swal.fire("Oops...", "Ingresa el número de la dirección", "error");
      return false

    } else if (this.numero_direccion == "") {

      Swal.fire("Oops...", "Ingresa el alguna diferencia", "error");
      return false

    } else {



      var registro_direccion;



      registro_direccion = {
        id_cliente: id_cliente["id_cli_nat"], id_provincia: this.selectProvincia, id_distrito: this.selectDistrito,
        id_precio_envio: String(this.listPrecioEnvio[0]["id_envio"]), direccion: this.direccion_otras,
        id_opcion_direccion: this.selectTipoDireccion, numero: this.numero_direccion,
        referencia_1: this.referencia, referencia_2: this.referencia,
        principal: "2", latitud: this.latitude, longitud: this.longitude
      };



      this.direccionesService.crearDireccion(registro_direccion).subscribe(response => {

        Swal.fire({
          title: "<img src='assets/img/tiendas/iconodireccionalert.png' style='height:100px;margin-bottom:20px' />",
          html: "Se agregó la dirección correctamente",
          confirmButtonText: 'DE ACUERDO',
        }).then(() => {
          $('#modalRegistrarDireccion').modal('hide');

          //  this.router.navigate(['/mi-cuenta']);

        });

      },
        error => {
          Swal.fire("Oops...", "Ocurrio un error , intente de nuevo por favor", "error");
        })



    }

  }



  onItemChange(valor) {

    if (valor == 'principal') {
      this.recojo_tienda = false
      this.bloquearSelectDireccion = true;
      this.precio_envio_general = this.precio_envio;

      $('.lista-cart').removeClass('active');
      $('.lista-cart ').find('.lista-cart-menu').slideUp(300);

    } else if (valor == "recojo_tienda") {
      this.recojo_tienda = true
      this.precio_envio_general = 0;
      this.bloquearSelectDireccion = true;

    }

    else {
      this.recojo_tienda = false
      this.bloquearSelectDireccion = false;


      if (this.selectedDireccionSecond == "") {

        this.precio_envio_general = 0;

      } else {

        var precio_envio = this.selectedDireccionSecond.split("-");
        this.precio_envio_general = precio_envio[1];

      }

    }



  }

  getTotalNeto() {

    return ((this.totalBruto + Number(this.precio_envio_general)) * (100 - this.totalDscto) / 100).toFixed(2);

  }



  finalizarCompra() {
    var cupon = { nro_cupon: this.cupon_descuento };

    if (this.totalBruto == 0) {
      Swal.fire("Oops...", "El precio total es invalido", "error");
    }
    else if (this.totalBruto <= 65 && !this.recojo_tienda) {
      Swal.fire("Oops...", "El costo minimo para envíos es de S/.65.00", "error");

      return false;

    } else if (this.select_direccion == "" && !this.recojo_tienda) {
      Swal.fire("Oops...", "Ud. Debe seleccionar una dirección", "error");

      return false;

    } else if (this.precio_envio_general == "0" && !this.recojo_tienda) {
      Swal.fire("Oops...", "Todavía no existe cobertura hacia esta dirección , por favor seleccione otra dirección", "error");

      return false;


    } else if (!this.confirmarTerminos) {

      Swal.fire({
        title: "<img src='assets/img/tiendas/iconterminoslaert.png' style='height:100px;margin-bottom:20px' />",
        html: "Por favor, acepte los términos y políticas de privacidad ",
        confirmButtonText: 'DE ACUERDO',
      });

      return false;
    } else {



      if (this.recojo_tienda) {
        this.select_direccion = 0
      } else if (Number(this.select_direccion) == this.id_direccion) {
        //haciendo elmatch del id de direccion
        this.select_direccion = this.id_direccion;
        //  this.precio_envio_general = this.precio_envio;



      } else if (Number(this.select_direccion) == 0) {

        if (this.selectedDireccionSecond == "") {

          Swal.fire("Oops...", "Por favor seleccione una dirección de la lista", "error");

          return false;
        } else {
          //haciendo elmatch del id de direccion

          var precio_envio_select = this.selectedDireccionSecond.split("-");
          this.select_direccion = Number(precio_envio_select[0]);

        }
      }

      this.cuponesService.actualizarCupon(cupon).subscribe(response1 => {



      },
        error => {
          console.log(error);
        })

      var totalAux = this.getTotalNeto();

      var boleta = {
        cliente: this.idpersona["id_cli_nat"], direccion: String(this.select_direccion), subtotal: String(this.totalBruto), precio_envio: String(this.precio_envio_general),
        descuento: String(this.totalDscto), total: String(totalAux), recojo_berries: this.recojo_tienda ? "SI" : "NO", detalle: this.getProductDetail(),
        productos: this.shoppingCartItems
      }





      var auxMontoFinal = parseFloat(totalAux + "") * 100;

      localStorage.setItem("montoFinal", auxMontoFinal + "");

      localStorage.setItem("boleta", JSON.stringify(boleta))

      //  localStorage.setItem("boleta" , JSON.stringify(boleta))
      this.router.navigate(['/pasarela-pago']);



    }

  }




  openDropdown() {

    var aux = $('.lista-cart');

    aux.attr('tabindex', 1).focus();
    aux.toggleClass('active');
    aux.find('.lista-cart-menu').slideToggle(300);



  }

  /*
  closeDropdown(){
   
   var aux = $('.lista-cart');
   
   aux.removeClass('active');
   aux.find('.dropdown-menu').slideUp(300);
   
  }
  */








  listaDireccion() {

    let cliente = { cliente: this.idpersona["id_cli_nat"] };

    this.direccionesService.getDireccionByCliente(cliente).subscribe(response => {

      this.isNoDireccion = true;



      for (var i = 0; i < response.length; i++) {

        if (response[i]["direccion_principal"] == 1) {
          this.direccion_principal = response[i]["direccion"] + " " + response[i]["numero"] + " " + response[i]["distrito"];
          this.id_direccion = response[i]["id_direccion"];
          this.precio_envio = response[i]["precio_envio"];

        } else if (response[i]["direccion_principal"] == 2) {
          this.listaDirecciones.push({
            id_direccion: response[i]["id_direccion"],
            provincia: response[i]["provincia"],
            distrito: response[i]["distrito"],
            precio_envio: response[i]["precio_envio"],
            direccion: response[i]["direccion"],
            opcion_direccion: response[i]["opcion_direccion"],
            numero: response[i]["numero"],
            referencia_1: response[i]["referencia_1"],
            referencia_2: response[i]["referencia_2"],
            direccion_principal: response[i]["direccion_principal"],
            latitud: "",
            longitud: "",
            mensaje: "envio a domicilio",
          })



        }



      }

      this.listaDirecciones = this.listaDirecciones.filter((valorActual, indiceActual, arreglo) => {
        return arreglo.findIndex(
          valorDelArreglo => JSON.stringify(valorDelArreglo) === JSON.stringify(valorActual)
        ) === indiceActual
      });




    },
      error => {
        console.log(error);
        this.isNoDireccion = false;

        if (this.isNoDireccion == false) {
          // $("#msjNoDireccion").html("<span>Ud. debe agregar una dirección antes de proseguir con su compra , lo puede hacer en <a routerLi>Mi Cuenta</a></span>")
        }
      })

  }




  getProductsComplemento() {

    //tipo consulta 1 -> productos por Categoria , idparametro categoria que deseo
    //tipo consulta 2 -> solo un producto , id parametro id producto que deseo ( solo para uno)

    let image = { tipo_consulta: "2" };

    this.productoService.getAllProducts().subscribe(response => {

      let shuffled = response.sort(() => 0.5 - Math.random());

      // Trae un sub-array de n elementos despues de ordenarlos.

      this.arrayProducts = shuffled.slice(0, 4);

      for (var i = 0; i < this.arrayProducts.length; i++) {

        this.arrayProducts[i].cantidad = 1;

      }

      this.productoService.getImageProduct(image).subscribe(response1 => {

        this.arrayImagesProducts = response1["productos"];

        for (var i = 0; i < this.arrayProducts.length; i++) {

          for (var j = 0; j < this.arrayImagesProducts.length; j++) {

            if (this.arrayProducts[i]["id_pro"] === Number(this.arrayImagesProducts[j]["id_producto"])) {

              this.tercerArray.push({
                id_pro: this.arrayProducts[i]["id_pro"],
                linea_de_negocio: this.arrayProducts[i]["linea_de_negocio"],
                presentacion: this.arrayProducts[i]["presentacion"],
                nombre_producto: this.arrayProducts[i]["nombre_producto"],
                precio_con_igv: this.arrayProducts[i]["precio_con_igv"],
                stock_actual: this.arrayProducts[i]["stock_actual"],
                stock_reservado: this.arrayProducts[i]["stock_reservado"],
                stock_disponible: this.arrayProducts[i]["stock_disponible"],
                cantidad: this.arrayProducts[i]["cantidad"],
                porcentaje: "0",
                nuevo_precio: this.arrayProducts[i]["precio_con_igv"],
                imagen: this.arrayImagesProducts[j]["imagen_principal"]
              })
            }


          }
        }


      })

    },
      error => {
        console.log(error);
      });

  }


}
