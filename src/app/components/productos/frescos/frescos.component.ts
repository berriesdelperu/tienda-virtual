import { Component, OnInit, AfterViewInit } from "@angular/core";
import { ProductoService } from "../../../services/producto.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-frescos",
  templateUrl: "./frescos.component.html",
  styleUrls: ["./frescos.component.css"],
})
export class FrescosComponent implements OnInit {
  public listProductsByCategory: Object[];

  constructor(private detailProduct: ProductoService, public router: Router) {}

  ngOnInit() {
    window.scrollTo(0, 0);
    $("#content-tienda").removeClass("header-tienda");
    $(".navbar .nav-item .nav-link").removeClass("color-nav");
    $(".navbar .nav-item .dropdown-toggle").removeClass("border-dropdown-gray");
    $(".navbar .nav-item .rounded-market").removeClass("border-button-nav");
    $("#img-market").attr("src", "assets/img/icons/icon-market.svg");
    $("#img-cart").attr("src", "assets/img/icons/icon-cart.svg");
    $("#img-user").attr("src", "assets/img/icons/icon-user.svg");

    this.getProductByDetail();
  }

  getProductByDetail() {
    let id_categoria = { linea: "3" };

    this.detailProduct
      .getProductsByCategoria(id_categoria)
      .subscribe((response) => {
        this.listProductsByCategory = response;
      });
  }

  goBlog() {
    this.router.navigate(["/blog"]);
  }
}
