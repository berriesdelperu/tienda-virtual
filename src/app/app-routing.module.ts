import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InicioComponent } from './components/inicio/inicio.component';
import { NopagefoundComponent } from './components/nopagefound/nopagefound.component';



const routes: Routes = [
  {
    path: 'productos',
    loadChildren: () => import('./components/productos/productos.module').then(m => m.ProductosModule)
  },
  {
    path: 'quienes-somos',
    loadChildren: () => import('./components/quienes-somos/quienes-somos.module').then(m => m.QuienesSomosModule)
  },
  {
    path: 'contacto',
    loadChildren: () => import('./components/contacto/contacto.module').then(m => m.ContactoModule)
  },
  {
    path: 'blog',
    loadChildren: () => import('./components/blog/blog.module').then(m => m.BlogModule)
  },
  {
    path: 'tienda',
    loadChildren: () => import('./components/tienda/tienda.module').then(m => m.TiendaModule)
  },
  {
    path: 'carrito-compras',
    loadChildren: () => import('./components/carrito-compras/carrito-compras.module').then(m => m.CarritoComprasModule)
  },
  {
    path: 'mi-cuenta',
    loadChildren: () => import('./components/mi-cuenta/mi-cuenta.module').then(m => m.MiCuentaModule)
  },
  {
    path: 'pasarela-pago',
    loadChildren: () => import('./components/pasarela-pago/pasarela-pago.module').then(m => m.PasarelaPagoModule)
  },
  {
    path: 'terminos-y-condiciones',
    loadChildren: () => import('./components/terminos/terminos.module').then(m => m.TerminosModule)
  },
  {
    path: 'politicas-de-privacidad',
    loadChildren: () => import('./components/politicas/politicas.module').then(m => m.PoliticasModule)
  },
  {
    path: 'refresh',
    loadChildren: () => import('./components/refresh/refresh.module').then(m => m.RefreshModule)
  },
  {
    path: '',
    component:InicioComponent
  },
  {
    path: 'no-encontrado',
    component: NopagefoundComponent
  },
  {
    path: '**',
    redirectTo: 'no-encontrado'
  }



];



@NgModule({
  imports: [
    RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }