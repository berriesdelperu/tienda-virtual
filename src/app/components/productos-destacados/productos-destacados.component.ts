import { Component, OnInit } from '@angular/core';
import {ProductoService} from '../../services/producto.service';
import { UtilService} from '../../utils/util.service';

@Component({
  selector: 'app-productos-destacados',
  templateUrl: './productos-destacados.component.html',
  styleUrls: ['./productos-destacados.component.css']
})
export class ProductosDestacadosComponent implements OnInit {

  public arrayProducts: Array<any> = [];
  public arrayImagesProducts: Array<any> = [];
  public tercerArray: Array<any> = [];


  constructor(private productoService: ProductoService , private utilService: UtilService ) { }
    
   

  ngOnInit() {
    this.getProducts();
  }

  getProducts(){
    
    let image = {tipo_consulta: "2"};
    
    this.productoService.getProductosDestacados().subscribe(response =>{


      this.arrayProducts = response;


      
        this.productoService.getImageProduct(image).subscribe(response1 =>{

          var arrayIdItemsWImages = []; 
          var arrayIdItemsSystem = []; 
 
          this.arrayImagesProducts = response1["productos"];

          for(var j=0; j<this.arrayImagesProducts.length;j++){
            arrayIdItemsWImages.push(Number(this.arrayImagesProducts[j]["id_producto"]));
          }
  
          for(var k=0; k<this.arrayProducts.length;k++){
            arrayIdItemsSystem.push(Number(this.arrayProducts[k]["id_pro"]));
          }

          for(var i=0; i<this.arrayProducts.length;i++){
            if(arrayIdItemsWImages.includes(this.arrayProducts[i]["id_pro"])){ 
  
  
              this.tercerArray.push({
                   id_pro: this.arrayProducts[i]["id_pro"],
                   linea_de_negocio : this.arrayProducts[i]["linea_de_negocio"],
                   presentacion: this.arrayProducts[i]["presentacion"],
                   nombre_producto: this.arrayProducts[i]["nombre_producto"],
                   precio_con_igv: this.arrayProducts[i]["precio_con_igv"],
                   stock_actual: this.arrayProducts[i]["stock_actual"],
                   stock_reservado: this.arrayProducts[i]["stock_reservado"],
                   stock_disponible: this.arrayProducts[i]["stock_disponible"],
                   cantidad: 1,
                   porcentaje: "0",
                   nuevo_precio:  this.arrayProducts[i]["precio_con_igv"],
                   imagen: this.arrayImagesProducts[arrayIdItemsWImages.indexOf(this.arrayProducts[i]["id_pro"])]["imagen_principal"]
              });
                 
             }else{
  
              this.tercerArray.push({
                  id_pro: this.arrayProducts[i]["id_pro"],
                  linea_de_negocio : this.arrayProducts[i]["linea_de_negocio"],
                  presentacion: this.arrayProducts[i]["presentacion"],
                  nombre_producto: this.arrayProducts[i]["nombre_producto"],
                  precio_con_igv: this.arrayProducts[i]["precio_con_igv"],
                  stock_actual: this.arrayProducts[i]["stock_actual"],
                  stock_reservado: this.arrayProducts[i]["stock_reservado"],
                  stock_disponible: this.arrayProducts[i]["stock_disponible"],
                  cantidad: 1,
                  porcentaje: "0",
                  nuevo_precio:  this.arrayProducts[i]["precio_con_igv"],
                  imagen: "assets/img/icons/image-not-found.png"
            });
  
  
             }
    
          }

        
          


        })
     
    }, 
    error =>{
      console.log(error);
    });

    


  }

}
