import { Component, OnInit, ViewChild, AfterViewInit } from "@angular/core";
import { ProductoService } from "../../../services/producto.service";
import { Router } from "@angular/router";
import { UtilService } from "../../../utils/util.service";
import { ComplementosComponent } from "../../complementos/complementos.component";

@Component({
  selector: "app-deshidratados",
  templateUrl: "./deshidratados.component.html",
  styleUrls: ["./deshidratados.component.css"],
})
export class DeshidratadosComponent implements OnInit {
  public arrayProducts: Array<any> = [];
  public listDeshidratados: Array<any> = [];
  public slidesDeshidratados: any = [[]];
  public arrayImagesProducts: Array<any> = [];
  public auxArray: Array<any> = [];
  @ViewChild(ComplementosComponent, { static: true })
  complemento: ComplementosComponent;

  constructor(
    private productoService: ProductoService,
    public router: Router,
    private utilService: UtilService
  ) {}

  ngOnInit() {
    window.scrollTo(0, 0);
    $("#content-tienda").removeClass("header-tienda");
    $(".navbar .nav-item .nav-link").removeClass("color-nav");
    $(".navbar .nav-item .dropdown-toggle").removeClass("border-dropdown-gray");
    $(".navbar .nav-item .rounded-market").removeClass("border-button-nav");
    $("#img-market").attr("src", "assets/img/icons/icon-market.svg");
    $("#img-cart").attr("src", "assets/img/icons/icon-cart.svg");
    $("#img-user").attr("src", "assets/img/icons/icon-user.svg");

    this.getDeshidratados();
  }

  getDeshidratados() {
    let id_categoria = { linea: "2" };
    let image = { tipo_consulta: "2" };

    this.productoService
      .getProductsByCategoria(id_categoria)
      .subscribe((response) => {
        this.arrayProducts = response;

        this.productoService.getImageProduct(image).subscribe((response1) => {
          this.arrayImagesProducts = response1["productos"];

          for (var i = 0; i < this.arrayProducts.length; i++) {
            for (var j = 0; j < this.arrayImagesProducts.length; j++) {
              if (
                this.arrayProducts[i]["id_pro"] ===
                Number(this.arrayImagesProducts[j]["id_producto"])
              ) {
                this.listDeshidratados.push({
                  id_pro: this.arrayProducts[i]["id_pro"],
                  linea_de_negocio: this.arrayProducts[i]["linea_de_negocio"],
                  presentacion: this.arrayProducts[i]["presentacion"],
                  nombre_producto: this.arrayProducts[i]["nombre_producto"],
                  precio_con_igv: this.arrayProducts[i]["precio_con_igv"],
                  stock_actual: this.arrayProducts[i]["stock_actual"],
                  stock_reservado: this.arrayProducts[i]["stock_reservado"],
                  stock_disponible: this.arrayProducts[i]["stock_disponible"],
                  cantidad: 1,
                  porcentaje: "0",
                  nuevo_precio: this.arrayProducts[i]["precio_con_igv"],
                  imagen: this.arrayImagesProducts[j]["imagen_principal"],
                });
                break;
              }
            }
          }

          this.slidesDeshidratados = this.utilService.chunk(
            this.listDeshidratados,
            4
          );
        });
      });
  }

  goBlog() {
    this.router.navigate(["/blog"]);
  }
}
