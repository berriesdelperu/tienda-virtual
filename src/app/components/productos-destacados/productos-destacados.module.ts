import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductosDestacadosComponent} from './productos-destacados.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [ProductosDestacadosComponent],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [ProductosDestacadosComponent]
})
export class ProductosDestacadosModule { }
