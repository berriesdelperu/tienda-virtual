import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ProductoService } from 'src/app/services/producto.service';
import { UtilService } from 'src/app/utils/util.service';

@Component({
  selector: 'app-buscar-producto',
  templateUrl: './buscar-producto.component.html',
  styleUrls: ['./buscar-producto.component.css']
})
export class BuscarProductoComponent implements OnInit {

  stateForm: FormGroup;

  showDropDown = false;

  keyword = 'nombre_producto';

  public arrayProducts: Array<any> = [];
  public arrayTotalProductos: Array<any> = [];
  public arrayImagesProducts: Array<any> = [];
  public listaSearchProducts: Object[];


  public isResultadoOK: Boolean;


  constructor(private fb: FormBuilder, private productoService: ProductoService, private utilService: UtilService) {

    this.initForm()

    this.isResultadoOK = false;
  }

  ngOnInit() {




    window.scrollTo(0, 0);


    $(".imgIconMovilmenuFinal").attr(
      "src",
      "assets/img/icons/icon-menu-2.svg"
    );
    $(".imgIconMoviltiendaFinal").attr(
      "src",
      "assets/img/icons/icon-tienda-2.svg"
    );
    $(".imgIconMovilCartFinal").attr(
      "src",
      "assets/img/icons/icon-cart-2.svg"
    );
    $(".imgIconMovilUserFinal").attr(
      "src",
      "assets/img/icons/icon-user-2.svg"
    );


    $(".content-tienda").removeClass('header-tienda');
    $('.navbar .nav-item .nav-link').css('color', "#778489");
    $('.navbar .nav-item .dropdown-toggle').addClass('border-dropdown-gray');
    $('.navbar .nav-item .rounded-market').addClass('border-button-nav');;
    $("#img-market").attr("src", "assets/img/icons/icon-market-gray.svg");
    $("#img-cart").attr("src", "assets/img/icons/icon-cart-gray.svg");
    $("#img-user").attr("src", "assets/img/icons/icon-user-gray.svg");
    document.getElementById("scrolling-navbar").style.background = "white"

    this.listSearch();
    this.obtenerAllProducts();
  }



  initForm(): FormGroup {
    return this.stateForm = this.fb.group({
      search: [null]
    })
  }


  listSearch() {

    this.productoService.getAllProducts().subscribe(response => {

      var auxArray = [];

      for (var i = 0; i < response.length; i++) {
        auxArray.push({
          nombre_producto: response[i]["nombre_producto"]
        })
      }

      this.listaSearchProducts = auxArray.filter((valorActual, indiceActual, arreglo) => {
        return arreglo.findIndex(
          valorDelArreglo => JSON.stringify(valorDelArreglo) === JSON.stringify(valorActual)
        ) === indiceActual
      });



    })

  }


  selectEvent(item) {
    this.isResultadoOK = true;

    this.stateForm.patchValue({ "search": item.nombre_producto });
  }

  obtenerAllProducts() {


    let image = { tipo_consulta: "2" };

    this.productoService.getAllProducts().subscribe(response => {

      this.arrayProducts = response;

      this.productoService.getImageProduct(image).subscribe(response1 => {

        var arrayIdItemsWImages = [];
        var arrayIdItemsSystem = [];

        this.arrayImagesProducts = response1["productos"];

        for (var j = 0; j < this.arrayImagesProducts.length; j++) {
          arrayIdItemsWImages.push(Number(this.arrayImagesProducts[j]["id_producto"]));
        }

        for (var k = 0; k < this.arrayProducts.length; k++) {
          arrayIdItemsSystem.push(Number(this.arrayProducts[k]["id_pro"]));
        }

        for (var i = 0; i < this.arrayProducts.length; i++) {
          if (arrayIdItemsWImages.includes(this.arrayProducts[i]["id_pro"])) {


            this.arrayTotalProductos.push({
              id_pro: this.arrayProducts[i]["id_pro"],
              linea_de_negocio: this.arrayProducts[i]["linea_de_negocio"],
              presentacion: this.arrayProducts[i]["presentacion"],
              nombre_producto: this.arrayProducts[i]["nombre_producto"],
              precio_con_igv: this.arrayProducts[i]["precio_con_igv"],
              stock_actual: this.arrayProducts[i]["stock_actual"],
              stock_reservado: this.arrayProducts[i]["stock_reservado"],
              stock_disponible: this.arrayProducts[i]["stock_disponible"],
              cantidad: 1,
              porcentaje: "0",
              nuevo_precio: this.arrayProducts[i]["precio_con_igv"],
              imagen: this.arrayImagesProducts[arrayIdItemsWImages.indexOf(this.arrayProducts[i]["id_pro"])]["imagen_principal"]
            });

          } else {

            this.arrayTotalProductos.push({
              id_pro: this.arrayProducts[i]["id_pro"],
              linea_de_negocio: this.arrayProducts[i]["linea_de_negocio"],
              presentacion: this.arrayProducts[i]["presentacion"],
              nombre_producto: this.arrayProducts[i]["nombre_producto"],
              precio_con_igv: this.arrayProducts[i]["precio_con_igv"],
              stock_actual: this.arrayProducts[i]["stock_actual"],
              stock_reservado: this.arrayProducts[i]["stock_reservado"],
              stock_disponible: this.arrayProducts[i]["stock_disponible"],
              cantidad: 1,
              porcentaje: "0",
              nuevo_precio: this.arrayProducts[i]["precio_con_igv"],
              imagen: "assets/img/icons/image-not-found.png"
            });


          }

        }




      })


    })



  }

  getSearchValue() {

    return this.stateForm.value.search;

  }











}
