import { Component, OnInit, ViewChild, AfterViewInit } from "@angular/core";
import { ContactoService } from "../../../services/contacto.service";
import { Router } from "@angular/router";
import { Location } from "@angular/common";
import Swal from "sweetalert2";
import { ComplementosComponent } from "../../complementos/complementos.component";
import { ProductosDestacadosComponent } from "../../productos-destacados/productos-destacados.component";
import { UtilService } from "src/app/utils/util.service";
@Component({
  selector: "app-formulario",
  templateUrl: "./formulario.component.html",
  styleUrls: ["./formulario.component.css"],
})
export class FormularioComponent implements OnInit {
  public selectedPersona: string;
  public nombres: string;
  public apellidos: string;
  public telefono: string;
  public correo: string;
  public selectedMotivo: string;
  public mensaje: string;
  public razon_social: string;
  public nombre_comercial: string;
  public ruc: string;
  public nombre_contacto: string;
  public apellido_contacto: string;
  public isPersona: Boolean;
  public isEmpresa: Boolean;
  @ViewChild(ProductosDestacadosComponent, { static: true })
  complemento: ProductosDestacadosComponent;

  constructor(
    private serviceContact: ContactoService,
    private router: Router,
    public location: Location,
    public utilService: UtilService
  ) {
    this.isPersona = true;
    this.selectedPersona = "";
    this.nombres = "";
    this.apellidos = "";
    this.telefono = "";
    this.correo = "";
    this.selectedMotivo = "";
    this.mensaje = "";
    this.razon_social = "";
    this.nombre_comercial = "";
    this.ruc = "";
    this.nombre_contacto = "";
    this.apellido_contacto = "";
  }

  ngOnDestroy(): void {}

  // ngOnInit() {}

  ngOnInit() {
    window.scrollTo(0, 0);
    $(".imgIconMovilmenuFinal").attr("src", "assets/img/icons/icon-menu.svg");
    $(".imgIconMoviltiendaFinal").attr(
      "src",
      "assets/img/icons/icon-tienda.svg"
    );
    $(".imgIconMovilCartFinal").attr("src", "assets/img/icons/icon-cart.svg");
    $(".imgIconMovilUserFinal").attr("src", "assets/img/icons/icon-user.svg");
    $(".navigationFixed").removeClass("fondito-berries");
    $(".content-tienda").removeClass("header-tienda");
    $(".navbar .nav-item .nav-link").css("color", "#ffffff");
    $(".navbar .nav-item .dropdown-toggle").removeClass("border-dropdown-gray");
    $(".navbar .nav-item .rounded-market").removeClass("border-button-nav");
    $("#img-market").attr("src", "assets/img/icons/icon-market.svg");
    $("#img-cart").attr("src", "assets/img/icons/icon-cart.svg");
    $("#img-user").attr("src", "assets/img/icons/icon-user.svg");
    document.getElementById("scrolling-navbar").style.background =
      "transparent";
    $(".carousel").on("touchstart", function (event) {
      var xClick = event.originalEvent.touches[0].pageX;
      $(this).one("touchmove", function (event) {
        var xMove = event.originalEvent.touches[0].pageX;
        if (Math.floor(xClick - xMove) > 5) {
          ($(this) as any).carousel("next");
        } else if (Math.floor(xClick - xMove) < -5) {
          ($(this) as any).carousel("prev");
        }
      });
      $(".carousel").on("touchend", function () {
        $(this).off("touchmove");
      });
    });
  }

  changePersona($event) {
    this.selectedPersona = $event.target.value;

    if (this.selectedPersona == "1") {
      this.isEmpresa = false;
      this.isPersona = true;
    } else {
      this.isEmpresa = true;
      this.isPersona = false;
    }
  }

  changeMotivo($event) {
    this.selectedMotivo = $event.target.value;
  }

  enviarFormularioContacto() {
    if (this.selectedPersona == "") {
      Swal.fire({
        title:
          "<img src='assets/img/tiendas/alertempresaonatural.png' style='height:100px;margin-bottom:20px' />",
        html: "Seleccione si es persona natural o empresa",
        confirmButtonText: "DE ACUERDO",
      });

      return false;
    } else {
      if (this.selectedPersona == "1") {
        if (this.nombres == "") {
          Swal.fire("Oops...", "Ingrese su(s) nombre(s)", "error");
          return false;
        } else if (this.apellidos == "") {
          Swal.fire("Oops...", "Ingrese su(s) apellido(s)", "error");
          return false;
        } else if (this.telefono == "" || this.telefono.length < 9) {
          Swal.fire(
            "Oops...",
            "Ingrese su teléfono válido de 9 dígitos",
            "error"
          );

          return false;
        } else if (!this.utilService.validateEmail(this.correo)) {
          Swal.fire("Oops...", "Ingrese un correo válido", "error");

          return false;
        } else if (this.selectedMotivo == "") {
          Swal.fire("Oops...", "Seleccione su motivo", "error");

          return false;
        } else if (this.mensaje == "") {
          Swal.fire("Oops...", "Ingrese su mensaje", "error");

          return false;
        } else {
          var data_persona_natural = {
            tipo_contacto: this.selectedPersona,
            nombre_contacto: this.nombres,
            apellido_contacto: this.apellidos,
            telefono: this.telefono,
            correo: this.correo,
            motivo: this.selectedMotivo,
            mensaje: this.mensaje,
          };

          this.serviceContact
            .registerFormContact(data_persona_natural)
            .subscribe(
              (response) => {
                if (response["success"] == true) {
                  Swal.fire({
                    type: "success",
                    title: "Formulario Registrado!",
                    text: "Sus datos fueron registrados!",
                  }).then(() => {
                    this.router
                      .navigateByUrl("/refresh", { skipLocationChange: true })
                      .then(() => {
                        this.router.navigate([decodeURI(this.location.path())]);
                      });
                  });
                } else {
                }
              },
              (error) => {
                console.log(error);
              }
            );
        }
      } else {
        if (this.razon_social == "") {
          Swal.fire("Oops...", "Ingrese su razón social", "error");
          return false;
        } else if (this.nombre_comercial == "") {
          Swal.fire("Oops...", "Ingrese su nombre comercial", "error");
          return false;
        } else if (this.ruc == "") {
          Swal.fire("Oops...", "Ingrese su teléfono", "error");

          return false;
        } else if (this.nombre_contacto == "") {
          Swal.fire({
            title:
              "<img src='assets/img/tiendas/iconocorreoalert.png' style='height:100px;margin-bottom:20px' />",
            html: "Por favor, ingrese su correo",
            confirmButtonText: "DE ACUERDO",
          });

          return false;
        } else if (this.apellido_contacto == "") {
          Swal.fire("Oops...", "Ingrese su apellido", "error");

          return false;
        } else if (this.telefono == "") {
          Swal.fire("Oops...", "Ingrese su telefono", "error");

          return false;
        } else if (this.correo == "") {
          Swal.fire("Oops...", "Ingrese su correo", "error");

          return false;
        } else if (this.selectedMotivo == "") {
          Swal.fire("Oops...", "Seleccione su motivo", "error");

          return false;
        } else if (this.mensaje == "") {
          Swal.fire("Oops...", "Ingrese su mensaje", "error");

          return false;
        } else {
          var data_empresa = {
            tipo_contacto: this.selectedPersona,
            razon_social: this.razon_social,
            nombre_comercial: this.nombre_comercial,
            ruc: this.ruc,
            nombre_contacto: this.nombre_contacto,
            apellido_contacto: this.apellido_contacto,
            telefono: this.telefono,
            correo: this.correo,
            motivo: this.selectedMotivo,
            mensaje: this.mensaje,
          };

          this.serviceContact.registerFormContact(data_empresa).subscribe(
            (response) => {
              if (response["success"] == true) {
                Swal.fire({
                  type: "success",
                  title: "Formulario Registrado!",
                  text: "Sus datos fueron registrados!",
                }).then(() => {
                  this.router
                    .navigateByUrl("/refresh", { skipLocationChange: true })
                    .then(() => {
                      this.router.navigate([decodeURI(this.location.path())]);
                    });
                });
              } else {
              }
            },
            (error) => {
              console.log(error);
            }
          );
        }
      }
    }
  }
}
