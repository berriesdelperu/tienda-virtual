import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BlogComponent } from './blog.component';
import { DetalleArticuloComponent} from './detalle-articulo/detalle-articulo.component';



const routes: Routes = [

  
  {
    path: '',
    component: BlogComponent,
    pathMatch: 'full' 
  },
  {
    path: ':id/:slug',
    component: DetalleArticuloComponent
  }
];





@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlogRoutingModule { }
