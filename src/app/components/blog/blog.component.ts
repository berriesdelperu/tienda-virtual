import {
  Component,
  OnInit,
  ChangeDetectorRef,
  AfterViewInit,
} from "@angular/core";
import { BlogService } from "../../services/blog.service";
import { Subscription } from "rxjs";

declare var $: any;

@Component({
  selector: "app-blog",
  templateUrl: "./blog.component.html",
  styleUrls: ["./blog.component.css"],
})
export class BlogComponent implements OnInit {
  public arrayArticles: Object[];
  public arrayCategories: Object[];
  public searchName: string;
  public p: any;

  public sub_articles: Subscription;
  public sub_categories: Subscription;

  constructor(private blogService: BlogService, public ref: ChangeDetectorRef) {
    this.arrayArticles = [];
  }

  ngOnInit() {
    $(".imgIconMovilmenuFinal").attr("src", "assets/img/icons/icon-menu-2.svg");
    $(".imgIconMoviltiendaFinal").attr(
      "src",
      "assets/img/icons/icon-tienda-2.svg"
    );
    $(".imgIconMovilCartFinal").attr("src", "assets/img/icons/icon-cart-2.svg");
    $(".imgIconMovilUserFinal").attr("src", "assets/img/icons/icon-user-2.svg");

    window.scrollTo(0, 0);

    $(".content-tienda").addClass("header-tienda");
    $(".navbar .nav-item .nav-link").css("color", "#778489");
    $(".navbar .nav-item .dropdown-toggle").addClass("border-dropdown-gray");
    $(".navbar .nav-item .rounded-market").addClass("border-button-nav");
    $("#img-market").attr("src", "assets/img/icons/icon-market-gray.svg");
    $("#img-cart").attr("src", "assets/img/icons/icon-cart-gray.svg");
    $("#img-user").attr("src", "assets/img/icons/icon-user-gray.svg");

    $(window).resize(function () {
      if ($(window).width() < 500) {
        $(".content-tienda").addClass("header-tienda");
        $(".navbar .nav-item .nav-link").addClass("color-nav");
        $(".navbar .nav-item .dropdown-toggle").addClass(
          "border-dropdown-gray"
        );
        $(".navbar .nav-item .rounded-market").addClass("border-button-nav");
        $("#img-market").attr("src", "assets/img/icons/icon-market-gray.svg");
        $("#icon-cart").attr("src", "assets/img/icons/icon-cart-2.svg");
        $("#img-user").attr("src", "assets/img/icons/icon-user-2.svg");
      }
    });

    this.getCategories();
    this.getArticles();
  }

  filtro($event, id) {
    var list = $(".contenedor-blog");
    $event.stopPropagation();

    var relid = id,
      that = $event.path[0];

    if (that.classList.contains("active-blog")) {
      that.classList.remove("active-blog");
      list
        .filter("[data-blog='" + relid + "']")
        .removeClass("normal")
        .hide("fast");
      $($event.target).css("color", "#cad2d5");
      $($event.target).css("background", "#ffff");
      $($event.target).css("border", "solid 1px #cad2d5");
    } else {
      list
        .filter(":not(.normal)[data-blog!='" + relid + "']")
        .removeClass("normal")
        .hide("fast");
      list
        .filter("[data-blog='" + relid + "']")
        .addClass("normal")
        .show("fast");
      that.classList.add("active-blog");
      $($event.target).css("color", "#fff");
      $($event.target).css("background", "#cad2d5");
      $($event.target).css("border", "solid 1px #cad2d5");
    }
    // If nothing selected show everything
    if (!list.filter(".normal").length) {
      list.show("fast");
    }
  }

  okFiltros() {
    $("#img-mobile-active").attr(
      "src",
      "assets/img/icons/icon-filtro-active.PNG"
    );
    $("#btn-filtro-1").addClass("btn-filtro-mobile-active");
  }

  limpiarFiltro() {
    var list = $(".contenedor-blog");
    list.removeClass("normal");
    list.show("fast");
    $("#btn-filtro-1").removeClass("btn-filtro-mobile-active");
    $(".btn-blog").removeClass("active-blog");

    $("#img-mobile-active").attr(
      "src",
      "assets/img/icons/icon-filtro-mobile.PNG"
    );
    $(".btn-blog").attr("style", "");
  }

  getCategories() {
    this.sub_categories = this.blogService.getCategories().subscribe(
      (response) => {
        this.arrayCategories = response;
        console.log(response);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  getArticles() {
    this.sub_articles = this.blogService.getAllArticles().subscribe(
      (response) => {
        this.arrayArticles = response;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  ngOnDestroy() {
    this.sub_articles.unsubscribe();
    this.sub_categories.unsubscribe();
  }
}
