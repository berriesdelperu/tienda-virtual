export class Producto{
    public id_pro:number;
    public estado: string;
    public id_lin_neg: number;
    public linea_de_negocio:string;
    public presentacion:string;
    public nombre_producto:string;
    public stock_actual:number;
    public stock_disponible:number;
    public cantidad:number;
    
}