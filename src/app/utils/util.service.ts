import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';
import {Producto} from '../models/producto';
import {CartService} from '../services/cart.service';
@Injectable({
  providedIn: 'root'
})
export class UtilService {

  public auxArray : Array <any> = [];


  constructor(private cartService: CartService) { }



  add(product){

    if(product.cantidad < product.stock_disponible){

      product.cantidad = product.cantidad + 1;

    }else{

  
      Swal.fire({
        title: "<img src='assets/img/tiendas/sinstock.png' style='height:100px;margin-bottom:20px' />",
        html: "El stock de este producto llego a su límite",
        confirmButtonText:  'DE ACUERDO',
      });
      return false;

    }


  }


  del(product){

    if(product.cantidad > 1 ){

      product.cantidad = product.cantidad - 1;

    }

   
  }


  public addToCart(product: Producto) {

    if(product.cantidad < product.stock_disponible){
      this.cartService.addToCart(product);

      Swal.fire({
        title: "<img src='assets/img/tiendas/minicartalert.png' style='height:100px;margin-bottom:20px' />",
        html: "Producto agregado al carrito",
        confirmButtonText:  'DE ACUERDO',
      });


    }else{

      Swal.fire({
        title: "<img src='assets/img/tiendas/sinstock.png' style='height:100px;margin-bottom:20px' />",
        html: "El stock de este producto llego a su límite",
        confirmButtonText:  'DE ACUERDO',
      });
    }

  }
  
  
  

  chunk(arr, chunkSize) {
    let R = [];

    for (let i = 0, len = arr.length; i < len; i += chunkSize) {
      R.push(arr.slice(i, i + chunkSize));
    }
    return R;
  }

  public validarSoloNumeros(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  
  }

  public validarCaracteresEspeciales(e) {
    var tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla == 8) {
      return true;
    }

    // Patron de entrada, en este caso solo acepta numeros y letras
    var patron = /[ .A-Za-z0-9]/;
    var tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
  }

  public validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  titleCase(str) {

    str = str.toLowerCase();
    str = str.split(' ');
    for (var i = 0; i < str.length; i++) {
      str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1); 
    }
    
    return str.join(' '); 
  }

  

  
}
