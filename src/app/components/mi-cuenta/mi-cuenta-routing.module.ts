import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MiCuentaComponent } from './mi-cuenta.component';
import { EditarCuentaComponent } from './editar-cuenta/editar-cuenta.component';


const routes: Routes = [
  {
    path: '',
    component: MiCuentaComponent
  },
  {
    path: 'editar-cuenta',
    component: EditarCuentaComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MiCuentaRoutingModule { }
