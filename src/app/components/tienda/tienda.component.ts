import { Component, OnInit, AfterViewInit, HostListener } from "@angular/core";
import { ProductoService } from "../../services/producto.service";
import { Router } from "@angular/router";
import { UtilService } from "src/app/utils/util.service";

declare var $: any;

@Component({
  selector: "app-tienda",
  templateUrl: "./tienda.component.html",
  styleUrls: ["./tienda.component.css"],
  preserveWhitespaces: false,
})
export class TiendaComponent implements OnInit {
  public arrayProductsCongelados: Array<any> = [];
  public listCongelados: Array<any> = [];
  public slidesCongelados: any = [[]];
  public arrayImagesProductsCongelados: Array<any> = [];

  public arrayProductsMermeladas: Array<any> = [];
  public listMermeladas: Array<any> = [];
  public slidesMermeladas: any = [[]];
  public arrayImagesProductsMermeladas: Array<any> = [];

  public arrayProductsMieles: Array<any> = [];
  public listMieles: Array<any> = [];
  public slidesMieles: any = [[]];
  public arrayImagesProductsMieles: Array<any> = [];

  public arrayProductsPulpas: Array<any> = [];
  public listPulpas: Array<any> = [];
  public slidesPulpas: any = [[]];
  public arrayImagesProductsPulpas: Array<any> = [];

  public arrayProductsDeshidratados: Array<any> = [];
  public listDeshidratados: Array<any> = [];
  public slidesDeshidratados: any = [[]];
  public arrayImagesProductsDeshidratados: Array<any> = [];

  public arrayProductsPromocion: Array<any> = [];
  public listPromocion: Array<any> = [];
  public slidesPromocion: any = [[]];
  public arrayImagesProductsPromocion: Array<any> = [];

  public countItems = 4;
  public innerWidth: any;

  constructor(
    private productoService: ProductoService,
    public router: Router,
    public utilService: UtilService
  ) {
    if (innerWidth < 992) {
      this.countItems = 2;
    }

    if (innerWidth < 769) {
      this.countItems = 1;
    }
  }

  @HostListener("window:resize", ["$event"])
  onResize(event) {
    this.innerWidth = window.innerWidth;
  }

  ngOnInit() {
    this.innerWidth = window.innerWidth;
    window.scrollTo(0, 0);

    $(".imgIconMovilmenuFinal").attr("src", "assets/img/icons/icon-menu-2.svg");
    $(".imgIconMoviltiendaFinal").attr(
      "src",
      "assets/img/icons/icon-tienda-2.svg"
    );
    $(".imgIconMovilCartFinal").attr("src", "assets/img/icons/icon-cart-2.svg");
    $(".imgIconMovilUserFinal").attr("src", "assets/img/icons/icon-user-2.svg");

    $(".content-tienda").removeClass("header-tienda");
    $(".navbar .nav-item .nav-link").css("color", "#778489");
    $(".navbar .nav-item .dropdown-toggle").addClass("border-dropdown-gray");
    $(".navbar .nav-item .rounded-market").addClass("border-button-nav");
    $("#img-market").attr("src", "assets/img/icons/icon-market-gray.svg");
    $("#img-cart").attr("src", "assets/img/icons/icon-cart-gray.svg");
    $("#img-user").attr("src", "assets/img/icons/icon-user-gray.svg");
    document.getElementById("scrolling-navbar").style.background = "white";

    this.getPromociones();
    this.getCongelados();
    this.getMermeladas();
    this.getMieles();
    this.getPulpas();

    var list = $("#contenedor-productos section");
    $(".myBtnContainer button").on("click", function (event) {
      event.preventDefault();

      var relid = this.id,
        that = $(this);

      if (that.is(".active-button")) {
        that.removeClass("active-button");
        list
          .filter("[data-category='" + relid + "']")
          .removeClass("normal")
          .hide("fast");
        that.css("color", "#cad2d5");
        that.css("background", "#ffff");
        that.css("border", "solid 1px #cad2d5");
      } else {
        list
          .filter(":not(.normal)[data-category!='" + relid + "']")
          .removeClass("normal")
          .hide("fast");
        list
          .filter("[data-category='" + relid + "']")
          .addClass("normal")
          .show("fast");
        that.addClass("active-button");
        that.css("color", "#fff");
        that.css("background", "#2c385f");
        that.css("border", "solid 1px #2c385f");
      }
      // If nothing selected show everything
      if (!list.filter(".normal").length) {
        list.show("fast");
      }
    });

    $(".carousel").on("touchstart", function (event) {
      var xClick = event.originalEvent.touches[0].pageX;
      $(this).one("touchmove", function (event) {
        var xMove = event.originalEvent.touches[0].pageX;
        if (Math.floor(xClick - xMove) > 5) {
          $(this).carousel("next");
        } else if (Math.floor(xClick - xMove) < -5) {
          $(this).carousel("prev");
        }
      });
      $(".carousel").on("touchend", function () {
        $(this).off("touchmove");
      });
    });
  }

  limpiarFiltro() {
    var list = $("#contenedor-productos section");
    list.removeClass("normal");
    list.show("fast");
    $("#btn-filtro-1").removeClass("btn-filtro-mobile-active");
    $(".btn-blog").removeClass("active-blog");

    $(".btn-cat").removeClass("active-button");

    $("#img-mobile-active").attr(
      "src",
      "assets/img/icons/icon-filtro-mobile.PNG"
    );

    $(".btn-cat").attr("style", "");
  }

  okFiltros() {
    $("#img-mobile-active").attr(
      "src",
      "assets/img/icons/icon-filtro-active.PNG"
    );
    $("#btn-filtro-1").addClass("btn-filtro-mobile-active");
  }

  getPromociones() {
    let image = { tipo_consulta: "2" };

    this.productoService.getAllProducts().subscribe((response) => {
      this.arrayProductsPromocion = response;

      this.productoService
        .getProductsPromocion(image)
        .subscribe((response1) => {
          this.arrayImagesProductsPromocion = response1["productos"];

          for (var i = 0; i < this.arrayProductsPromocion.length; i++) {
            for (var j = 0; j < this.arrayImagesProductsPromocion.length; j++) {
              if (
                this.arrayProductsPromocion[i]["id_pro"] ===
                Number(this.arrayImagesProductsPromocion[j]["id_producto"])
              ) {
                this.listPromocion.push({
                  id_pro: this.arrayProductsPromocion[i]["id_pro"],
                  linea_de_negocio: this.arrayProductsPromocion[i][
                    "linea_de_negocio"
                  ],
                  presentacion: this.arrayProductsPromocion[i]["presentacion"],
                  nombre_producto: this.arrayProductsPromocion[i][
                    "nombre_producto"
                  ],
                  precio_con_igv: this.arrayProductsPromocion[i][
                    "precio_con_igv"
                  ],
                  stock_actual: this.arrayProductsPromocion[i]["stock_actual"],
                  stock_reservado: this.arrayProductsPromocion[i][
                    "stock_reservado"
                  ],
                  stock_disponible: this.arrayProductsPromocion[i][
                    "stock_disponible"
                  ],
                  cantidad: 1,
                  porcentaje: this.arrayImagesProductsPromocion[j][
                    "monto_promo"
                  ],
                  nuevo_precio: (
                    this.arrayProductsPromocion[i]["precio_con_igv"] -
                    this.arrayProductsPromocion[i]["precio_con_igv"] *
                      (Number(
                        this.arrayImagesProductsPromocion[j]["monto_promo"]
                      ) /
                        100)
                  ).toFixed(2),
                  imagen: this.arrayImagesProductsPromocion[j][
                    "imagen_principal"
                  ],
                });
              }
            }
          }

          if (this.listPromocion.length < 5) {
            $("#rowRightSliderTiendaPromociones").hide();
            $("#rowRightSliderTiendaPromociones2").hide();
          }

          if (this.countItems == 1) {
            if (this.innerWidth < 575) {
              $("#rowRightSliderTiendaPromociones").show();
              $("#rowRightSliderTiendaPromociones2").show();
            }
          }

          this.slidesPromocion = this.utilService.chunk(
            this.listPromocion,
            this.countItems
          );
        });
    });
  }

  getCongelados() {
    let id_categoria = { linea: "1" };
    let image = { tipo_consulta: "2" };

    this.productoService
      .getProductsByCategoria(id_categoria)
      .subscribe((response) => {
        this.arrayProductsCongelados = response;

        this.productoService.getImageProduct(image).subscribe((response1) => {
          var arrayIdItemsWImages = [];
          var arrayIdItemsSystem = [];

          this.arrayImagesProductsCongelados = response1["productos"];

          for (var j = 0; j < this.arrayImagesProductsCongelados.length; j++) {
            arrayIdItemsWImages.push(
              Number(this.arrayImagesProductsCongelados[j]["id_producto"])
            );
          }

          for (var k = 0; k < this.arrayProductsCongelados.length; k++) {
            arrayIdItemsSystem.push(
              Number(this.arrayProductsCongelados[k]["id_pro"])
            );
          }

          for (var i = 0; i < this.arrayProductsCongelados.length; i++) {
            if (
              arrayIdItemsWImages.includes(
                this.arrayProductsCongelados[i]["id_pro"]
              )
            ) {
              this.listCongelados.push({
                id_pro: this.arrayProductsCongelados[i]["id_pro"],
                linea_de_negocio: this.arrayProductsCongelados[i][
                  "linea_de_negocio"
                ],
                presentacion: this.arrayProductsCongelados[i]["presentacion"],
                nombre_producto: this.arrayProductsCongelados[i][
                  "nombre_producto"
                ],
                precio_con_igv: this.arrayProductsCongelados[i][
                  "precio_con_igv"
                ],
                stock_actual: this.arrayProductsCongelados[i]["stock_actual"],
                stock_reservado: this.arrayProductsCongelados[i][
                  "stock_reservado"
                ],
                stock_disponible: this.arrayProductsCongelados[i][
                  "stock_disponible"
                ],
                cantidad: 1,
                porcentaje: "0",
                nuevo_precio: this.arrayProductsCongelados[i]["precio_con_igv"],
                imagen: this.arrayImagesProductsCongelados[
                  arrayIdItemsWImages.indexOf(
                    this.arrayProductsCongelados[i]["id_pro"]
                  )
                ]["imagen_principal"],
              });
            } else {
              this.listCongelados.push({
                id_pro: this.arrayProductsCongelados[i]["id_pro"],
                linea_de_negocio: this.arrayProductsCongelados[i][
                  "linea_de_negocio"
                ],
                presentacion: this.arrayProductsCongelados[i]["presentacion"],
                nombre_producto: this.arrayProductsCongelados[i][
                  "nombre_producto"
                ],
                precio_con_igv: this.arrayProductsCongelados[i][
                  "precio_con_igv"
                ],
                stock_actual: this.arrayProductsCongelados[i]["stock_actual"],
                stock_reservado: this.arrayProductsCongelados[i][
                  "stock_reservado"
                ],
                stock_disponible: this.arrayProductsCongelados[i][
                  "stock_disponible"
                ],
                cantidad: 1,
                porcentaje: "0",
                nuevo_precio: this.arrayProductsCongelados[i]["precio_con_igv"],
                imagen: "assets/img/icons/image-not-found.png",
              });
            }
          }

          if (this.listCongelados.length < 5) {
            $("#rowRightSliderTiendaCongelados").hide();
            $("#rowRightSliderTiendaCongelados2").hide();
          }

          this.slidesCongelados = this.utilService.chunk(
            this.listCongelados,
            this.countItems
          );
        });
      });
  }

  getMermeladas() {
    let id_categoria = { linea: "4" };
    let image = { tipo_consulta: "2" };

    this.productoService
      .getProductsByCategoria(id_categoria)
      .subscribe((response) => {
        this.arrayProductsMermeladas = response;

        this.productoService.getImageProduct(image).subscribe((response1) => {
          var arrayIdItemsWImages = [];
          var arrayIdItemsSystem = [];

          this.arrayImagesProductsMermeladas = response1["productos"];

          for (var j = 0; j < this.arrayImagesProductsMermeladas.length; j++) {
            arrayIdItemsWImages.push(
              Number(this.arrayImagesProductsMermeladas[j]["id_producto"])
            );
          }

          for (var k = 0; k < this.arrayProductsMermeladas.length; k++) {
            arrayIdItemsSystem.push(
              Number(this.arrayProductsMermeladas[k]["id_pro"])
            );
          }

          for (var i = 0; i < this.arrayProductsMermeladas.length; i++) {
            if (
              arrayIdItemsWImages.includes(
                this.arrayProductsMermeladas[i]["id_pro"]
              )
            ) {
              this.listMermeladas.push({
                id_pro: this.arrayProductsMermeladas[i]["id_pro"],
                linea_de_negocio: this.arrayProductsMermeladas[i][
                  "linea_de_negocio"
                ],
                presentacion: this.arrayProductsMermeladas[i]["presentacion"],
                nombre_producto: this.arrayProductsMermeladas[i][
                  "nombre_producto"
                ],
                precio_con_igv: this.arrayProductsMermeladas[i][
                  "precio_con_igv"
                ],
                stock_actual: this.arrayProductsMermeladas[i]["stock_actual"],
                stock_reservado: this.arrayProductsMermeladas[i][
                  "stock_reservado"
                ],
                stock_disponible: this.arrayProductsMermeladas[i][
                  "stock_disponible"
                ],
                cantidad: 1,
                porcentaje: "0",
                nuevo_precio: this.arrayProductsMermeladas[i]["precio_con_igv"],
                imagen: this.arrayImagesProductsMermeladas[
                  arrayIdItemsWImages.indexOf(
                    this.arrayProductsMermeladas[i]["id_pro"]
                  )
                ]["imagen_principal"],
              });
            } else {
              this.listMermeladas.push({
                id_pro: this.arrayProductsMermeladas[i]["id_pro"],
                linea_de_negocio: this.arrayProductsMermeladas[i][
                  "linea_de_negocio"
                ],
                presentacion: this.arrayProductsMermeladas[i]["presentacion"],
                nombre_producto: this.arrayProductsMermeladas[i][
                  "nombre_producto"
                ],
                precio_con_igv: this.arrayProductsMermeladas[i][
                  "precio_con_igv"
                ],
                stock_actual: this.arrayProductsMermeladas[i]["stock_actual"],
                stock_reservado: this.arrayProductsMermeladas[i][
                  "stock_reservado"
                ],
                stock_disponible: this.arrayProductsMermeladas[i][
                  "stock_disponible"
                ],
                cantidad: 1,
                porcentaje: "0",
                nuevo_precio: this.arrayProductsMermeladas[i]["precio_con_igv"],
                imagen: "assets/img/icons/image-not-found.png",
              });
            }
          }

          if (this.listMermeladas.length < 5) {
            $("#rowRightSliderTiendaMermeladas").hide();
            $("#rowRightSliderTiendaMermeladas2").hide();
          }

          this.slidesMermeladas = this.utilService.chunk(
            this.listMermeladas,
            this.countItems
          );
        });
      });
  }

  getMieles() {
    let id_categoria = { linea: "5" };
    let image = { tipo_consulta: "2" };

    this.productoService
      .getProductsByCategoria(id_categoria)
      .subscribe((response) => {
        this.arrayProductsMieles = response;

        this.productoService.getImageProduct(image).subscribe((response1) => {
          var arrayIdItemsWImages = [];
          var arrayIdItemsSystem = [];

          this.arrayImagesProductsMieles = response1["productos"];

          for (var j = 0; j < this.arrayImagesProductsMieles.length; j++) {
            arrayIdItemsWImages.push(
              Number(this.arrayImagesProductsMieles[j]["id_producto"])
            );
          }

          for (var k = 0; k < this.arrayProductsMieles.length; k++) {
            arrayIdItemsSystem.push(
              Number(this.arrayProductsMieles[k]["id_pro"])
            );
          }

          for (var i = 0; i < this.arrayProductsMieles.length; i++) {
            if (
              arrayIdItemsWImages.includes(
                this.arrayProductsMieles[i]["id_pro"]
              )
            ) {
              this.listMieles.push({
                id_pro: this.arrayProductsMieles[i]["id_pro"],
                linea_de_negocio: this.arrayProductsMieles[i][
                  "linea_de_negocio"
                ],
                presentacion: this.arrayProductsMieles[i]["presentacion"],
                nombre_producto: this.arrayProductsMieles[i]["nombre_producto"],
                precio_con_igv: this.arrayProductsMieles[i]["precio_con_igv"],
                stock_actual: this.arrayProductsMieles[i]["stock_actual"],
                stock_reservado: this.arrayProductsMieles[i]["stock_reservado"],
                stock_disponible: this.arrayProductsMieles[i][
                  "stock_disponible"
                ],
                cantidad: 1,
                porcentaje: "0",
                nuevo_precio: this.arrayProductsMieles[i]["precio_con_igv"],
                imagen: this.arrayImagesProductsMieles[
                  arrayIdItemsWImages.indexOf(
                    this.arrayProductsMieles[i]["id_pro"]
                  )
                ]["imagen_principal"],
              });
            } else {
              this.listMieles.push({
                id_pro: this.arrayProductsMieles[i]["id_pro"],
                linea_de_negocio: this.arrayProductsMieles[i][
                  "linea_de_negocio"
                ],
                presentacion: this.arrayProductsMieles[i]["presentacion"],
                nombre_producto: this.arrayProductsMieles[i]["nombre_producto"],
                precio_con_igv: this.arrayProductsMieles[i]["precio_con_igv"],
                stock_actual: this.arrayProductsMieles[i]["stock_actual"],
                stock_reservado: this.arrayProductsMieles[i]["stock_reservado"],
                stock_disponible: this.arrayProductsMieles[i][
                  "stock_disponible"
                ],
                cantidad: 1,
                porcentaje: "0",
                nuevo_precio: this.arrayProductsMieles[i]["precio_con_igv"],
                imagen: "assets/img/icons/image-not-found.png",
              });
            }
          }

          if (this.listMieles.length < 5) {
            $("#rowRightSliderTiendaMiel").hide();
            $("#rowRightSliderTiendaMiel2").hide();
          }

          if (this.countItems == 1) {
            if (this.innerWidth < 575) {
              $("#rowRightSliderTiendaMiel").show();
              $("#rowRightSliderTiendaMiel2").show();
            }
          }

          this.slidesMieles = this.utilService.chunk(
            this.listMieles,
            this.countItems
          );
        });
      });
  }

  /*
  getFrescos(){

    let id_categoria = { linea: "3"};

    this.productoService.getProductsByCategoria(id_categoria).subscribe(response =>{
      this.arrayProductsMieles = response;

 
        this.productoService.getImageProduct(image).subscribe(response1 =>{
 
          this.arrayImagesProductsMieles = response1["productos"];
 
          for(var i=0; i<this.arrayProductsMieles.length;i++){
 
            for(var j=0; j<this.arrayImagesProductsMieles.length;j++){
 
             if(this.arrayProductsMieles[i]["id_pro"] === Number(this.arrayImagesProductsMieles[j]["id_producto"])){ 
 
            
                  this.listMieles.push({
                    id_pro: this.arrayProductsMieles[i]["id_pro"],
                    linea_de_negocio : this.arrayProductsMieles[i]["linea_de_negocio"],
                    presentacion: this.arrayProductsMieles[i]["presentacion"],
                    nombre_producto: this.arrayProductsMieles[i]["nombre_producto"],
                    precio_con_igv: this.arrayProductsMieles[i]["precio_con_igv"],
                    stock_actual: this.arrayProductsMieles[i]["stock_actual"],
                    stock_reservado: this.arrayProductsMieles[i]["stock_reservado"],
                    stock_disponible: this.arrayProductsMieles[i]["stock_disponible"],
                    cantidad: 1,
                    imagen: this.arrayImagesProductsMermeladas[j]["imagen_principal"]
                  })
                  
              }
  
            }
          }
          this.slidesMieles = this.utilService.chunk(this.listMieles, this.countItems);
  
 
   
        })
   
 
    })

   
  }

  */

  getPulpas() {
    let id_categoria = { linea: "7" };

    let image = { tipo_consulta: "2" };

    this.productoService
      .getProductsByCategoria(id_categoria)
      .subscribe((response) => {
        this.arrayProductsPulpas = response;

        this.productoService.getImageProduct(image).subscribe((response1) => {
          var arrayIdItemsWImages = [];
          var arrayIdItemsSystem = [];

          this.arrayImagesProductsPulpas = response1["productos"];

          for (var j = 0; j < this.arrayImagesProductsPulpas.length; j++) {
            arrayIdItemsWImages.push(
              Number(this.arrayImagesProductsPulpas[j]["id_producto"])
            );
          }

          for (var k = 0; k < this.arrayProductsPulpas.length; k++) {
            arrayIdItemsSystem.push(
              Number(this.arrayProductsPulpas[k]["id_pro"])
            );
          }

          for (var i = 0; i < this.arrayProductsPulpas.length; i++) {
            if (
              arrayIdItemsWImages.includes(
                this.arrayProductsPulpas[i]["id_pro"]
              )
            ) {
              this.listPulpas.push({
                id_pro: this.arrayProductsPulpas[i]["id_pro"],
                linea_de_negocio: this.arrayProductsPulpas[i][
                  "linea_de_negocio"
                ],
                presentacion: this.arrayProductsPulpas[i]["presentacion"],
                nombre_producto: this.arrayProductsPulpas[i]["nombre_producto"],
                precio_con_igv: this.arrayProductsPulpas[i]["precio_con_igv"],
                stock_actual: this.arrayProductsPulpas[i]["stock_actual"],
                stock_reservado: this.arrayProductsPulpas[i]["stock_reservado"],
                stock_disponible: this.arrayProductsPulpas[i][
                  "stock_disponible"
                ],
                cantidad: 1,
                porcentaje: "0",
                nuevo_precio: this.arrayProductsPulpas[i]["precio_con_igv"],
                imagen: this.arrayImagesProductsPulpas[
                  arrayIdItemsWImages.indexOf(
                    this.arrayProductsPulpas[i]["id_pro"]
                  )
                ]["imagen_principal"],
              });
            } else {
              this.listPulpas.push({
                id_pro: this.arrayProductsPulpas[i]["id_pro"],
                linea_de_negocio: this.arrayProductsPulpas[i][
                  "linea_de_negocio"
                ],
                presentacion: this.arrayProductsPulpas[i]["presentacion"],
                nombre_producto: this.arrayProductsPulpas[i]["nombre_producto"],
                precio_con_igv: this.arrayProductsPulpas[i]["precio_con_igv"],
                stock_actual: this.arrayProductsPulpas[i]["stock_actual"],
                stock_reservado: this.arrayProductsPulpas[i]["stock_reservado"],
                stock_disponible: this.arrayProductsPulpas[i][
                  "stock_disponible"
                ],
                cantidad: 1,
                porcentaje: "0",
                nuevo_precio: this.arrayProductsPulpas[i]["precio_con_igv"],
                imagen: "assets/img/icons/image-not-found.png",
              });
            }
          }

          if (
            (this.listPulpas.length < 5 && $(window).width() < 700) ||
            this.listPulpas.length < 2
          ) {
            $("#rowRightSliderTiendaPulpas").hide();
            $("#rowRightSliderTiendaPulpas2").hide();
          }

          if (this.countItems == 1) {
            if (this.innerWidth < 575) {
              $("#rowRightSliderTiendaPulpas").show();
              $("#rowRightSliderTiendaPulpas2").show();
            }
          }

          this.slidesPulpas = this.utilService.chunk(
            this.listPulpas,
            this.countItems
          );
        });
      });
  }

  /*
  getDeshidratados(){

    let id_categoria = { linea: "2"};

    let image = {tipo_consulta: "2"};

    this.productoService.getProductsByCategoria(id_categoria).subscribe(response =>{
      this.arrayProductsDeshidratados = response;

 
        this.productoService.getImageProduct(image).subscribe(response1 =>{
 
          this.arrayImagesProductsDeshidratados = response1["productos"];
 
          for(var i=0; i<this.arrayProductsDeshidratados.length;i++){
 
            for(var j=0; j<this.arrayImagesProductsDeshidratados.length;j++){
 
             if(this.arrayProductsDeshidratados[i]["id_pro"] === Number(this.arrayImagesProductsDeshidratados[j]["id_producto"])){ 
 
            
                  this.listDeshidratados.push({
                    id_pro: this.arrayProductsDeshidratados[i]["id_pro"],
                    linea_de_negocio : this.arrayProductsDeshidratados[i]["linea_de_negocio"],
                    presentacion: this.arrayProductsDeshidratados[i]["presentacion"],
                    nombre_producto: this.arrayProductsDeshidratados[i]["nombre_producto"],
                    precio_con_igv: this.arrayProductsDeshidratados[i]["precio_con_igv"],
                    stock_actual: this.arrayProductsDeshidratados[i]["stock_actual"],
                    stock_reservado: this.arrayProductsDeshidratados[i]["stock_reservado"],
                    stock_disponible: this.arrayProductsDeshidratados[i]["stock_disponible"],
                    cantidad: 1,
                    porcentaje: "0",
                    nuevo_precio:  this.arrayProductsDeshidratados[i]["precio_con_igv"],
                    imagen: this.arrayImagesProductsDeshidratados[j]["imagen_principal"]
                  })
                  
              }
  
            }
          }
          this.slidesDeshidratados = this.utilService.chunk(this.listDeshidratados, this.countItems);

  
 
   
        })
   
 
    })

   
  }
  */

  goBuscarProducto() {
    this.router.navigate(["/tienda/buscar"]);
  }
}
